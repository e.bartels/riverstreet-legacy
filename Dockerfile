FROM python:2.7-slim

WORKDIR /app

COPY requirements.txt ./

RUN BUILD_DEPS=" \
        build-essential \
        python-dev \
        default-libmysqlclient-dev \
        libexpat1-dev \
    "; \
    RUNTIME_DEPS=" \
        procps \
        libxml2 \
        shared-mime-info \
        mime-support \
        libexpat1 \
        libmariadb3 \
    "; \
    apt-get -qy update && \
    apt-get install -qy --no-install-recommends $RUNTIME_DEPS && \
    apt-get install -qy --no-install-recommends $BUILD_DEPS && \
    pip install --no-cache-dir --no-binary Django -r requirements.txt && \
    # fix for markdown import error
    rm -f /usr/local/bin/markdown.py* && \
    apt-get purge -y --auto-remove $BUILD_DEPS && \
    rm -rf /var/cache/apt/* /var/lib/apt/lists/*

COPY . ./

ENV PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1 \
    DJANGO_ENV=production \
    PYTHONPATH=/app/python_modules/site-packages:$PYTHONPATH

RUN python -m compileall . && \
    useradd -d /app -M -u 1000 web && \
    mkdir media && chown web.web media && \
    ln -s /app/media/files cms/static/files && \
    ln -s /app/media/images cms/static/images && \
    ln -s /app/media/videos cms/static/videos && \
    ln -s /app/media/_thumbs cms/static/_thumbs && \
    mkdir tmp && chown web.web tmp

EXPOSE 8000 8000
USER web

ENTRYPOINT ["uwsgi", "--die-on-term", "--ini", "/app/config/uwsgi.ini"]
