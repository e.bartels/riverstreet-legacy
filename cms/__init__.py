import logging

# Turn off exceptions for logging module
if hasattr(logging, 'raiseExceptions'):
    logging.raiseExceptions = 0
