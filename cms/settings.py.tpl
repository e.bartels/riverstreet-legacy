from settings_default import *

# Database Settings
DATABASE_ENGINE = ''           # 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
DATABASE_NAME = ''      # Or path to database file if using sqlite3.
DATABASE_USER = ''             # Not used with sqlite3.
DATABASE_PASSWORD = ''         # Not used with sqlite3.
DATABASE_HOST = ''             # Set to empty string for localhost. Not used with sqlite3.
DATABASE_PORT = ''             # Set to empty string for default. Not used with sqlite3.

# Set Time Zone
TIME_ZONE = "Etc/GMT+8" # i.e. PST

ADMINS = (
    #('Your Name', 'youremail@host.com'),
)
MANAGERS = ADMINS


# Path containing this module
INSTALLATION_DIR = os.path.dirname(os.path.abspath(__file__)) 

# Make this unique, and don't share it with anybody.
SECRET_KEY = ''

#EMAIL_HOST = ''  # Some hosting providers need this set to send mails (e.g. WestHost)

MEDIA_ROOT = os.path.abspath(os.path.join( INSTALLATION_DIR, 'static')) + "/"

TEMPLATE_DIRS += (
    os.path.abspath(os.path.join(INSTALLATION_DIR, 'templates')),
)
