from django.contrib import admin

from cms.apps.utils.adminsites import ArtcodeAdmin

# Override the global admin.site
admin.site = ArtcodeAdmin()

# These apps will be hidden from the admin index pages.
admin.site.hidden_apps = ['sites', 'tagging', 'menus', 'accounts',]

# Set up custom labels, models and ordering for admin apps.
admin.site.index_config = {
    'auth': {
        'label': 'User Logins',
        'order': 0,
    },
    'media': {
        'label': 'Media Library',
        'order': 1,
    },
    'content': {
        'order': 2,
        'models': ['Page', 'Collection', 'WorkSplash',],
    },
    'blog': {
        'order': 3,
        'models': ['Entry', 'Category', 'Link']
    },
    'clientsections': {
        'label': 'Client Sections',
        'models': ['ClientSection',],
    }
}

# Disable admin delete action globally
admin.site.disable_action('delete_selected')


# automatically discover apps that have admin modules
# but have not yet been registered.
admin.autodiscover()
