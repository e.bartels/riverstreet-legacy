from cms.apps.configuration import *

SITE_GROUP = ConfigurationGroup('SITE', 'Site Settings', ordering=0)
INTERFACE_GROUP = ConfigurationGroup('INTERFACE', 'Interface', ordering=1)

config_register_list(
    StringValue(SITE_GROUP, 
        'TITLE', 
        description='Site Title', 
        ordering=0,
        default='',
    ),
    LongStringValue(SITE_GROUP, 
        'KEYWORDS', 
        description='Site Keywords',
        help_text='Enter a comma-separated list of keywords that describe your site.  This helps some search engines index your pages.',
        ordering=1,
        default='',
    ),
    LongStringValue(SITE_GROUP,
        'DESCRIPTION',
        description='Site Description',
        help_text='Enter a description for your site. This is used by some search engines.',
        ordering=2,
        default='',
    ),
    StringValue(SITE_GROUP,
        'ADDRESS',
        description='Address',
        ordering=4,
        default=''
    ),
    StringValue(SITE_GROUP,
        'PHONE',
        description='Phone',
        ordering=5,
        default=''
    ),
    StringValue(SITE_GROUP,
        'EMAIL',
        description='Email',
        ordering=6,
        default=''
    ),
    StringValue(SITE_GROUP,
        'SUBTITLE',
        description='Front Page Subtitle',
        ordering=3,
        default='hello there'
    )
)

