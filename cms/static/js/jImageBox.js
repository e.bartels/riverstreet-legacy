(function($) {
jImageBox = function(){
    return this.initialize.apply(this, arguments);
};

jImageBox.prototype = {
    options: {
        postOpen: null
    },

    webroot: (webroot || '/'),

    initialize: function($$, options) {
        var self = this;
        self.options = $.extend(self.options, options);
        self.$$ = $$;
        $$.each(function(i, a) {
            $(a).click(function(){
                self.open(i, true);
                return false;
            });
        });
    },

    open: function(index) {
        var self = this;
        self.close();

        var $link = self.$$.slice(index);
        var image_id = self.image_id = $link.attr('id').replace('image_','');

        var $overlay = $('<div id="overlay"></div>').
            css({display : 'none'}).
            appendTo(document.body).
            click(self.close);

        var fadeIn = (arguments.length > 1) && arguments[1];
        if (fadeIn) 
            $overlay.fadeIn(200); 
        else
            $overlay.show()

        var $loading = $('<img id="fullscreenLoad" src="'+self.webroot+'img/loadingAnimation.gif" alt="loading" >');
        $loading.hide().appendTo(document.body);

        // Build DOM nodes 
        var $fsContainer = $('<div id="fullscreenContainer"><img id="fullscreenImage" ><div id="fullscreenImageData">'+
            '<div id="fullscreenCaption"></div>'+
            '<a id="fullscreenCloseButton" href="#">X</a>'+
            '</div></div>').appendTo(document.body);
        
        $('#fullscreenCloseButton').click(self.close);

        if( self.$$.length > 1 ) {
            var count = self.$$.length;
            var cur = index + 1;
            $('<div id="fullscreenControls"><span>'+cur+' of '+count+'</span> &nbsp;<a id="fullscreenPrev">&laquo;</a> <a id="fullscreenNext">&raquo;</a></div>').
                insertAfter('#fullscreenCaption');
            $('#fullscreenNext, #fullscreenImage').click(function(){
                var i = ((index + 1) + self.$$.length) % self.$$.length;
                self.open(i);
            });
            $('#fullscreenPrev').click(function(){
                var i = ((index - 1) + self.$$.length) % self.$$.length;
                self.open(i);
            });
        }

        // Set up Data
        var caption = $link.attr('caption') || '';
        $('#fullscreenCaption').append(unescape(caption));

        
        // Load Image
        var preload = new Image();
        if( $.browser.safari ) { // safari 1.x fix
            var webKitFields = RegExp("( AppleWebKit/)([^ ]+)").exec(navigator.userAgent);
            var version = parseFloat(webKitFields[2]);          
            if( parseFloat(version) < 200 ) { // 1.x needs the image in the document to get width and height
                preload = $('<img id="fullscreenPreload" >')[0]; 
                $(preload).css({'position': 'absolute', 'top':'-5000px'});
                $(document.body).append(preload);
            }
        }
        
        var captionHeight = $('#fullscreenImageData').height() || 0;
        $fsContainer.hide();
        preload.onload = function() {
            var pad = 10;
            var pagesize = [$(window).width(), $(window).height()];
            var x = pagesize[0] - (pad*2);
            var y = pagesize[1] - captionHeight - (pad * 4) - 10; //(caption.length > 0 ? 100: 50);
            var imageWidth = preload.width;
            var imageHeight = preload.height;
            $('#fullscreenPreload').hide().remove(); // safari 1.x fix
            if (imageWidth > x) {
                imageHeight = imageHeight * (x / imageWidth); 
                imageWidth = x; 
                if (imageHeight > y) { 
                    imageWidth = imageWidth * (y / imageHeight); 
                    imageHeight = y; 
                }
            } else if (imageHeight > y) { 
                imageWidth = imageWidth * (y / imageHeight); 
                imageHeight = y; 
                if (imageWidth > x) { 
                    imageHeight = imageHeight * (x / imageWidth); 
                    imageWidth = x;
                }
            }
            $('#fullscreenImage').attr({
                'src': preload.src                
            }).css({
                'width': imageWidth,
                'height': imageHeight
            });

            $fsContainer.css({width:(imageWidth+(pad*2))});
            var t = parseInt(( pagesize[1] - $fsContainer.height() )/2); 
            if( t < 0) t = 0;
            t += document.documentElement.scrollTop || document.body.scrollTop;                
            var l = parseInt(( pagesize[0] - $fsContainer.width() )/2);
            $fsContainer.css({'position': 'absolute', 'top':t+'px','left':l+'px'});

            if (self.options.postOpen ) {
                self.options.postOpen.apply(self);
            }

            $loading.remove();
            $fsContainer.show();
        };
        preload.src = self.$$.slice(index).attr('href'); 
        $loading.show();

        return false;
    },

    close: function() {
        var self = this;
        $('#overlay').hide().remove();
        $('#fullscreenContainer').remove();
        $('#fullscreenLoad').remove();

        return false;
    }
}

$.fn.jImageBox = function(options) {
    var ibox = new jImageBox(this, options);
    return this; 
}
    
})(jQuery);
