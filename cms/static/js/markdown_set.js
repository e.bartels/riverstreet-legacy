var mdConverter = new Showdown.converter(); // js markdown converter

// markdown extensions to markitup editor.
var markdownExtension = { 
    undo_list: [],
    redo_list: [],
    previewMaxDelay: 3000, // longest update pause (in ms) for markdown conversion


    // maximize the editor
    maximize: function () {
        var self = this;
        var $$ = $(self.textArea);
        var $p = $$.parents('.'+self.options.nameSpace);
        place_holder = $p.parent();
        $p.addClass('markItUpFullscreen').appendTo(document.body);
        $$.height($p.height() - 58);
        if (self.iFrame) self.preview(); 
        window.scrollTo(0,0);
        self.fullScreen = true;
    },
    // unmaximize the editor
    unmaximize: function() {
        // unmaximized the editor
        var self = this;
        var $$ = $(self.textArea);
        var $p = $$.parents('.'+self.options.nameSpace);
        if( self.iFrame ) self.preview();
        self.fullScreen = false;
        $p.removeClass('markItUpFullscreen');
        $p.appendTo(place_holder);
        $$.height('');    
    },

    // Markdown preview 
    onPreviewInput: function() {
        var self = this;
        // if we already have convertText scheduled, cancel it
        if (self.convertTextTimer) {
            window.clearTimeout(self.convertTextTimer);
            self.convertTextTimer = undefined;
        }

        var timeUntilConvertText = 0;
        // make timer adaptive
        timeUntilConvertText = self.processingTime || 0;
        
        if (timeUntilConvertText > self.previewMaxDelay)
            timeUntilConvertText = self.previwMaxDelay;
        
        // Schedule convertText().
        self.convertTextTimer = window.setTimeout(function(){self.mdConvertText()},timeUntilConvertText);
    },

    mdConvertText: function() {
        // convert textarea text to markdown
        var self = this;
        var $$ = $(self.textArea);
        var text = $$.val();
        
        if (!self.iFrame) return;

        var startTime = new Date().getTime();

        // Do the conversion
        text = mdConverter.makeHtml(text);

        var endTime = new Date().getTime();	
        processingTime = endTime - startTime;
        
        try { // in case the document is't loaded yet.
            $(self.previewWin.document.body).find('#content').html(text);
        } catch (error){ 
            window.setTimeout( function(){ self.mdConvertText()}, 100 );
        }
    },

    // Improved undo & redo functionality
    initUndo: function() {
        var self = this;
        // keyboard events
        $(self.textArea).bind($.browser.msie ? 'keydown' : 'keypress', function(e){
            if (e.metaKey) {
                if (e.shiftKey && (e.which == 122 || e.which == 90)) { //ctrl-shift-z
                    self.redo();
                    return false;
                }
                else if (e.which == 121 || e.which == 89) { // ctrl-y : redo
                    self.redo();                                
                    return false;
                }
                else if (e.which == 122 || e.which == 90) { // ctrl-z : undo
                    self.undo();
                    return false;
                }                            
            } else {
                if (e.which == 17 ) return true; // ctrl key only (msie)
                else if (e.which == 32 || e.which == 0 || e.which == 9) {  // space or tab (word boundary)
                    self.saveUndo();
                }
                else $(self.textArea).trigger('update'); // regular char
            }
        });
        $(self.textArea).bind('update', function(){
            // Save undo history if difference is >= 8 chars (avoid saving every single char)
            var diff;
            if (self.undo_list.length < 1) diff = 9; 
            else diff = self.textArea.value.length - self.undo_list[self.undo_list.length-1].content.length
            if (diff >= 8 ) self.saveUndo();
        });
        // initial save for undo history
        $(self.textArea).focus(function(){self.saveUndo()});
    },
    undo: function() {
        // undo last change from undo list and save current to redo list
        var self = this;
        var item = self.undo_list.pop();
        if (typeof item != 'undefined') {
            self.get();
            var last_item = self.redo_list.slice(-1)[0];            
            if (self.redo_list.length < 1 || self.redo_list.slice(-1)[0].content != self.textArea.value){
                self.redo_list.push({
                    content: self.textArea.value, 
                    position: self.caretPosition,
                    scroll: item.scroll
                });
            }                            
            self.textArea.value = item.content;
            self.set(item.position,0); 
            self.textArea.scrollTop = item.scroll;
        }
        
    },
    redo:  function() {
        // redo last change from redo list and save current to undo list.
        var self = this;
        var item = self.redo_list.pop();
        if (typeof item != 'undefined') {
            self.get();
            if (self.redo_list.length < 1 || self.redo_list.slice(-1)[0].content != self.textArea.value){
                self.undo_list.push({
                    content: self.textArea.value, 
                    position: self.caretPosition,
                    scroll: item.scroll
                });
            }
            self.textArea.value = item.content;
            self.set(item.position,0);
            self.textArea.scrollTop = item.scroll;
        }    
    },
    saveUndo: function() {
        // Save current field content to undo list
        var self = this;
        if (self.undo_list.length < 1 || (self.textArea.value != self.undo_list[self.undo_list.length-1].content)){
            self.get();
            self.undo_list.push({
                content: self.textArea.value, 
                position: self.caretPosition,
                scroll: self.textArea.scrollTop
            });
            self.redo_list.length = 0;
        }
    },
    markdownTitle: function(char) {
        heading = '';
        n = $.trim(this.selection||this.placeHolder).length;
        for(i = 0; i < n; i++) {
            heading += char;
        }
        return '\n'+heading+'\n';
    }

};


                    
/* Editor Button Definitions */
var mdButtons = {
    'bold' : {name:'Bold', key:"B", openWith:'**', closeWith:'**', className: 'button-strong' },
    'italic' : {name:'Italic', key:"I", openWith:'*', closeWith:'*', className: 'button-em' },
    'h1': {name:'First Level Heading', key:"1", placeHolder:'Your title here...', className: 'button-h1', closeWith:function(markItUp) { return this.markdownTitle('=') }},
    'h2': {name:'Second Level Heading', key:"2", placeHolder:'Your title here...', className: 'button-h2', closeWith:function(markItUp) { return this.markdownTitle('-') } },
    'h3': {name:'Heading 3', key:"3", openWith:'### ', placeHolder:'Your title here...', className: 'button-h3' },
    'h4': {name:'Heading 4', key:"4", openWith:'#### ', placeHolder:'Your title here...', className: 'button-h4' },
    'h5': {name:'Heading 5', key:"5", openWith:'##### ', placeHolder:'Your title here...', className: 'button-h5' },
    'h6': {name:'Heading 6', key:"6", openWith:'###### ', placeHolder:'Your title here...',className: 'button-h6' },

    'ul' :  {
        name:'Bulleted List', className: 'button-ul', 
        replaceWith : function() {
            var lines = this.selection.split("\n");
            if (lines.length < 2) return '\n- ' + this.selection;
            lines = $.map(lines, function(l, i){
                return '- ' + l;
            });
            return lines.join("\n");
        }       
    },
    'ol' : {
        name:'Numeric List', className: 'button-ol', 
        replaceWith:function() {
            var lines = this.selection.split("\n");
            if (lines.length < 2) return '\n1. ' + this.selection;
            lines = $.map(lines, function(l, i){
                return (i+1) + '. ' + l;
            });
            return lines.join("\n");
        }
    },
    'blockquote' : {
        name:'Blockquote', className: 'button-blockquote', 
        beforeInsert: function() { if( this.multiLine ) { this.ctrlKey = this.shiftKey = true; }},
        openWith: function(){
            if (! this.multiLine ) {
                return '\n> ';
            }
            return '> ';
        }, 
        closeWith : function() {
            if (! this.multiLine ) {
                return '\n\n';
            }
            return ''
        }
    },
    'code' : {
        name:'Code Block / Code', className: 'button-code',
        beforeInsert: function() { if( this.multiLine ) { this.ctrlKey = this.shiftKey = true; }},
        openWith: function() {
           if( this.multiLine ) {
               return '    ';
           } else {
               return '`';
           }
        },
        closeWith: function() {
            if( ! this.multiLine ) {
                return '`';
            }
        }
    },
    'link' : {
        name:'Link', className: 'button-link',
        openWith: '',
        closeWith: function() {
            var editor = this;

            // Create dialog markup
            var $dialog = $('<div class="dialog"></div>').appendTo(document.body);
            var $url = $('<input type="text" class="url" value="http://" size="30"/>')
                .appendTo($dialog).before('<label>URL: </label> ')
                .after('<a href="'+admin_root+'media/link_browser/" class="find-link">find</a>');
            var $text = $('<input type="text" class="link_text" size="30" />')
                .appendTo($dialog).before('<br><br>').before('<label>Text: </label> ').val(editor.get());
            var $target = $('<select></select>').append('<option value="">--Not Set--</option>')
                .append('<option value="_self">Open link in the same window</option>')
                .append('<option value="_blank">Open link in a new window</option>')
                .appendTo($dialog).before('<br><br>').before('<label>Target: </label> ');
            

            // Open dialog
            $dialog.dialog({
                width: 350, height: 290,
                title: '<h4>Create a link</h4>',
                closeText: 'X',
                modal: true,
                draggable: true,
                open: function() {
                    window.setTimeout(function(){$dialog.find('input').eq(0).focus();},100);
                },
                close: function() {
                    $(this).dialog('destroy').remove();
                },
                buttons: {
                    'cancel': function() {
                        $(this).dialog('close');
                    },
                    'ok': function() {
                        var url = $url.val(),
                            text = $text.val(),
                            target = $target.val(),
                            attributes = [],
                            attr = '';
                        
                        if (!text) text = editor.get();
                        if (target) attributes.push('target="' + target + '"');
                        attr = attributes.join(' ');
                        
                        if (attr)
                            var link = '<a href="'+url+'" ' + attr + '>' + text + '</a>';
                        else
                            var link = '['+text+']('+url+')';

                        editor.insert(link);
                        $(this).dialog('close');
                        editor.refreshPreview();
                    }
                }
            }).parent().find('.ui-dialog-buttonpane button:contains(ok)').addClass('default');;
            

            // URL search popup
            var $find_link = $dialog.find('.find-link');
            var callback = function(window, file) {
                $find_link.prev().val(file);
                window.close();
            };
            $find_link.click(function() {
                var link = this;
                var size = '820x600';
                var href= link.href + '?_popup=1'
                size = size.split('x');

                window.file_manager_callback = callback;

                var w = window.open(href, 'lookup_url', 'width='+size[0]+',height='+size[1] + ',scrollbars=1');
                if (window.focus) {w.focus()}
                return false;
            });
            return ''
        }
    },
    'image': {
        name:'Picture', className: 'button-img', 
        openWith: '',
        replaceWith: function() {
            var editor = this;
            
            // Create dialog markup
            var $dialog = $('<div class="dialog images-dialog"></div>').appendTo(document.body);
            var $url = $('<input type="text" class="url" value="http://" size="30"/>')
                .appendTo($dialog).after('<a href="'+admin_root+'media/image/file_browser/" class="find-link">find</a><br><br>')
                .before('<label>URL: </label> ');
            var $text = $('<input type="text" class="link_text" size="30" />')
                .appendTo($dialog).before('<label>Alt Text: </label> ').val(editor.get());
            var $width = $('<input type="text" class="image_width" size="3" />')
                .appendTo($dialog).before('<br><br>').before('<label>Width: </label>').after(' <span>px</span>');
            var $height = $('<input type="text" class="image_height" size="3" />')
                .appendTo($dialog).before('<br><br>').before('<label>Height: </label>').after(' <span>px</span>');
            
            var $flt = $('<select class="image_float">').appendTo($dialog).before('<br><br>')
                .append('<option value="">None</option>')
                .append('<option value="left">Left</option>')
                .append('<option value="right">Right</option>')
                .before('<label>Float: </label');

            var $margins = $('<div class="margins"></div>').appendTo($dialog).before('<br/><br/>').before('<label>Margins: </label>');

            
            var $margin_top = $('<input type="text" class="margin-top" size="3" />')
                .appendTo($margins).before('<br><br>').before('<label>t: </label>').after(' <span>px</span>');
            var $margin_right = $('<input type="text" class="margin-right" size="3" />')
                .appendTo($margins).before('<label>r: </label>').after(' <span>px</span>');
            var $margin_bottom = $('<input type="text" class="margin-bottom" size="3" />')
                .appendTo($margins).before('<br><br>').before('<label>b: </label>').after(' <span>px</span>');
            var $margin_left = $('<input type="text" class="margin-left" size="3" />')
                .appendTo($margins).before('<label>l: </label>').after(' <span>px</span>');

            // Open dialog
            $dialog.dialog({
                width: 350, height: 560,
                title: '<h4>Insert an Image </h4>',
                closeText: 'X',
                modal: true,
                draggable: true,
                open: function() {
                    window.setTimeout(function(){$dialog.find('input').eq(0).focus();},100);
                },
                close: function() {
                    $(this).dialog('destroy').remove()
                },
                buttons: {
                    'cancel': function() {
                        $(this).dialog('close');
                    },
                    'ok': function() {
                        var url = $url.val(),
                            text = $text.val(),
                            width = $width.val(),
                            height = $height.val(),
                            flt = $flt.val(),
                            mt = $margin_top.val(),
                            mr = $margin_right.val(),
                            mb = $margin_bottom.val(),
                            ml = $margin_left.val(),
                            attributes = [],
                            attr = '';
                        if (!text) text = editor.get();
                        
                        if (width) attributes.push('width="' + width + '"');
                        if (height) attributes.push('height="' + height + '"');

                        var margin = mt || mr || mb || ml;                  
                        var style = flt || margin || false;

                        if (style) {
                            var s = 'style="';
                            if (flt) s += 'float: ' + flt + '; ';
                            if (mt) s += 'margin-top: '+mt+'px; ';
                            if (mr) s += 'margin-right: '+mr+'px; ';
                            if (mb) s += 'margin-bottom: '+mb+'px; ';
                            if (ml) s += 'margin-left: '+ml+'px; ';
                            
                            s += '"';
                            attributes.push(s);
                        }

                        attr = attributes.join(' ');

                        var link = '<img src="'+url+'" alt="'+text+'" '+attr+' />';
                        editor.insert(link);
                        $(this).dialog('close');
                        editor.refreshPreview();
                    }
                }
            }).parent().find('.ui-dialog-buttonpane button:contains(ok)').addClass('default');

            // URL search popup
            var $find_link = $dialog.find('.find-link');
            var callback = function(window, file) {
                $find_link.prev().val(file);
                window.close();
            };
            $find_link.click(function() {
                var link = this;
                var size = '820x600';
                var href= link.href + '?_popup=1'
                size = size.split('x');

                window.file_manager_callback = callback;

                var w = window.open(href, 'lookup_url', 'width='+size[0]+',height='+size[1] + ',scrollbars=1');
                if (window.focus) {w.focus()}
                return false;
            });
        }
        //replaceWith:'![$!{Alternative text}!]($!{Url=http://}! "$!{Title}!")' 
    },
    'video': {
        name:'Video', className: 'button-video', 
        openWith: '',
        replaceWith: function() {
            var editor = this;
            
            // Create dialog markup
            var $dialog = $('<div class="dialog videos-dialog"></div>').appendTo(document.body);
            var $video_title = $('<input type="text" class="title" disabled="true" value="" size="30"/>')
                .appendTo($dialog).after('<a href="'+admin_root+'media/video/file_browser/" class="find-link">find</a><br><br>')
                .before('<label>Select a video: </label> ');
            var $embed = $('<input type="hidden" class="embed" value="" size="30"/>')
                .appendTo($dialog);
            /*var $width = $('<input type="text" value="360" class="video_width" size="3" />')
                .appendTo($dialog).before('<br><br>').before('<label>Width: </label>').after(' <span>px</span>');
            var $height = $('<input type="text" value="292" class="video_height" size="3" />')
                .appendTo($dialog).before('<br><br>').before('<label>Height: </label>').after(' <span>px</span>');
            */

            // Open dialog
            $dialog.dialog({
                width: 350, height: 150,
                title: '<h4>Insert a Video</h4>',
                closeText: 'X',
                modal: true,
                draggable: true,
                open: function() {
                    window.setTimeout(function(){$dialog.find('input').eq(0).focus();},100);
                },
                close: function() {
                    $(this).dialog('destroy').remove()
                },
                buttons: {
                    'cancel': function() {
                        $(this).dialog('close');
                    },
                    'ok': function() {
                        var embed = $embed.val(),
                            //width = $width.val(),
                            //height = $height.val(),
                            attributes = [],
                            attr = '';
                         
                        editor.insert("\n" + embed + "\n");
                        $(this).dialog('close');
                        //editor.refreshPreview();
                    }
                }
            }).parent().find('.ui-dialog-buttonpane button:contains(ok)').addClass('default');

            // URL search popup
            var $find_link = $dialog.find('.find-link');
            var callback = function(window, file) {
                $embed.val(file);
                $video_title.val(file);
                window.close();
            };
            $find_link.click(function() {
                var link = this;
                var size = '820x600';
                //var width = $width.val(), height = $height.val()
                var href= link.href + '?_popup=1'; //&width='+width+'&height='+height;
                size = size.split('x');

                window.file_manager_callback = callback;

                var w = window.open(href, 'lookup_url', 'width='+size[0]+',height='+size[1] + ',scrollbars=1');
                if (window.focus) {w.focus()}
                return false;
            });
        }
        //replaceWith:'![$!{Alternative text}!]($!{Url=http://}! "$!{Title}!")' 
    },
    'preview' :  {
        name:'Preview', className:"button-preview", 
        call: function(){
            var self = this;
            var $$ = $(self.textArea);
            // Event to update the preview window
            updateEvent = function() { 
                self.onPreviewInput();
            };
            if (!self.iFrame ){
                self.preview();
                $$.keyup(updateEvent);
                $('li > a ', self.header).click(updateEvent);
                $(self.previewWin).one('load', updateEvent);
                
                // override the default refresh function to do nothing
                // we'll handle the refresh with our custom markdown conversion
                $(self.previewWin).one('load', function() {
                    self.preview_save_func = self.refreshPreview;
                    self.refreshPreview = function(){};
                });
                // reset the default refresh function
                $(self.previewWin).unload(function(){
                    self.refreshPreview = self.preview_save_func;
                });
                window.setTimeout(updateEvent, 500);
            } else {
                $$.unbind('keyup', updateEvent);
                $('li > a', self.header).unbind('click', updateEvent);
                self.preview();
            }
            
            if (self.fullScreen) {  // Preview win height if in fullscreen mode.              
                var h = $(window).height() - 58;
                if( self.iFrame ) {                    
                    $$.height( h /2 );
                    $$.parent().find('.markItUpPreviewFrame').height( h/2);
                } else {
                    $$.height(h);
                }
            }            
        }
    },
    'separator' : {separator:'---------------' },
    'fullscreen' : {name:'Full Screen', key:'F', className:"button-fullscreen", call: function(){
        if (! this.fullScreen) {            
            this.maximize();
        } else {
            this.unmaximize();
        }
    }}
};


/* Define Editors */
markdownSet = [
    mdButtons['bold'], mdButtons['italic'], mdButtons['separator'], mdButtons['h1'], 
    mdButtons['h2'], mdButtons['h3'], mdButtons['h4'], mdButtons['h5'], mdButtons['separator'], 
    mdButtons['ul'], mdButtons['ol'], mdButtons['separator'], mdButtons['blockquote'], mdButtons['code'], 
    mdButtons['separator'],mdButtons['link'], mdButtons['image'], mdButtons['video'], mdButtons['separator'], 
    mdButtons['preview'], mdButtons['fullscreen']
];
markdownSetSimple = [
    mdButtons['bold'], mdButtons['italic'], mdButtons['separator'], mdButtons['ul'],
    mdButtons['ol'], mdButtons['blockquote'], mdButtons['code'], mdButtons['separator'],
    mdButtons['link'], mdButtons['separator'], mdButtons['preview']
];

markdown = {	
    nameSpace:			"markdown",
    previewBodyCss:     [media_url + "css/reset.css", media_url + "css/typography.css"],
    onEnter: 			{openWith:'  '},
    onShiftEnter:		{keepDefault:false,	openWith:'\n'},
    onCtrlEnter:		{openWith:'\n\n'},
    onTab: 				{keepDefault:false, openWith:'    '},
    placeHolder:		"Enter text here...",
    markupSet:          markdownSet,
    beforeInsert: function() {
        // Multiline detection
        this.multiLine = RegExp('\n').test(this.selection);
        // Save undo info when changing text.
        this.saveUndo();
    },
    preInit: function(self) {
        $.extend(self, markdownExtension); 
        self.initUndo();                    
    }

}
markdownSimple = $.extend({}, markdown, {markupSet: markdownSetSimple, nameSpace: 'markdown markdownSimple'});

markdownBlogSidebar = $.extend({}, markdownSimple, {previewBodyId: '#blog-sidebar'});


$(function() {
    $('textarea.markdown').markItUp(markdown);
    $('textarea.markdownSimple').markItUp(markdownSimple);
});
