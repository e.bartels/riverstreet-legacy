/* global webroot:true, Cufon:true, Raphael:true */

var eAnimation;

$(function() {
    // Font Replacement
    Cufon.replace('#header .subtitle', {fontFamily: 'Gotham Rounded Light'});

    // 'e' animation
    var animation = new eAnimation();

    $('#main-menu a, .categories-menu a').hover(function() {
      var link = $(this);
      var color = link.css('color');
      animation.setFill(color);
    }, function() {
      animation.setFill();
    });

    // Scroller for submenus
    $('#submenus .submenu, #blog-sidebar .blog-archive-list').Scroller({
        scrollbar: false,
        prevButtonHTML: '<a>back</a>',
        nextButtonHTML: '<a>more</a>',
        paginate: true, animate: true

    });
});


eAnimation = function() {
    return this.init.apply(this, arguments);
};

// Spinning 'e' in logo that follows the mouse cursor
eAnimation.prototype = {
    timeout: null,

    init: function(color) {
        var self = this;
       
        self.container = $('#logo a'); // animation container
        self.color = color || self.container.css('color');
        self.container.find('img').remove();
        self.R = Raphael(self.container[0], 128, 165);
         
        self.img = self.R.image(webroot + 'img/logo_noe.png', 0, 0, 128, 165);

        self.e = self.R.print(70, 80, "e", self.R.getFont("Gotham Rounded Light"), 57).attr({fill: self.color });

        self.root = self.container.offset(); // root of the element in x/y coords
        self.root.top = self.root.top + 80;
        self.root.left = self.root.left + 70;

        self.angleOrigin = 0; // origin rotation in degrees
        self.rotate(42);

        $(document).mousemove(function(e) { self.mousemove(e); });

        window.setInterval(function() {
            if (self.resting) {
              return;
            }
            var t = new Date().getTime();
            if (self.time && t - self.time > 1000) {
                self.originAnimation();
                self.time = t + 100000000;
            }
        }, 1000);
        
    },

    setFill: function(color) {
        var self = this;
        if (!color){
          color = self.color;
        }
        self.e.attr({fill: color});
    },

    rotate: function(deg) {
        var self = this;
        self.d = deg;
        self.e.transform('r'+deg);
    },

    originAnimation: function() {
        var self = this;
        var dif = (Math.abs(self.d) % 360);
        if (dif > 215) {
          dif -= 360;
        }
        self.e.attr({transform: 'r'+dif}).animate({transform: 'r42'}, 3000, 'elastic');
        self.resting = true;
    },

    mousemove: function(e) {
        var self = this;
        self.e.stop();
        var x = e.pageX, y = e.pageY;
        var radians = Math.atan2(self.root.top - y, self.root.left - x);
        var d = (radians/Math.PI) * 180;
        self.R.safari();
        self.rotate(d + 140);
        
        self.resting = false;
        self.time = new Date().getTime();
    }
};
