/*global flowplayer:true, media_url:true*/
var CollectionPage;

(function($) {
    "use strict";

    CollectionPage = function() {
        return this.initialize.apply(this, arguments);
    };

    CollectionPage.prototype = {
        defaults: {
            el: '.collection',
            mediaEl: '.media',
            videosEl: '.videos',
            imagesEl: '.images'
        },

        initialize: function(o) {
            var self = this;
            this.o = $.extend({}, this.defaults, o);

            this.el = $(this.o.el);
            this.mediaEl = $(this.o.mediaEl);
            this.video_links = $(this.o.videosEl).find('>a');
            this.image_links = $(this.o.imagesEl).find('>a');
            this.layout = this.el.is('.hd') ? 'hd' : 'sd';

            // Video Links
            this.video_links.click(function(e, autoplay) {
                e.preventDefault();
                var i = self.video_links.index(e.currentTarget);
                self.showVideo(i, autoplay);
            });

            // Image Links
            $('.images a').click(function(e) {
                e.preventDefault();
                var i = self.image_links.index(e.currentTarget);
                self.showImage(i);
            });

            // Show first item
            $('.videos a, .images a').eq(0).trigger('click', [false]);
        },

        showVideo: function(i, autoplay) {
            if (typeof(autoplay) === 'undefined') {
                autoplay = true;
            }
            var item = this.video_links.eq(i);
            this.mediaEl.empty();

            if (item.is('.vimeo')) {
                this.showVimeoVideo(item, autoplay);
            } else {
                this.showLocalVideo(item, autoplay);
            }

            $('<div class="caption"></div>').html(
                item.attr('caption')
            ).appendTo(this.mediaEl);

            this.image_links.removeClass('active');
            this.video_links.removeClass('active').eq(i).addClass('active');
        },

        showLocalVideo: function(item, autoplay) {
            var playlist,
                href = item.attr('href');
            this.mediaEl.append('<a href="'+href+'" id="video-player"></a>');
            if (autoplay) {
                playlist = [ { url: href } ];
            } else {
                playlist = [
                    {
                        url: item.attr('image'),
                        scaling: 'fit',
                        autoPlay: true
                    },
                    {
                        url: href
                    }
                ];
            }
            flowplayer("video-player", media_url + "flowplayer/flowplayer-3.1.2.swf", {
                clip: {autoPlay: autoplay},
                playlist:  playlist
            });
        },

        showVimeoVideo: function(item, autoplay) {
            var data = item.data('video'),
                href = 'https://player.vimeo.com/video/'+ data.video_id + '?title=0&byline=0&portrait=0';
            if (autoplay) {
                href = href + '&autoplay=1';
            }

            var width = this.layout === 'hd' ? 855 : 640,
                height = 480;
            var iframe = $('<iframe frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>')
                            .attr('src', href)
                            .attr('width', width)
                            .attr('height', height);
            iframe.appendTo(this.mediaEl);
        },

        showImage: function(i) {
            var item = this.image_links.eq(i);
            this.mediaEl.empty();
            var image = item.clone(false),
                href = item.attr('href');
            image.attr('target', '_blank').find("img").attr('src', href);
            this.mediaEl.append(image);
            $('<div class="caption"></div>').html(
                item.attr('caption')
            ).appendTo(this.mediaEl);

            this.video_links.removeClass('active');
            this.image_links.removeClass('active').eq(i).addClass('active');
        }
    };
})(jQuery);
