// ----------------------------------------------------------------------------
// Mark It Up! Universal MarkUp Engine, JQuery plugin
// v 1.0.0 beta
// Dual licensed under the MIT and GPL licenses.
// ----------------------------------------------------------------------------
// Copyright (C) 2007 Jay Salvat
// http://markitup.jaysalvat.com/
// ----------------------------------------------------------------------------
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// ----------------------------------------------------------------------------
(function($) {

    $.markItUp = function() {
        return this.init.apply(this, arguments);
    };

    $.markItUp.prototype = {
        options : {	
            nameSpace:				"",
			previewIFrame:			true,
			previewBodyId:			"",
			previewBodyClassName:	"",
			previewBodyCss:			"",
			previewParserPath:		"",
			previewParserVar:		"data",
            preInit:                "",
            postInit:               "",
			beforeInsert:			"",
			afterInsert:			"",
			placeHolder:			"",
			onEnter:			 	{},
			onShiftEnter:		 	{},
			onCtrlEnter:		 	{},
			onTab:				 	{},
			markupSet:			[	{} ]						
		},

		// init and build editor
        init : function(textArea, settings, extraSettings) {
            var self = this;
            $.extend(self.options, settings, extraSettings);
            self.textArea = textArea;
            self.ctrlKey = false;
            self.shiftKey = false;
            self.altKey = false;
            self.levels = [];
			self.iFrame = false;
			self.scrollPosition = 0;
			self.caretPosition = 0; 
			self.caretOffset = -1;
			self.clicked = null;
            self.header = null;
            self.footer = null;
           
            // preInit callback
            if ($.isFunction(self.options.preInit)){
                self.options.preInit(self);
            }

            var options = self.options
            var $$ = $(textArea);
            $$.attr("id", "").addClass("markItUpEditor");	
            $$.wrap('<span class="'+options.nameSpace+'"></span>');	
            $$.wrap('<div id="'+($$.attr("id")||"")+'" class="markItUp"></div>');		
            $$.wrap('<div class="markItUpContainer"></div>');

            // add the header before textarea
            self.header = $('<div class="markItUpHeader"></div>').insertBefore($$);
            $(self.dropMenus(options.markupSet)).appendTo(self.header);
            
            // add the resize handle after textarea
            resizeHandle = $('<div class="markItUpResizeHandle"></div>')
                .insertAfter($$)
                .bind("mousedown", function(e) {
                    var h = $$.height();
                    var y = e.clientY;
                    var mouseMove = function(e) {
                        $$.css("height", Math.max(20, e.clientY+h-y)+"px");
                    };
                    var mouseUp = function(e) {
                        $("html").unbind("mousemove", mouseMove).unbind("mouseup", mouseUp);
                    };
                    $("html").bind("mousemove", mouseMove).bind("mouseup", mouseUp);
            });
            //resizeHandle.wrap('<div class="markItUpFooter"></div>');
            self.footer = $('<div class="markItUpFooter"></div>').insertAfter($$);
            self.footer.append(resizeHandle);
            
            // listen key events
            $$.keydown(function(e){ return self.keyPressed(e)}).
               keyup(function(e){ return self.keyPressed(e)});
            
            // bind an event to catch external calls
            $$.bind("insertion", function(e, settings) {
                if (settings.target != false) {
                    self.get();
                }
                if (textArea == self.focused) {
                    self.markup(settings);
                }
            });
            
            // remember the last focus
            $$.focus(function() {
               self.focused = this;
            });

            // postInit callback
            if ($.isFunction(self.options.postInit)){
                self.options.postInit(self);
            }
        },

        // recursively build header with dropMenus from markupset
        dropMenus: function(markupSet) {
            var self = this;
            var ul = $("<ul></ul>"), i = 0;
            $("li:hover > ul", ul).css("display", "block");
            $(markupSet).each(function() {
                var button = this, t = ""; 
                var title = (button.key) ? " [Ctrl+"+button.key+"]" : "";
                if (button.separator) {
                    li = $('<li class="'+(button.className||"")+' markItUpSeparator">'+(button.separator||"")+'</li>').appendTo(ul);							
                } else {
                    i++;
                    for (n = self.levels.length, j = n-1; j >= 0; j--) {
                        t += self.levels[j]+"-";
                    }
                    li = $('<li class="'+(button.className||"")+' markItUpButton markItUpButton'+t+(i)+'"><a href="#" accesskey="'+(button.key||"")+'" title="'+(button.name+title||"")+'">'+(button.name||"")+'</a></li>')
                    .click(function(e) { 
                        self.shiftKey = e.shiftKey;
                        self.altKey = e.altKey;
                        self.ctrlKey = e.ctrlKey;
                        if (button.call) {
                            func = ( $.isFunction(button.call) )? button.call : eval('self.'+button.call) ;
                            func.call(self);
                        }
                        self.markup(button);
                        return false;	
                    }).hover(function() {
                            $("> ul", this).show();
                            //$(this).find("> ul").show();
                        }, function() {
                            $("> ul", this).hide();
                            //$(this).find("> ul").hide();			
                        }
                    ).appendTo(ul);	
                    if (button.dropMenu) {
                        self.levels.push(i);
                        $(li).addClass("markItUpDropMenu").append(dropMenus(button.dropMenu));
                    }
                }
            }); 
            self.levels.pop();
            return ul;
        },

        // markItUp markups
        magicMarkups : function(string) {
            var self = this;
            if (string) {
                string = string.toString();
                // $!(alternative)!, $!(default|!|alternative)!
                string = string.replace(/\$\!\((.*?)\)\!/gm,
                    function(x, a) {
                        b = a.split(/\|\!\|/gm);
                        if (self.altKey == true) {
                            return (b[1] != undefined) ?  b[1] : b[0];
                        } else {
                            return (b[1] == undefined) ? "" : b[0];
                        }														
                    });
                // $!{prompt}!, $!{prompt=value}!
                string = string.replace(/\$\!\{(.*?)\}\!/gm, 
                    function (a) { 
                        b = a.replace(/(\$\!\{|\}\!)/gm, "").split("="); 
                        return prompt(b[0], (b[1]) ? b[1] : ""); 
                    });	
                return string;
            }
            return "";
        },

        // prepare action
        prepare : function (action)	{
            var self = this;
            if ($.isFunction(action)) action = action.call(self);
            return self.magicMarkups(action);
        },
        
        // build block to insert
        build : function(string)	{
            var self = this;
            var options = self.options;
            var openWith 	= self.prepare(self.clicked.openWith);
            var placeHolder = self.prepare((!self.clicked.placeHolder) ? options.placeHolder : self.clicked.placeHolder);
            var replaceWith = self.prepare(self.clicked.replaceWith); 
            var closeWith 	= self.prepare(self.clicked.closeWith);
            
            if (replaceWith != "") {
                block = openWith + replaceWith + closeWith;				
            } else if (self.selection == "" && placeHolder != "" && (openWith != "" && closeWith != ""))	{
                block = openWith + placeHolder + closeWith;
            } else {
                block = openWith + (string||self.selection) + closeWith;								
            }
            
            return {	block:		block, 
                        openWith:	openWith, 
                        replaceWith:replaceWith, 
                        placeHolder:placeHolder,
                        closeWith:	closeWith
                    };		
        },
        
        // define markup to insert
        markup : function(button) {	
            var self = this;
            var $$ = $(self.textArea);
            var options = self.options;
            self.clicked = button;
            self.get();
            
            // callbacks before insertion
            self.prepare(options.beforeInsert);
            self.prepare(self.clicked.beforeInsert);
            if (self.ctrlKey == true && self.shiftKey == true) {
                self.prepare(self.clicked.beforeMultiInsert);
            }
            
                        
            if (self.ctrlKey == true && self.shiftKey == true) {                
                lines = self.selection.split((($.browser.mozilla) ? "\n" : "\r\n"));
                for (j = 0, n = lines.length, i = 0; i < n; i++) {
                    if ($.trim(lines[i]) != "")	{
                    lines[i] = self.build(lines[i]).block;
                    } else {
                        lines[i] = "";
                    }
                }
                string = { block:lines.join("\n")};
                start = self.caretPosition;
                len = string.block.length + (($.browser.opera) ? n : 0);				
            } else if (self.ctrlKey == false) {
                string = self.build(self.selection);
                start = self.caretPosition + string.openWith.length;
                len = string.block.length - string.openWith.length - string.closeWith.length;
            } else if (self.shiftKey == true) {
                string = self.build(self.selection);
                start = self.caretPosition;
                len = string.block.length;			
            } else {
                string = self.build(self.selection);
                start = self.caretPosition + string.block.length ;	
                len = 0;
            }
            if ((self.selection == "" && string.replaceWith == "")) { 
                if ($.browser.opera) { // opera bug fix
                    self.caretPosition += (string.block.length - string.block.replace(/^\n*/g, "").length);
                }
                start = self.caretPosition + string.openWith.length;
                len = string.block.length - string.openWith.length - string.closeWith.length;
                self.caretOffset = $$.val().substring(self.caretPosition,  $$.val().length).length;
            }
            
            
            // do job
            self.insert(string.block);
            self.set(start, len);
            self.get();
            
            
            // callbacks after insertion
            if (self.ctrlKey == true && self.shiftKey == true) {
                self.prepare(self.clicked.afterMultiInsert);
            }
            self.prepare(self.clicked.afterInsert);
            self.prepare(options.afterInsert);
            
            // refresh preview if opened
            if (self.iFrame) self.refreshPreview();
        },

        // add markup
        insert : function(block) {
            var self = this;
            var $$ = $(self.textArea);
            if (document.selection) {					
                newSelection = document.selection.createRange();
                newSelection.text = block;
            } else { 
                $$.val($$.val().substring(0, self.caretPosition) + block + $$.val().substring(self.caretPosition + self.selection.length, $$.val().length));
            }
        },
        
        // set a selection
        set : function(start, len) {
            var self = this;
            if (self.textArea.createTextRange){
                range = self.textArea.createTextRange();
                range.collapse(true);
                range.moveStart("character", start); 
                range.moveEnd("character", len); 
                range.select();
            } else if (self.textArea.setSelectionRange ){
                self.textArea.setSelectionRange(start, start + len);
            }
            self.textArea.scrollTop = self.scrollPosition;
            self.textArea.focus();	
        },

        // get the selection
        get: function() {
            var self = this;
            var $$ = $(self.textArea);
            self.textArea.focus();
            self.scrollPosition = self.textArea.scrollTop;
            if (document.selection) {		
                selection = document.selection.createRange().text;
                if ($.browser.msie) { // ie
                    var range = document.selection.createRange();
                    var rangeCopy = range.duplicate();
                    rangeCopy.moveToElementText(self.textArea);
                    caretPosition = -1;
                    while(rangeCopy.inRange(range)) { // fix most of the ie bugs with linefeeds...
                        rangeCopy.moveStart("character");
                        ++caretPosition;
                    }					
                } else { // opera
                    caretPosition = self.textArea.selectionStart;
                }
            } else { // gecko
                caretPosition = self.textArea.selectionStart || 0;
                selection = $$.val().substring(caretPosition, self.textArea.selectionEnd);
            } 
            self.selection = selection;
            self.caretPosition = caretPosition;
            return selection;
        },
        
        // open preview window
        preview : function() {
            var self = this;
            var options = self.options;
            if (self.iFrame == false) {
                //height = $$.css("height") || $$.height();
                if (options.previewIFrame == true) {
                    //iFrame = $('<iframe class="markItUpPreviewFrame" style="height:'+height+'"></iframe>').insertAfter($$).show();
                    self.iFrame = $('<iframe class="markItUpPreviewFrame"></iframe>').insertAfter(self.footer).show();
                    self.previewWin = self.iFrame[self.iFrame.length-1].contentWindow || frame[self.iFrame.length-1];
                } else {
                    //width = $$.css("width") || $$.width();
                    //win = window.open("", "preview", "resizable=yes, scrollbars=yes, width="+width+", height="+height);
                    self.previewWin = window.open("", "preview", "resizable=yes, scrollbars=yes");
                }
            } else {
                if (self.altKey && self.iFrame) {
                    self.refreshPreview();						
                } else {
                    self.iFrame.remove(); 
                    self.iFrame = false;						
                }
            }
        },

        // refresh Preview window
        refreshPreview : function() {
            var self = this;
            var $$ = $(self.textArea);
            var options = this.options;
            var content;
            if (options.previewParserPath != "") {
                $.ajax({ 
                    type: "POST", 
                    async: false,
                    url: options.previewParserPath, 
                    data: options.previewParserVar+"="+$$.val(), 
                    success: function(data) { content = data; }
                });
            } else {
                content = $$.val();
            }
            var style_def;
            if (typeof options.previewBodyCss == 'string') {
                style_def = '<link href="'+options.previewBodyCss+'" rel="stylesheet" type="text/css" />\n';
            } else {
                style_def = '';
                $.each(options.previewBodyCss, function(i, href) {
                    style_def += '<link href="'+href+'" rel="stylesheet" type="text/css" />\n';
                });
            }
            html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
            html+= '<html>\n<head>\n'+style_def+'</head>\n';
            html+= '<body id="'+options.previewBodyId+'" style="padding: 10px;" class="'+options.previewBodyClassName+'">\n';
            html+= '<div id="wrap"><div id="content">'+content+'</div></div>';
            html+= '</body></html>';
            self.previewWin.document.open();
            self.previewWin.document.write(html);
            self.previewWin.document.close();
        },

        // set keys pressed
        keyPressed : function(e) { // safari and opera don't fire event on shift, control and alt key properly
            var self = this;
            var $$ = $(self.textArea);
            ctrlKey = e.ctrlKey; 
            shiftKey = e.shiftKey;
            altKey = e.altKey;
            options = self.options;

            if (e.type == "keydown") {	
                    if (ctrlKey) {
                    a = $("a[accesskey="+String.fromCharCode(e.keyCode)+"]", self.header);
                    if (a.length != 0) {
                        ctrlKey = false;
                        a.parent("li").trigger("click");
                        e.preventDefault(); 
                        e.stopPropagation(); 
                        return false;
                    }						
                }
                if (e.keyCode == 13 ||  e.keyCode == 10) { // Enter key
                    if (ctrlKey == true) {  // Enter + Ctrl
                        ctrlKey = false;
                        self.markup(options.onCtrlEnter);	
                        return options.onCtrlEnter.keepDefault;
                    } else if (shiftKey == true) { // Enter + Shift
                        shiftKey = false;
                        self.markup(options.onShiftEnter);	
                        return options.onShiftEnter.keepDefault;
                    } else { // only Enter
                        self.markup(options.onEnter);	
                        return options.onEnter.keepDefault;
                    }
                }
                if (e.keyCode == 9) { // Tab key
                    if (self.caretOffset != -1) {
                        self.get();
                        self.caretOffset = $$.val().length - self.caretOffset;
                        self.set(self.caretOffset, 0);
                        self.caretOffset = -1;
                        return false;
                    } else {
                        self.markup(options.onTab);	
                        self.caretOffset = -1;
                        return options.onTab.keepDefault;							
                    }
                }
            }
        }
        
    };
	$.fn.markItUp = function(settings, extraSettings) {		 
		return this.each(function() {		
		    new $.markItUp(this, settings, extraSettings);
		});		
	};

})(jQuery);
