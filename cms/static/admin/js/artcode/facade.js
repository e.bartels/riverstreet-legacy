
var Facade = function() {
    this.init.apply(this, arguments);
}
Facade.prototype = {
    options: {
        previewContainer: '#preview'
    },

    init: function () {
        var self = this;
        var o = self.options
        self.preview = new Facade.Preview(o.previewContainer);

        // Form Events
        $('#docTypeSelector').change(function() {
            self.preview.setDocType(this.value);
        });
        $('#layoutSelector').change(function() {
            self.preview.setLayoutType(this.value);
        });

        var $inputs = $('.style-form input[type!=hidden], .style-form select').change(function() {
            var selector = $(this).prev().attr('selector');
            var property = $(this).prev().attr('property');
            var val = $(this).val();
            self.preview.updateStyle(selector, property, val);
        });
        
        $(window).load(function() {
            $inputs.trigger('change');
            $('#docTypeSelector').trigger('change');
            $('#layoutSelector').trigger('change');
        });
    }
}

Facade.Preview = function() {
    this.init.apply(this, arguments);
}
Facade.Preview.prototype = {
    init: function(container) {
        var self = this;
        self.container = container;
        self.iframe = $('<iframe src="/admin/visual/preview/" ></iframe>');
        $(self.container).append(self.iframe);
        $(window).resize(function() {
            $(self.container).height( $(window).height() - 82);
        }).resize();

        $(self.iframe).load(function() {
            $(this).contents().find('a').attr('href', '#').click(function(){ return false; });
        });
    },

    getBodyDiv: function() {
        return this.iframe.contents().find('body > div').eq(0);
    },

    setDocType: function(yuiDoc) {
        var self = this;
        var $$ = self.getBodyDiv();
        $$.attr('id', yuiDoc);
    },

    setLayoutType: function(yuiTemplate) {
        var self = this;
        var $$ = self.getBodyDiv();
        $$.attr('class', yuiTemplate);
        var $sidebar = $$.find('#sidebar');
        if (yuiTemplate == 'yui-t7') { 
            $sidebar.hide();
        }
        else {
            if ($sidebar.length < 1) {
                $sidebar = $('<div id="sidebar" class="yui-b"></div>');
                $sidebar.append('<div class="container"></div>');
                self.iframe.contents().find('#bd').append($sidebar);
            }
            $sidebar.show();
        }
    },

    updateStyle: function(selector, property, val) {
        var self = this;
        
        //console.log([selector, property, val]);

        if (selector[0] != '#' && selector[0] != '.' && selector != 'body')
            $$ = self.iframe.contents().find('#bd');
        else 
            $$ = self.iframe.contents();

        if (/:hover/.test(selector)) {
            selector = selector.replace(/:hover/, '');
            var saved_prop;
            $$.find(selector).hover(function() {
                saved_prop = $$.find(selector).css(property);
                $(this).css(property, val);
            }, function() {
                $(this).css(property, saved_prop);
            });
            return;
        }

        $$.find(selector).css(property, val);
    },

    toggleFullscreen: function() {
        var self = this;
        if (! self.fullscreen) {
            self.iframe.addClass('fullscreen');
            self.fullscreen = true;
        }
        else {
            self.iframe.removeClass('fullscreen');
            self.fullscreen = false;
        }
    }
}
