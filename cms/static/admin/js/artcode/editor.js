
var mceEditor = {
    /* Basic */
    mode : "textareas",
    editor_selector : "mceEditor",
    width: '70%',
    accessibility_warnings : false,
    button_tile_map : true,
    browsers : "msie,gecko,safari,opera",
    gecko_spellcheck : true,
    dialog_type : "modal",

    /* Theme */
    theme : "advanced", 
    theme_advanced_toolbar_location:"top",
	theme_advanced_toolbar_align : "left",
    theme_advanced_buttons1 : 'bold,italic,underline,strikethrough,separator,bullist,numlist,outdent,indent,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,link,unlink,image,separator,formatselect,styleselect,separator,code,removeformat,separator,fullscreen',
    theme_advanced_buttons2 : '',
    theme_advanced_buttons3 : '',
    theme_advanced_path: false,
    theme_advanced_path_location : "bottom",
    theme_advanced_resizing : true,
    theme_advanced_resize_horizontal : false,
    theme_advanced_statusbar_location : 'bottom',
    
    /* Cleanup */
    cleanup : true, cleanup_on_startup : false,
    apply_source_formatting : true,
    convert_fonts_to_spans : true,
    convert_newlines_to_brs : false,
    doctype: '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">',
    content_css: media_url+'admin/css/artcode/editor_content.css',
    verify_css_classes : true,    
    remove_linebreaks: false,
    fix_list_elements: true,    
    remove_trailing_nbsp : true,
    verify_html : true,
    relative_urls : false, 
   
    /* Plugins */
    plugins : "autosave,fullscreen,safari",
    fullscreen_settings: {theme_advanced_path: false},
    file_browser_callback : 'myFileBrowser'
};

var mceEditorFull = $.extend({}, mceEditor, {
    theme_advanced_buttons1 : 'bold,italic,underline,strikethrough,separator,bullist,numlist,outdent,indent,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,formatselect,styleselect,separator,link,unlink,image,|,fullscreen',
    theme_advanced_buttons2 : 'tablecontrols,separator,code,removeformat',
    verify_html : false,
    plugins : "autosave,fullscreen,safari,table"
});

var mceEditorSimple = $.extend({}, mceEditor, {
    editor_selector : "mceEditorSimple",   
    theme_advanced_buttons1 : 'bold,italic,underline,strikethrough,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,styleselect,separator,link,unlink,separator,code,removeformat',
    force_br_newlines : true,
    force_p_newlines: false
});

var mceEditorCustom = $.extend({}, mceEditorSimple, {
    editor_selector : 'mceEditorCustom',
    theme_advanced_buttons1 : 'bold,italic,underline,strikethrough,forecolor,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,|,removeformat'
});

tinyMCE.init(mceEditor);
tinyMCE.init(mceEditorSimple);
tinyMCE.init(mceEditorCustom);



/*   File Browser */

function myFileBrowser (field_name, url, type, win) {
    //alert("Field_Name: " + field_name + "\nURL: " + url + "\nType: " + type + "\nWin: " + win); // debug/testing
    var searchString = window.location.search;
    var popup_location = (type == "image" ? admin_root + "media/image/file_browser/" : admin_root + "media/link_browser/") + '?_popup=1&type='+type;
    
    tinyMCE.activeEditor.windowManager.open({
            title: "File Browser",
            file: popup_location,
            width: '820',
            height: '600',
            close_previous: 'no',
            resizable : 'yes',
            scrollbars : 'yes',
            inline : 'yes'
        }, {
            window : win,
            input : field_name
    });    
    return false;
}

function image_manager_callback( open_win, url ) {
    //open_win refers to the window which called this function i.e. my image browser
    var win = open_win.tinyMCEPopup.getWindowArg("window") ;
                // using the "getWindowArg" function gives you the reference to your Insert Image window
    if( win ) {
        var doc = win.document ;
        if( doc ) {
            var f = doc.forms[0] ;    
            if( f ) {
                // Here comes the actual transfer of your image's url into tinyMCE's dialogue window.
                f.src.value = url;
                // Here comes the function which inserts your selected image into the preview pane
                //win.showPreviewImage(url);
                open_win.close();
            } else {
                alert( "Could not locate the parent form" ) ;
            }
        } else {
            alert( "Could not locate the parent document" ) ;
        }
    } else {
        alert( "Could not locate the parent window" ) ;
    }
}

function file_manager_callback( open_win, url ) {
    //open_win refers to the window which called this function i.e. my image browser
    var win = open_win.tinyMCEPopup.getWindowArg("window") ;
                // using the "getWindowArg" function gives you the reference to your Insert Image window
    if( win ) {
        var doc = win.document ;
        if( doc ) {
            var f = doc.forms[0] ;            
            if( f ) {
                // Here comes the actual transfer of your image's url into tinyMCE's dialogue window.
                f.href.value = url;
                open_win.close();
            } else {
                alert( "Could not locate the parent form" ) ;
            }
        } else {
            alert( "Could not locate the parent document" ) ;
        }
    } else {
        alert( "Could not locate the parent window" ) ;
    }
}

