/*
 * ColorChooser
 * Requires farbtastic.js
 */
var ColorChooser = function() {
    return this.init.apply(this, arguments);
};

ColorChooser.prototype = {
    init: function(obj) {
        var self = this;
        self.input = obj;
        self.chooser = $('<div class="color-chooser"></div>').insertAfter(self.input);
       
        // Set up the chooser
        var $input = $(self.input);
        self.chooser.farbtastic(self.input).css({
            position: 'absolute',
            zIndex: 100,            
        });
        self.close();
        
        // Events
        $input.click( function() {
            self.open();
            self.cancelClose();
        }).hover(function(){}, function() {self.timedClose();});

        $input.bind('farbtasticUpdate', function() {
            $input.trigger('change');
        });

        self.chooser.hover(
            function() {self.cancelClose()},
            function() {self.timedClose()}
        );
    },

    open: function() {
        var self = this;
        self.cancelClose();
        self.chooser.show().addClass('open');
        $(self.input).trigger('openColorChooser');
        
        // Position the chooser widget
        var offset = $(self.input).offset();
        self.chooser.css({
            top: offset.top - (self.chooser.height() / 2 ) + 20,
            left: (offset.left + $(self.input).width() + 20)
        });

        self.timedClose();
    },

    close: function() {
        var self = this;
        self.chooser.hide().removeClass('open');
        $(self.input).trigger('closeColorChooser');
    },

    timedClose: function() {
        var self = this;
        self.timer = window.setTimeout(function() {
            self.close();
        }, 1400);
    },
    cancelClose: function() {
        var self = this;
        window.clearTimeout(self.timer);
    }
}

jQuery.ColorChoosers = []
jQuery.fn.ColorChooser = function() {
    return this.each(function() {
        jQuery.ColorChoosers.push( new ColorChooser(this) );
        jQuery(this).bind('openColorChooser', function() {
            var openChooser = this;
            jQuery.each(jQuery.ColorChoosers, function(i, c) {
                if (c.input != openChooser) c.close();
            });
        });
    });
}

jQuery.fn.SliderInput = function() {
    return this.each(function() {
        var $input = $(this).parent().find('input');
        var value = parseInt($input.attr('start_val'));
        var min = parseInt($input.attr('min'));
        var max = parseInt($input.attr('max'));
        $(this).slider({
            min: min, max: max,
            value: value,
            change: function(event, ui) {
                $(this).parent().find('input').val(ui.value);
            },
            slide: function(event, ui) {
                $(this).parent().find('input').val(ui.value);
            }
        });
    });
}

$(function() {
    $('input.color-input').ColorChooser();
    
    $('.ui-slider').SliderInput();
});
