/*
 * Requires Jquery
 * Javascript for media.admin.widgets.ImageForeignKeyWidget
 * Updates a thumbnail image based on foreign key when text field changes.
 */

var ImageForeignKeyWidget = function() {
    return this.initialize.apply(this, arguments);
};

ImageForeignKeyWidget.prototype = {
    val: null,

    initialize: function (id){
        var self = this;
        self.id = id;
        var $input = $('#' + id);
        self.input = $input[0];
        self.val = $input.val();
        window.setInterval(function() {
            var v1 = self.val;
            var v2 = $input.val();
            if (v1 != v2) {
                self.val = v2;
                self.updateImageForeignKeyWidget(v2);
            }
        }, 1000);
    }, 

    updateImageForeignKeyWidget: function (val) {
        var self = this;
        var $parent = $(self.input).parent();
        var $container = $parent.find('.foreignKeyImage');
        if (!val){ 
            $container.hide();
            return;
        }
        $.get('/admin/fetch_url', {
            model: 'media.Image',
            pk: val
        }, function(url) {
            if (url) {
                url = url.replace(media_url, '');
                if ($container.length < 1) $container = self.createContainer();
                $container.show().find('img').attr('src', '/thumbnails/' + url + '?size=120x120');
                $container.find('a').attr('href', '/admin/media/image/' + val);
            } else {
                $container.hide();
            }
        });
    },

    createContainer: function() {
        var self = this;
        var $$ = $(self.input);
        $$.parent().append('<br/>');
        $container = $('<div class="foreignKeyImage"></div>').appendTo($$.parent());
        $container.append('<label>&nbsp;</label').append('<a href="" target="_blank"><img src="" alt="" /></a>');
        return $container;
    }
};


/*
 * Javascript for content.admin.widgets.InternalURLWidget
 * Provides a callback, so the link_browser component
 * can return the form field.
 */
var InternalURLWidget = function() {
    return this.initialize.apply(this, arguments);
};

InternalURLWidget.prototype = {
    initialize: function(id) {
        var self = this;
        self.id = id;
        var $input = $('#' + id);
        self.input = $input[0];
        $input.nextAll('a').click(function() {return self.openPopup(this);});
        window.file_manager_callback = self.callback;
    },

    openPopup: function(link) {
        var self = this;
        var size = '820x600';
        var href= link.href + '?pop=1'
        var name = link.rel;
        size = size.split('x');
        var w = window.open(href, name, 'width='+size[0]+',height='+size[1] + ',scrollbars=1');
        if (window.focus) {w.focus()}
        return false;
    },

    callback: function(window, file) {
        var name = window.name;
        var input_id = '#id_' + name.replace('lookup_','');
        var $input = $(input_id);
        $input.val(file);
        window.close();
    }
};

