// Generate 32 char random id 
function gen_id() {
    var id = ""
    for (var i=0; i < 32; i++) {
        id += Math.floor(Math.random() * 16).toString(16); 
    }
    return id
}

$(function() {
    // Form Validation for Required Items on multipart upload forms.
    var form_valid = true;
    $('form[enctype=multipart/form-data]').submit(function() {
        return true;

        form_valid = true;
        $('label.required', this).each(function() {
            // Skip inline formsets so the empty 
            // fields don't trigger validation error.
            if ($(this).parents('.inline-group').length) {
                return;
            }
            var $parent = $(this).parent();
            var $$ = $parent.find(':input');
            if ($$.attr('type') != 'file') {
                if (! $$.val()) {
                    form_valid = false;
                    if ( ! $('ul.errorlist',$parent).length ) {
                        $('<ul class="errorlist"><li>This field is required.</li></ul>')
                            .prependTo($parent);
                    }
                }
            }
        });
        if (!form_valid) return false;
    });
    if (!form_valid) return false;

    // Add upload progress for multipart forms.
    $('form[enctype=multipart/form-data]').submit(function(){ 
        // First make sure we're actually uploading something.
        var has_upload_data = false;
        $('input[type=file]',this).each(function() {
            if (this.value) has_upload_data = true;
        });
        if (!has_upload_data) return true;

        // Prevent multiple submits
        if ($.data(this, 'submitted')) return false;

        var freq = 5000; // freqency of update in ms
        var id = gen_id(); // id for this upload so we can fetch progress info.
        var progress_url = '/admin/upload_progress/'; // ajax view serving progress info

        // Append X-Progress-ID id form action
        this.action += (this.action.indexOf('?') == -1 ? '?' : '&') + 'X-Progress-ID=' + id;
        
        var $progress = $('<div id="upload-progress" class="upload-progress"></div>').
            appendTo(document.body).append('<div class="progress-container"><span class="progress-info">uploading 0%</span><div class="progress-bar"></div></div>');
        
        // progress bar position
        $progress.show();
        $progress.css({
            position: ($.browser.msie && $.browser.version < 7 )? 'absolute' : 'fixed',  
            left: '50%', bottom: '20%'
        }).css({
            marginLeft: 0-($progress.width()/2)
        });

        // Update progress bar
        function update_progress_info() {
            $progress.show();
            $.getJSON(progress_url, {'X-Progress-ID': id}, function(data, status){
                if (data) {
                    var progress = parseInt(data.uploaded) / parseInt(data.length);
                    var width = $progress.find('.progress-container').width()
                    var progress_width = width * progress;
                    $progress.find('.progress-bar').width(progress_width);
                    $progress.find('.progress-info').text('uploading ' + parseInt(progress*100) + '%');
                }
                window.setTimeout(update_progress_info, freq);
            });
        };
        window.setTimeout(update_progress_info, freq);

        $.data(this, 'submitted', true); // mark form as submitted.
    });
});


$(function() {
    // Popups
    $('a.popup').click(function(e){
        var size = this.rel || '800x600';
        var href= this.href.split('?'); 
        href[1] = href[1] + '&_popup=1';
        href = href.join('?');
        var name = this.name || null;
        size = size.split('x');
        var w = window.open(href, name, 'width='+size[0]+',height='+size[1] + ',scrollbars=1');
        if (window.focus) {w.focus()}
        return false;
    });

    // Messages
    window.setTimeout(function() {
        $('ul.messagelist').animate({
            opacity: 0
        }, 1000, function() {
            $(this).slideUp('slow');
        });
    }, 5000);
});
