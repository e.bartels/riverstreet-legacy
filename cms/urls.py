from django.conf.urls.defaults import *
from django.contrib import admin
from django.conf import settings

from cms import config
from cms import admin as acadmin


urlpatterns = patterns('',
    # STATIC MEDIA For Development Server
    (r'^static/(?P<path>.*)$', 'django.views.static.serve', 
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),

    # Home Page
    #url(r'^$', 'django.views.generic.simple.direct_to_template', {'template': 'index.html'}, name='homepage'),
    #url(r'^$', 'cms.apps.content.views.home', name='homepage'),
    url(r'^$', 'cms.apps.content.views.collections.splash', name='homepage'),

    # Content App
    (r'^collections/', include('cms.apps.content.urls.collections')),
    (r'^pages/', include('cms.apps.content.urls.pages')),
    (r'^news/', include('cms.apps.content.urls.news')),
    (r'^media/', include('cms.apps.media.urls')),

    # Accounts
    (r'^clients/', include('cms.apps.accounts.urls')),
    (r'^clientpages/', include('cms.apps.clientpages.urls')),

    # Menus
    (r'^index/', include('cms.apps.menus.urls')),

    # Blog App
    (r'^blog/', include('cms.apps.blog.urls')),

    # Upload Progress
    (r'^upload_progress/$', 'cms.apps.utils.admin_views.upload_progress'),

    # Admin:
    (r'^admin/settings/', include('cms.apps.configuration.urls')),
    (r'^admin/media/', include('cms.apps.media.admin.urls')),
    (r'^admin/fetch_url', 'cms.apps.utils.admin_views.fetch_url'),
    (r'^admin/upload_progress/$', 'cms.apps.utils.admin_views.upload_progress'),
    #(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    (r'^admin/', include(admin.site.urls)),


    # Client Pages
    (r'^(?P<section_slug>[\w-]+)/', include('cms.apps.clientsections.urls')),
)
