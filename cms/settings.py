import os
from settings_default import *  # noqa

# Make this unique, and don't share it with anybody.
SECRET_KEY = os.environ.get('SECRET_KEY', '')

DEBUG = os.environ.get('DEBUG', 'True') == 'True'
TEMPLATE_DEBUG = DEBUG
THUMBNAIL_DEBUG = DEBUG

ADMINS = (
    ('Eric Bartels', 'ebartels@gmail.com'),
)
MANAGERS = ADMINS

# Database Settings
DATABASE_ENGINE = 'mysql'
DATABASE_NAME = os.environ.get('DATABASE_NAME', 'riverstreet')
DATABASE_HOST = os.environ.get('DATABASE_HOST', 'localhost')
DATABASE_USER = os.environ.get('DATABASE_USER', '')
DATABASE_PASSWORD = os.environ.get('DATABASE_PASSWORD', '')
DATABASE_PORT = os.environ.get('DATABASE_PORT', '')

# Email settings
DEFAULT_FROM_EMAIL = 'Riverstreet Productions Inc. <mailer@artcodehost.io>'
SERVER_EMAIL = DEFAULT_FROM_EMAIL
EMAIL_SUBJECT_PREFIX = '[riverstreet.net]'

EMAIL_HOST = os.environ.get('EMAIL_HOST', '')
EMAIL_PORT = os.environ.get('EMAIL_PORT', '')
EMAIL_USE_TLS = os.environ.get('EMAIL_USE_TLS', 'True') == 'True'
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER', '')
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD', '')

# These not used anymore, but keeping them since the code still exists
FFMPEG_PATH = 'ffmpeg'
QTFASTSTART_PATH = 'qt-faststart'
