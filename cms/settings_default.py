# Django settings for cms project.
import os
from django.conf import global_settings

DEBUG = True
TEMPLATE_DEBUG = DEBUG

PROJECT_DIR = os.path.dirname(os.path.abspath(__file__)) # Path containing this module

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Los_Angeles'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.abspath(os.path.join( PROJECT_DIR, 'static')) + "/"

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/static/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
#    'django.template.loaders.eggs.load_template_source',
)
TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.doc.XViewMiddleware',
    'cms.apps.thumbdrive.middleware.ThumbdriveMiddleware',
    'cms.apps.utils.middleware.AJAXSimpleExceptionResponse',
)

ROOT_URLCONF = 'cms.urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.abspath(os.path.join(PROJECT_DIR, 'templates')),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.markup',
    'django.contrib.humanize',
    'django.contrib.admin',
    #'django.contrib.admindocs',

    'mptt',
    'sorl.thumbnail',
    'tagging',

    'cms.apps.utils',
    'cms.apps.menus',
    'cms.apps.caching',
    'cms.apps.configuration',
    'cms.apps.content',
    'cms.apps.media',
    'cms.apps.blog',
    'cms.apps.accounts',
    'cms.apps.clientpages',
    'cms.apps.clientsections',
    'cms.apps.thumbdrive',
)

TEMP_DIR = os.path.abspath(os.path.join(PROJECT_DIR, '..', 'tmp'))

# Caching
CACHE_TIMEOUT = 60*5
CACHE_BACKEND = "file://{}?timeout={}".format(
    os.path.join(TEMP_DIR, 'django_cache'),
    CACHE_TIMEOUT,
)

# Thumbnail settings for sorl.thumbnail
THUMBNAIL_PREFIX = '_thumb_'
THUMBNAIL_SUBDIR = '_thumbs'
THUMBNAIL_QUALITY = 85
THUMBNAIL_DEBUG = False

# File Uploads
FILE_UPLOAD_TEMP_DIR = TEMP_DIR
FILE_UPLOAD_HANDLERS = ('cms.apps.utils.upload.fileuploadhandler.UploadProgressCachedHandler', ) + \
        global_settings.FILE_UPLOAD_HANDLERS

# Default Markup Filter for cms.apps.utils.markup
MARKUP_FILTER = ('markdown', {})

# Login urls
LOGIN_URL = '/clients/login/'
LOGOUT_URL = '/clients/logout/'
LOGIN_REDIRECT_URL = '/clients/'
