from django.contrib.syndication.feeds import Feed
from django.core.urlresolvers import reverse

from cms.apps.blog.models import Entry 
from cms.apps.configuration import config_value

class LatestEntries(Feed):
    link = "/blog/feeds/latest/"
    description_template = "blog/feeds/latest_description.html"

    def items(self):
        return Entry.objects.recent()

    def item_pubdate(self, item):
        return item.pub_date

    def title(self):
       return config_value('SITE', 'TITLE') + ': Latest blog entries'

    def description(self):
        return 'Latest blog entries.'
    
    def link(self):
        return reverse('blog-feed', kwargs={'url': 'latest'})

    def item_categories(self, item):
        categories = item.categories.all()
        return (category.name for category in categories)
