from django.contrib import admin
from django import forms
from django.contrib.auth.models import User
from django.contrib.admin.widgets import RelatedFieldWidgetWrapper

from cms.apps.blog.models import Entry, Category, Link
from cms.apps.utils.forms.widgets import MDTextarea

class EntryAdmin(admin.ModelAdmin):
    model = Entry
    list_display = ('title', 'pub_date', 'author', 'created', 'modified', 'published',)
    list_filter = ('categories', 'published', 'author', 'pub_date')
    search_fields = ('title', 'body')
    date_hierarchy = 'pub_date'
    ordering = ('-pub_date',)
    #prepopulated_fields = {'slug' : ('title',)}
    fieldsets = (
        (None, {
            'fields': ('title', 'author', ('pub_date', 'published'), 'body',)
        }),
        (None, {
            'fields': ('categories', 'tagfield')
        }),
    )
    raw_id_fields = ('related_entries',)

    def formfield_for_dbfield(self, db_field, **kwargs):
        # Make content field an mceEditor instance
        if db_field.name == 'body':
            kwargs.pop('request')
            kwargs['widget'] = MDTextarea(attrs={'rows': '15'})
            return db_field.formfield(**kwargs)
        
        if db_field.name == 'title' or db_field.name == 'slug':
            kwargs.pop('request')
            kwargs['widget'] = forms.widgets.TextInput(attrs={'size':'60'})
            return db_field.formfield(**kwargs)
        
        if db_field.name == 'tags':
            kwargs.pop('request')
            kwargs['widget'] = forms.widgets.TextInput(attrs={'size':'80'})
            return db_field.formfield(**kwargs)
        
        return super(EntryAdmin, self).formfield_for_dbfield(db_field, **kwargs)

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        # Override author field. If not a superuser, then only allow that user
        # to select themselves as  author.
        if db_field.name == 'author':
            if request:
                if not request.user.is_superuser:
                    kwargs['queryset'] = User.objects.filter(pk=request.user.pk)
                kwargs['empty_label'] = None
                kwargs['initial'] = request.user.pk
        return super(EntryAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


class CategoryAdmin(admin.ModelAdmin):
    model = Link
    ordering = ('name',)

class LinkAdmin(admin.ModelAdmin):
    model = Link
    list_display = ('url', 'title',)
    ordering = ('title', )

admin.site.register(Entry, EntryAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Link, LinkAdmin)
