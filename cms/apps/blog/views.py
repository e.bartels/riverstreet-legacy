from django import http
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.generic.date_based import object_detail, archive_month, archive_day
from django.core.paginator import QuerySetPaginator, InvalidPage

from cms.apps.blog.models import Entry


def blog_index(request):
    tag = request.GET.get('tag')
    if tag:
        entries = Entry.tagged.with_all(tag)
    else:
        entries = Entry.objects.published()

    category = request.GET.get('cat')
    if category:
        entries = entries.filter(categories__slug=category)

    try:
        author = int(request.GET.get('author', ''))
    except ValueError:
        author = None
    if author:
        entries = entries.filter(author=author)

    # Pagination
    page = request.GET.get('page', 1)
    try:
        count = int(request.GET.get('count', 5))
    except ValueError:
        count = 5 
    paginator = QuerySetPaginator(entries, count)
    try:
        page = paginator.page(page)
        entries_for_page = page.object_list
    except InvalidPage:
        raise http.Http404


    querydict = request.GET.copy()

    return render_to_response('blog/index.html', {
        'entry_list' : entries_for_page,
        'category': category,
        'tag': tag,
        'author': author,
        'paginator': paginator,
        'page': page,
        'querydict': querydict
    }, context_instance=RequestContext(request))

def day_archive(request, year, month, day):
    entries = Entry.objects.published()

    category = request.GET.get('cat')
    if category:
        entries = entries.filter(categories__slug=category)
    
    return archive_day(request, year, month, day, entries, 'pub_date',
            template_object_name='entry',
            template_name='blog/index.html',
            allow_empty=True,
            extra_context={
                'category': category,
            }
        )

def month_archive(request, year, month):
    entries = Entry.objects.published()
 
    tag = request.GET.get('tag')
    if tag:
        entries = Entry.tagged.with_all(tag)
    else:
        entries = Entry.objects.published()

    category = request.GET.get('cat')
    if category:
        entries = entries.filter(categories__slug=category)

    author = request.GET.get('author')
    if author:
        entries = entries.filter(author=author)

    return archive_month(request, year, month, entries, 'pub_date',
            template_object_name='entry',
            template_name='blog/index.html',
            allow_empty=True,
            extra_context={
                'category': category,
                'tag': tag,
                'author': author
            }
        )

def entry_detail(request, year, month, day, slug):
    entries = Entry.objects.published()
    
    return object_detail(
        request, year, month, day, entries, 'pub_date', 
        slug=slug, 
        template_object_name='entry',
    )
