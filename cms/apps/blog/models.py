import re
from datetime import datetime

from django.utils.encoding import smart_str, smart_unicode
from django.utils.html import strip_tags
from django.contrib.contenttypes import generic
from django.contrib.auth.models import User
from django.db import models

import tagging
from tagging.fields import TagField
from tagging.models import TaggedItem
from cms.apps.utils.fields import AutoSlugField
from cms.apps.media.models import RelatedImagesField
from cms.apps import menus
from cms.apps.blog import config

try:
    import xml.etree.ElementTree as etree
except ImportError:
    from elementtree import ElementTree as etree

class CategoryManager(models.Manager):
    def in_use(self):
        return self.get_query_set().filter(entries__isnull=False).distinct()

class Category(models.Model):
    name = models.CharField(max_length=200) 
    slug = AutoSlugField(max_length=200, unique=True, editable=False, prepopulate_from='name',
            help_text='Unique text identifier used in urls.')

    objects = CategoryManager()

    class Meta:
        verbose_name_plural = 'Categories'

    def __unicode__(self):
        return self.name 

class EntryManager(models.Manager):
    def published(self):
        return self.get_query_set().filter(published=True, pub_date__lte=datetime.now())

    def recent(self, count=10):
        return self.published()[:count]

class Entry(models.Model):
    title = models.CharField(max_length=200)
    slug = AutoSlugField(max_length=200, unique=True, prepopulate_from='title', 
            help_text='Unique text identifier used in urls.')
    author = models.ForeignKey(User)
    pub_date = models.DateTimeField('Date published', default=datetime.today, db_index=True,
            help_text='Date you would like this entry to go live on the site.')
    published = models.BooleanField(default=True, help_text='Select to publish on the site.')
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    categories = models.ManyToManyField(Category, related_name='entries', blank=True)
    related_entries = models.ManyToManyField("self", blank=True, symmetrical=False)

    tagfield = TagField('Tags', help_text='Enter keywords for this image separated by comma.')

    images = RelatedImagesField()

    objects = EntryManager()

    class Meta:
        verbose_name = 'Blog Entry'
        verbose_name_plural = 'Blog Entries'
        ordering = ('-pub_date',)
        get_latest_by = 'pub_date'

    def __unicode__(self):
        return u'%s' % self.title

    @models.permalink
    def get_absolute_url(self):
        args = self.pub_date.strftime("%Y %b %d").split() + [self.slug]
        return ('blog-entry-view', args)

    anchorRe = re.compile(r'''<\s*a\s[^>]*\bhref\s*=\s*["'].*?["']\s*.*?<\s*/a\s*>''',
            re.IGNORECASE| re.MULTILINE| re.DOTALL| re.VERBOSE| re.UNICODE )                    

    @property
    def links(self):
        """
        Get a list of all link (<a> tags) in the entry body.
        """
        links = dict()
        for anchor in self.anchorRe.findall(self.body):
            try:
                element = etree.XML(smart_str(anchor))
                links[element.get('href')] = {
                    'anchor': element.text, 
                    'url': element.get('href'), 
                    'title': element.get('title')
                }
            except:
                pass
        return links.values()

    def get_excerpt(self, trunc=220):
        ustr = smart_unicode(self.body, encoding="utf-8")
        ustr = strip_tags(ustr)
        return ustr[:trunc]
    excerpt = property(get_excerpt)

tagging.register(Entry)

class Link(models.Model):
    url = models.URLField(max_length=255, verify_exists=False)
    title = models.CharField(max_length=255)


    class Meta:
        ordering = ('url', 'id',)

    def __unicode__(self):
        return self.url

# Register URLS with menus app
menus.register_url('Blog', name='blog-index')
