from django.conf.urls.defaults import *

from cms.apps.blog.feeds import LatestEntries

urlpatterns = patterns('cms.apps.blog.views',
    url(r'^$', 'blog_index', name='blog-index'),

    url(r'^(?P<year>\d{4})/(?P<month>\w{3})/$', 'month_archive', name='blog-month-archive'),
    url(r'^(?P<year>\d{4})/(?P<month>\w{3})/(?P<day>\d{1,2})/$', 'day_archive', name='blog-day-archive'),
    url(r'^(?P<year>\d{4})/(?P<month>\w{3})/(?P<day>\d{1,2})/(?P<slug>[\w-]+)/$', 'entry_detail', name='blog-entry-view'),
)


# Feeds
feeds = {
    'latest': LatestEntries
}
urlpatterns += patterns('',
    url(r'^feeds/(?P<url>.*)/$', 'django.contrib.syndication.views.feed', 
        {'feed_dict': feeds}, name='blog-feed'),
)
