from django import template
from django.contrib.auth.models import User

from cms.apps.blog.models import Entry, Link, Category

register = template.Library()

def blog_archive_list():
    """
    An inclusion tag to create a archive menu of previous blog entries.
    """
    dates = Entry.objects.dates('pub_date', 'month', order='DESC')
    
    return {    
        'dates': dates
    }
register.inclusion_tag('blog/includes/archive_list.html')(blog_archive_list)


def blog_link_list():
    """
    An inclusion tag to show list of blog links (blogroll).
    """
    links = Link.objects.all()

    return {
        'links': links
    }
register.inclusion_tag('blog/includes/link_list.html')(blog_link_list)

def blog_category_list():
    """
    An inclusion tag to show list of blog entry categories.
    """
    categories = Category.objects.all()

    return {
        'categories': categories,
    }
register.inclusion_tag('blog/includes/category_list.html')(blog_category_list)

def latest_blog_entries():
    """
    An inclusion tag to show a list of latest blog entries.
    """
    entries = Entry.objects.filter(published=True)[:5]
    return {
        'entries': entries
    }
register.inclusion_tag('blog/includes/latest.html')(latest_blog_entries)

def blog_author_list():
    """
    An inclusion tag to show a list of blog authors.
    """
    authors = User.objects.filter(entry__isnull=False).distinct()
    return {
        'authors': authors
    }
register.inclusion_tag('blog/includes/author_list.html')(blog_author_list)
