from cms.apps.configuration import *
from cms.apps.utils.values import MDTextValue 

BLOG_GROUP = ConfigurationGroup('BLOG', 'Blog Settings', ordering=3)

config_register_list(
    StringValue(BLOG_GROUP,
        'SIDEBAR_TITLE',
        description='Blog Sidebar Title',
        ordering=0,
        default='Meta',
    ),
    MDTextValue(BLOG_GROUP, 
        'SIDEBAR_CONTENT', 
        description='Blog Sidebar',
        help_text='Content for right sidebar area on blog pages.',
        ordering=1,
        default='',
    ),
)

