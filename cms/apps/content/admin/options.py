from django.contrib import admin
from django import forms
from django.template import Template, Context
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect

from cms.apps.utils.forms.widgets import MDTextarea, ColorInput
from cms.apps.content.models import *
from cms.apps.media.admin.widgets import ImageForeignKeyWidget
from extra import TagSelectModelAdmin

class PageAdmin(admin.ModelAdmin):
    model = Page
    list_filter = ('published',  )
    list_display = ('title', 'published', )

    def formfield_for_dbfield(self, db_field, **kwargs):
        # Make content field an mceEditor instance
        if db_field.name == 'text':
            kwargs.pop('request')
            kwargs['widget'] = MDTextarea(attrs={'rows': '15'})
            return db_field.formfield(**kwargs)
        if db_field.name == 'title':
            kwargs.pop('request')
            kwargs['widget'] = forms.widgets.TextInput(attrs={'size':'60'})
            return db_field.formfield(**kwargs)
        if db_field.name == 'menu_color':
            kwargs.pop('request')
            kwargs['widget'] = ColorInput(attrs={'size':'10'})
            return db_field.formfield(**kwargs)
        return super(PageAdmin, self).formfield_for_dbfield(db_field, **kwargs)

class CollectionAdmin(admin.ModelAdmin):
    model = Collection
    exclude = ('sort',)
    list_filter = ('published', 'categories' )
    list_display = ('image_column', 'title', 'published', 'date', 'category_column', )

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'title':
            kwargs.pop('request')
            kwargs['widget'] = forms.TextInput(attrs={'size': '60'})
            return db_field.formfield(**kwargs)

        RICH_FIELDS = ('text',)
        if db_field.name in RICH_FIELDS:
            kwargs.pop('request')
            kwargs['widget'] = MDTextarea(attrs={'rows': '10'})
            return db_field.formfield(**kwargs)

        formfield = super(CollectionAdmin, self).formfield_for_dbfield(db_field, **kwargs)        
        return formfield

    def category_column(self, obj):
        return ", ".join(( c.name for c in obj.categories.all() ))
    category_column.short_description = 'Categories'

    def image_column(self, obj):
        t = Template('{% load thumbnail %}{% if count %}<img src="{% thumbnail img.filename 60x60 crop %}" alt="{{img.filename}}"/><br/>{% endif %}  {{count}} image{{count|pluralize}}')
        count = obj.images.count()
        if count:
            img = obj.images.all()[:1][0]
        else:
            img = None
        return t.render(Context({
            'img': img,
            'count': count 
        }))
    image_column.allow_tags = True
    image_column.short_description = 'Images'

    def changelist_view(self, request, extra_context=None):
        extra_context = {
            'categories': CollectionCategory.objects.all()
        }
        return super(CollectionAdmin, self).changelist_view(request, extra_context)

class CollectionCategoryAdmin(admin.ModelAdmin):
    model = CollectionCategory
    
    def changelist_view(self, request, **kwargs):
        return HttpResponseRedirect(reverse('admin:content_collection_changelist'))

    def get_urls(self):
        from django.conf.urls.defaults import patterns, url
        urls = super(CollectionCategoryAdmin, self).get_urls()
        info = self.model._meta.app_label, self.model._meta.module_name
        myurls = patterns('',
            url(r'^sort/$',
                self.admin_site.admin_view(self.sort_view),
                name='%s_%s_sort' % info),
        )
        return myurls + urls

    def sort_view(self, request):
        cats = request.POST.getlist('category')
        for sort, cat_id in enumerate(cats):
            category = CollectionCategory.objects.get(id=cat_id)
            category.sort = sort
            category.save()
        return HttpResponse("ok")


class NewsItemAdmin(admin.ModelAdmin):
    model = NewsItem
    list_display = ('image_column', 'title', 'pub_date', 'published',)
    list_filter = ('published',)
    list_display_links = ('title', )
    date_hierarchy = 'pub_date'
    fieldsets = (
        (None, {
            'fields': ('title', ('pub_date', 'published',), 'body', 'image', )
        }),
    )

    def image_column(self, obj):
        if obj.image:
            t = Template('{% load thumbnail %}<a href="{{obj.pk}}/"><img src="{% thumbnail image.filename 60x60 crop %}" alt="{{image.filename}}"/></a>')
            return t.render(Context({'obj': obj, 'image': obj.image}))
        
        return '---'
    image_column.allow_tags = True
    image_column.short_description = 'Image'

    def formfield_for_dbfield(self, db_field, **kwargs):
        # Make content field an mceEditor instance
        if db_field.name == 'image':
            kwargs.pop('request')
            kwargs['widget'] = ImageForeignKeyWidget(db_field.rel)
            return db_field.formfield(**kwargs)
        if db_field.name == 'body':
            kwargs.pop('request')
            kwargs['widget'] = MDTextarea(attrs={'rows': '10'})
            return db_field.formfield(**kwargs)
        if db_field.name == 'title':
            kwargs.pop('request')
            kwargs['widget'] = forms.widgets.TextInput(attrs={'size':'60'})
            return db_field.formfield(**kwargs)
        return super(NewsItemAdmin, self).formfield_for_dbfield(db_field, **kwargs)


class WorkSplashAdmin(admin.ModelAdmin):
    list_display = ('created', 'active',)


admin.site.register(Page, PageAdmin)
admin.site.register(Collection, CollectionAdmin)
admin.site.register(CollectionCategory, CollectionCategoryAdmin)
admin.site.register(WorkSplash, WorkSplashAdmin)
#admin.site.register(NewsItem, NewsItemAdmin)
