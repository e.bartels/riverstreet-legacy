from django.contrib import admin
from django import forms
from django.contrib.admin.widgets import RelatedFieldWidgetWrapper
from tagging.models import Tag

class TagSelectModelAdmin(admin.ModelAdmin):
    """
    Some magic to get a Select Widget to add tags
    to a model in the django admin
    """
    tag_widget = forms.widgets.Select
    tag_field = forms.ModelChoiceField
    tag_required = False

    def _get_tags(self, obj):
        initial_tags = None
        if obj:
            initial_tags = obj.tags[0].id
        return initial_tags

    def _get_tag_widget(self):
        base_widget = self.tag_widget()
        class Rel:
            pass
        rel = Rel()
        rel.to = Tag
        return RelatedFieldWidgetWrapper(base_widget, rel, self.admin_site)

    def _get_tag_field(self, initial_tags, widget):
        if self.tag_required:
            empty_label = None
        else:
            empty_label = '---------'
        return self.tag_field(
            Tag.objects.all(), required=self.tag_required, initial=initial_tags, empty_label=empty_label,
            widget=widget,
        )


    def get_form(self, request, obj=None):
        """
        Returns the form with support for adding tags with a
        MultipleChoiceField
        """
        formclass = super(TagSelectModelAdmin, self).get_form(request, obj)
        
        initial_tags = self._get_tags(obj)

        # This is a real hack.  Should come up with a more generic way to do this
        # Adds the icon for adding a new tag to the widget html.
        widget = self._get_tag_widget()

        formclass.base_fields['tags'] = self._get_tag_field(initial_tags, widget)
        
        # Append to fieldsets
        if len(self.fieldsets) <=1 :
            self.fieldsets.append((None, {'fields': ('tags',)}))
        return formclass

    def save_model(self, request, obj, form, change):
        """
        Saves the obj and related tags
        """
        obj.save()
        obj.tags = '"%s"' % form.cleaned_data['tags'] 
        return

class TagSelectMultipleModelAdmin(TagSelectModelAdmin):
    """
    Same as TagSelectModelADmin, but uses a SelectMultiple widget instead.
    """
    tag_widget = forms.widgets.SelectMultiple
    tag_field = forms.ModelMultipleChoiceField
    tag_required = False

    def _get_tags(self, obj):
        initial_tags = None
        if obj:
            initial_tags = [tag.id for tag in obj.tags]
        return initial_tags

    def _get_tag_field(self, initial_tags, widget):
        return self.tag_field(
            Tag.objects.all(), required=self.tag_required, initial=initial_tags,
            widget=widget, help_text='Hold down "Control", or "Command" on a Mac, to select more than one.'
        )

    def save_model(self, request, obj, form, change):
        obj.save()
        tags = ", ".join(['"%s"' % t.name for t in form.cleaned_data['tags']])
        obj.tags = tags
        return

