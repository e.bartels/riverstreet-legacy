from django import forms
from django.utils.safestring import mark_safe
from django.conf import settings
from django.utils.translation import ugettext as _

class InternalURLWidget(forms.TextInput):
    def render(self, name, value, attrs):
        output = []
        url = '../../../media/link_browser/'
        output.append(super(InternalURLWidget, self).render(name, value, attrs))
        output.append('<a href="%s" id="lookup_id_%s" target="_blank" rel="lookup_%s"> ' % (url, name, name))
        output.append('<img src="%simg/admin/selector-search.gif" width="16" height="16" alt="%s" /></a>' % (settings.ADMIN_MEDIA_PREFIX, _('Lookup')))

        output.append("""
        <script type="text/javascript">
            $(function() { new InternalURLWidget('id_%s')});
        </script>
        """ % (name))
        return mark_safe(u''.join(output))

    class Media:
        js = (settings.ADMIN_MEDIA_PREFIX + 'js/artcode/custom_widgets.js', )
