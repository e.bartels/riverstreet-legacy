from django import template

from cms.apps.content.models import Collection, CollectionCategory

register = template.Library()

def collection_dates_menu():
    """
    An inclusion tag to pull in a list of dates for Collection objects.
    """
    dates = Collection.objects.dates('date', 'year', order='DESC')

    return {    
        'dates': dates
    }
register.inclusion_tag('content/includes/collection_dates_menu.html')(collection_dates_menu)

def collection_menu(category):
    """
    An inclusion tag to show a list of Collections.
    """
    categories = CollectionCategory.objects.filter(published=True)
    if category:
        collections = category.collections.filter(published=True)
    else:
        collections = CollectionCategory.objects.none()
    return {
        'categories': categories,
        'collections': collections,
        'category': category
    }
register.inclusion_tag('content/includes/collection_menu.html')(collection_menu)
