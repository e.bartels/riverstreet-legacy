from django import template
from django.db.models import get_model

from cms.apps.content.models import Collection

register = template.Library()

def category_menu(classname, url=None):
    category_model = get_model('content', classname)
    categories = category_model.objects.filter(published=True)
    if url is None:
        url = ''
    return {    
        'categories': categories,
        'url': url, 
    }

register.inclusion_tag('content/includes/category_menu.html')(category_menu)

