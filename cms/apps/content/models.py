import datetime

from django.db import models

from cms.apps.media.models import RelatedImagesField, RelatedVideosField
from cms.apps.utils.fields import AutoSlugField
from cms.apps.content import config
from cms.apps import menus


class Category(models.Model):
    """
    A generic Category model.
    """
    name = models.CharField(max_length=200)
    slug = AutoSlugField(max_length=200, unique=True, editable=False,
            prepopulate_from='name',
            help_text='Unique text identifier used in urls.')
    published = models.BooleanField(default=True,
            help_text='Whether to publish on the site.')
    sort = models.IntegerField(default=0,
            help_text='Sort order, lower numbers will appear first on site.')
    videos = RelatedVideosField()

    class Meta:
        ordering = ('sort', 'id')
        verbose_name_plural = 'Categories'

    def __unicode__(self):
        return self.name


class Page(models.Model):
    """
    A page holds text content to display for the cms.
    """
    title = models.CharField(max_length=200)
    slug = AutoSlugField(max_length=200, unique=True, editable=False,
            prepopulate_from='title',
            help_text='Unique text identifier used in urls.')
    subtitle = models.CharField(max_length=15, blank=True,
            help_text="Text that appears below River Street logo.")
    tagline = models.CharField(max_length=100, blank=True,
            help_text="Text that appears beneath the main site menu.")
    published = models.BooleanField(default=True,
            help_text='Whether to publish on the site.')
    text = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    menu_color = models.CharField(max_length=10, blank=True, default="#049ED1",
            help_text="Choose a menu color for this page.")

    def __unicode__(self):
        return u'%s' % self.title

    @models.permalink
    def get_absolute_url(self):
        return ('content-page-view', [self.slug])
menus.register_model(Page)  # Plug Page instances into the menus app


class CollectionCategory(Category):

    class Meta:
        verbose_name_plural = 'Collection Categories'

    @models.permalink
    def get_absolute_url(self):
        return ('content-collection-index', [self.slug])


class Collection(models.Model):
    """
    A Collection of images and/or files
    """
    title = models.CharField(max_length=200)
    slug = AutoSlugField(max_length=200, unique=True, editable=False,
            prepopulate_from='title',
            help_text='Unique text identifier used in urls.')
    date = models.DateField(blank=True, null=True)
    published = models.BooleanField(default=True,
            help_text='Whether to publish on the site.')
    client = models.CharField(max_length=255, blank=True)
    text = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    sort = models.IntegerField(default=0,
            help_text='Sort order, lower numbers will appear first on site.')

    categories = models.ManyToManyField('CollectionCategory', blank=True,
            related_name='collections')

    images = RelatedImagesField()
    videos = RelatedVideosField()
    #files = RelatedFilesField()

    class Meta:
        ordering = ('-date', 'id')

    def __unicode__(self):
        return u'%s' % self.title

    def has_media(self):
        if self.images.count() or self.files.count():
            return True
        return False

    @models.permalink
    def get_absolute_url(self):
        return ('content-collection-view', [self.slug])


class WorkSplash(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)
    videos = RelatedVideosField()

    class Meta:
        verbose_name = "Work Splash"
        verbose_name_plural = "Work Splashes"
        ordering = ('active', '-created',)

    def save(self, *args, **kwargs):
        if self.active:
            WorkSplash.objects.all().update(active=False)
        super(WorkSplash, self).save(*args, **kwargs)


class NewsItem(models.Model):
    title = models.CharField(max_length=200)
    slug = AutoSlugField(max_length=200, unique=True, editable=False,
            prepopulate_from='title',
            help_text='Unique text identifier used in urls.')
    pub_date = models.DateTimeField('Date published',
            default=datetime.datetime.today, db_index=True,
            help_text='Date you would like this entry to go live on the site.')
    published = models.BooleanField(default=True,
            help_text='Select to publish on the site.')
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    image = models.ForeignKey('media.Image', blank=True, null=True,
            help_text='Click the icon to select an image.')

    class Meta:
        ordering = ('-pub_date',)

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('content-newsitem-view', [self.slug])


# Register Models with menu system
menus.register_url('Our work', name='content-collection-index')
