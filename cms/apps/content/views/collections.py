from django import http
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.contrib.contenttypes.models import ContentType

from cms.apps.content.models import Collection, CollectionCategory, WorkSplash
from cms.apps.media.models import Image


def splash(request):
    ctype = ContentType.objects.get_for_model(Collection)
    images = Image.objects.filter(work_page=True).order_by('?')
    collections = []
    for image in images:
        relations = image.relations.filter(content_type=ctype)[:1]
        try:
            col = relations[0].object
            if not col.published:
                col = None
        except IndexError:
            col = None
        collections.append((image, col))

    try:
        splash = WorkSplash.objects.get(active=True)
    except WorkSplash.DoesNotExist:
        splash = None

    return render_to_response('content/collection/splash.html', {
        'collections': collections,
        'category': None,
        'splash': splash,
    }, context_instance=RequestContext(request))


def index(request, cat=None):
    """
    Index & search page for Collections
    """
    if cat:
        category = get_object_or_404(CollectionCategory, slug=cat,
                                                         published=True)
    else:
        try:
            category = CollectionCategory.objects.filter(published=True)[0]
        except IndexError:
            raise http.Http404

    # Store this category to retrieve when viewing a collection
    request.session['last_visited_category'] = category.slug

    ctx = {
        'category': category,
    }

    # Splash page is shown if there are category videos
    if not category.videos.published().count():
        try:
            collection = category.collections.filter(published=True)[0]
            url = reverse('content-collection-view',
                          args=(collection.slug,))
            return http.HttpResponseRedirect(url)
        except IndexError:
            pass

    return render_to_response('content/collection/category_splash.html',
            ctx,
            context_instance=RequestContext(request))


def view(request, slug):
    collection = get_object_or_404(Collection, slug=slug, published=True)

    # Attempt to get last visted category
    last_visited_category = request.session.get('last_visited_category', '---')
    try:
        category = collection.categories.get(published=True,
                                             slug=last_visited_category)
    except CollectionCategory.DoesNotExist:
        # Otherwise try to get the first category for the collection.
        try:
            category = collection.categories.filter(published=True)[0]
        except IndexError:
            category = None

    videos = collection.videos.published()
    images = collection.images.all()

    # Layout is hd if there are any vimeo videos
    layout = 'default'
    if any(v.has_vimeo_video for v in videos):
        ratio = max(v.aspect_ratio for v in videos)
        if ratio < (16 / 9.0):
            layout = 'sd'
        else:
            layout = 'hd'

    return render_to_response('content/collection/view.html', {
        'collection': collection,
        'category': category,
        'videos': videos,
        'images': images,
        'layout': layout,
    }, context_instance=RequestContext(request))
