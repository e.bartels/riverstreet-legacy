from django.http import HttpResponse, Http404
from django.shortcuts import render_to_response, get_object_or_404
from django.template.loader import render_to_string
from django.template import RequestContext
from django.conf import settings

from cms.apps.content.models import NewsItem

def index(request):
    """
    Index & search page for NewsItems 
    """
    items = NewsItem.objects.filter(published=True)
    
    return render_to_response('content/news/index.html', {
        'items': items,
    }, context_instance=RequestContext(request))

def view(request, slug):
    item = get_object_or_404(NewsItem, slug=slug, published=True)
    return render_to_response('content/news/view.html', {
        'item': item
    }, context_instance=RequestContext(request))
