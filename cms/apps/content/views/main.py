from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext

from cms.apps.blog.models import Entry
from cms.apps.media.models import MediaItem

__all__ = ['home',]

def home(request):
    try:
        featured = MediaItem.objects.published().filter(featured=True).order_by('?')[0]
    except IndexError:
        featured = None
    news = Entry.objects.published().filter(categories__name__icontains='news')[0:3]
    #notebook = Entry.objects.published().exclude(categories__name__icontains='news')[0:3]
    return render_to_response('index.html', {
        'featured': featured,
        'news': news,
        #'notebook': notebook,
    }, context_instance=RequestContext(request))


