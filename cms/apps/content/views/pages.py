import os

from django.http import HttpResponse, Http404
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.template.loader import render_to_string

from cms.apps.content.models import Page

def view(request, slug):
    page = get_object_or_404(Page, slug=slug, published=True)
    return render_to_response([
        'content/page/%s/view.html' % page.pk,
        'content/page/%s/view.html' % page.slug,
        'content/page/view.html'
    ], {
        'page': page
    }, context_instance=RequestContext(request))

def page_css(request, id):
    page = get_object_or_404(Page, id=id, published=True)
    response = render_to_response('content/page/colors.css', {
        'color': page.menu_color,
    }, context_instance=RequestContext(request))

    response['Content-Type'] = 'text/css'
    return response
    
def _fetch_resource_path(uri, rel):
    """
    Callback to allow pisa/reportlab to retrieve Images,
    Stylesheets, etc.
    `uri` is the href attribute from the html link element.
    `rel` gives a relative path, but it's not used here.
    """
    path = os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ""))
    return path

def pdf(request, slug):
    try:
        import ho.pisa as pisa
    except ImportError:
        raise Http404

    page = get_object_or_404(Page, slug=slug, published=True)
    html = render_to_string('content/page/pdf.html', {
        'page': page,
    }, context_instance=RequestContext(request))
    
    response = HttpResponse(mimetype='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=%s.pdf' % page.slug

    pdf = pisa.CreatePDF(html, response, link_callback=_fetch_resource_path)

    return response
