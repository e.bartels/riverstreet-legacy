from django.conf.urls.defaults import *

urlpatterns = patterns('cms.apps.content.views.collections',
    url(r'^$', 'splash', name='content-collection-index'),
    url(r'^cat/(?P<cat>[\w-]+)/$', 'index', name='content-collection-index'),
    url(r'^(?P<slug>[\w-]+)/$', 'view', name='content-collection-view'),
)
