from django.conf.urls.defaults import *

urlpatterns = patterns('cms.apps.content.views.pages',
    url(r'^(?P<slug>[\w-]+)/$', 'view', name='content-page-view'),
    url(r'^(?P<slug>[\w-]+).pdf$', 'pdf', name='content-page-pdf'),
    url(r'^(?P<id>\d+)/colors.css$', 'page_css', name='content-page-color_css'),
)
