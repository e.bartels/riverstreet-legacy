from django.conf.urls.defaults import *

urlpatterns = patterns('cms.apps.content.views.news',
    url(r'^$', 'index', name='content-newsitem-index'),
    url(r'^(?P<slug>[\w-]+)/$', 'view', name='content-newsitem-view'),
)

