import re
import uuid
import base64
from django.utils import simplejson as json

from django import forms
from django.db import models
from django.utils.translation import ugettext as _
from django.template.defaultfilters import slugify as dj_slugify
from django.core.serializers.json import DjangoJSONEncoder


def slugify(s):
    s = s.replace(':', '-')
    return dj_slugify(s)


class AutoSlugField(models.SlugField):
    """
    Allows a SlugField to pre-populate from a model field automatically when
    model is saved.

    Call like this:
    AutoSlugField(prepopulate_from='name', separator='_')
    """

    separator = '-'

    def __init__(self, prepopulate_from, *args, **kwargs):
        """
        prepopulate_from is the field you want to use to create the slug
        """
        super(AutoSlugField, self).__init__(*args, **kwargs)
        self.prepopulate_from = prepopulate_from

    def pre_save(self, instance, add):
        """
        Populates the field from the field given in prepopulate_from attribute
        """
        # slugify and check for existing conflict
        cls = instance._meta.get_field_by_name(self.attname)[1]
        if not cls:
            cls = instance.__class__
        proposal = getattr(instance, self.attname)
        if not self.editable or not proposal:
            proposal = slugify(getattr(instance, self.prepopulate_from))
            if not proposal:  # field only contained non-numeric chars
                proposal = 'zzz'
        if instance.pk:
            similar_ones = cls.objects.filter(**{
                self.attname + "__startswith": proposal,
            }).exclude(pk=instance.pk).values(self.attname)
        else:
            similar_ones = cls.objects.filter(**{
                self.attname + "__startswith": proposal,
            }).values(self.attname)
        similar_ones = [elem[self.attname] for elem in similar_ones]
        if proposal not in similar_ones:
            slug = proposal
        else:
            numbers = []
            for value in similar_ones:
                match = re.match(r'^%s%s(\d+)$' % (proposal,
                                                   self.separator), value)
                if match:
                    numbers.append(int(match.group(1)))
            if len(numbers) == 0:
                slug = "%s%s2" % (proposal, self.separator)
            else:
                largest = sorted(numbers)[-1]
                slug = "%s%s%d" % (proposal, self.separator, largest + 1)

        assert(slug not in similar_ones)
        setattr(instance, self.attname, slug)
        return slug

    def formfield(self, **kwargs):
        # max_length and error_message can be overridden
        kwargs['max_length'] = kwargs.get('max_length', self.max_length)
        kwargs['error_message'] = kwargs.get(
                'error_message',
                _(u'Please enter a value containing only letters, numbers, '
              u'dashes, and underscores.')
            )
        defaults = {
                'regex': r'^[-\w]+$',  # from django.core.validators.slug_re
                'form_class': forms.RegexField,
                }
        # other defaults cannot be overridden
        kwargs.update(defaults)
        return super(AutoSlugField, self).formfield(**kwargs)

    def get_internal_type(self):
        return 'SlugField'

    def db_type(self):
        return 'varchar(%s)' % self.max_length


class UUIDField(models.CharField):
    """
    A field which stores a UUID value in hex format. This may also have
    the Boolean attribute 'auto' which will set the value on initial save to a
    new UUID value (calculated using the UUID1 method). Note that while all
    UUIDs are expected to be unique we enforce this with a DB constraint.
    """
    __metaclass__ = models.SubfieldBase

    def __init__(self, verbose_name=None, name=None, auto=True, version=4,
            node=None, clock_seq=None, namespace=None, **kwargs):

        if version not in (1, 3, 4, 5):
            raise ValueError("UUID version %s is not supported." % version)

        self.auto = auto
        self.version = version

        # Set this as a fixed value, we store UUIDs in text.
        kwargs.setdefault('max_length', 32)

        if auto:
            kwargs['blank'] = True
            kwargs.setdefault('unique', True)
            kwargs.setdefault('editable', False)

        self.node, self.clock_seq, self.namespace = node, clock_seq, namespace

        models.CharField.__init__(self, verbose_name, name, **kwargs)

    def get_internal_type(self):
        return models.CharField.__name__

    def db_type(self):
        return 'char(%s)' % self.max_length

    def contribute_to_class(self, cls, name):
        """This needs to be here as workaround for django ticket #10728"""
        super(UUIDField, self).contribute_to_class(cls, name)

    def to_python(self, value):
        if not value and self.auto:
            value = unicode(self._create_uuid().hex)
        return super(UUIDField, self).to_python(value)

    def _create_uuid(self):
        if self.version == 1:
            args = (self.node, self.clock_seq)
        elif self.version in (3, 5):
            args = (self.namespace, self.name)
        else:
            args = ()
        return getattr(uuid, 'uuid%s' % (self.version,))(*args)

    def pre_save(self, model_instance, add):
        value = getattr(model_instance, self.attname, None)
        if not value and self.auto:
            value = self._create_uuid().hex
            setattr(model_instance, self.attname, value)
        return value


class UUIDB64Field(UUIDField):
    """
    Same as UUIDField but stores the uuid encoded in url safe base64.
    """
    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 22
        super(UUIDB64Field, self).__init__(*args, **kwargs)

    def to_python(self, value):
        if not value and self.auto:
            value = unicode(base64.urlsafe_b64encode(
                            self._create_uuid().bytes)[:-2])
        return super(UUIDB64Field, self).to_python(value)

    def pre_save(self, model_instance, add):
        value = getattr(model_instance, self.attname, None)
        if not value and self.auto:
            value = base64.urlsafe_b64encode(self._create_uuid().bytes)[:-2]
            setattr(model_instance, self.attname, value)
        return value


class JSONField(models.TextField):
    """
    JSONField is a generic textfield that neatly serializes/unserializes
    JSON objects seamlessly
    """

    # Used so to_python() is called
    __metaclass__ = models.SubfieldBase

    def to_python(self, value):
        """Convert our string value to JSON after we load it from the DB"""

        if value == "":
            return ""

        try:
            if isinstance(value, basestring):
                return json.loads(value)
        except ValueError:
            pass

        return value

    def get_db_prep_save(self, value, connection):
        """Convert our JSON object to a string before we save"""

        if value == "":
            return ""

        if isinstance(value, dict):
            value = json.dumps(value, cls=DjangoJSONEncoder)

        return super(JSONField, self).get_db_prep_save(value,
                                                       connection=connection)

    def contribute_to_class(self, cls, name):
        """This needs to be here as workaround for django ticket #10728"""
        super(JSONField, self).contribute_to_class(cls, name)


# South Introspection Rules
try:
    from south.modelsinspector import add_introspection_rules
    rules = [
        (
            (AutoSlugField,),
            [],
            {
                "prepopulate_from": ["prepopulate_from", {}],
            },
        ),
        (
            (UUIDField,),
            [],
            {
                "auto": ["auto", {"default": True}],
                "version": ["version", {"default": 4}],
                "node": ["node", {"default": None}],
                "clock_seq": ["clock_seq", {"default": None}],
                "namespace": ["namespace", {"default": None}],
            }
        ),
    ]

    add_introspection_rules(rules, ["^.+\.apps\.utils\.fields"])

except ImportError:
    pass
