import re

from django import template
from cms.apps.menus.models import MenuItem

register = template.Library()

def subtitle(obj):
    if isinstance(obj, str) or isinstance(obj, unicode):
        try:
            item = MenuItem.objects.get(url=obj)
            return item.subtitle
        except MenuItem.DoesNotExist:
            pass
    else:
        if hasattr('subtitle', obj) and obj.subtitle:
            return obj.subtitle
    return ''
register.simple_tag(subtitle)


class TaglineNode(template.Node):
    def __init__(self, obj, varname=None):
        self.obj = template.Variable(obj)
        self.varname = varname 

    def render(self, context):
        obj = self.obj.resolve(context) 
        tagline = ''
        if isinstance(obj, str) or isinstance(obj, unicode):
            try:
                item = MenuItem.objects.get(url=obj)
                tagline = item.tagline
            except MenuItem.DoesNotExist:
                pass
        else:
            if hasattr('tagline', obj):
                tagline = obj.tagline
        
        if not self.varname:
            return tagline

        context[self.varname] = tagline
        return ''

def tagline(parser, token):
    try:
        tag_name, arg = token.contents.split(None, 1)
    except ValueError:
        raise template.TemplateSyntaxError, "%r tag requires arguments" % token.contents.split()[0]
    
    args = arg.split()
    if len(args) == 3:
        m = re.search(r'(.*?) as (\w+)', arg)
        if not m:
            raise template.TemplateSyntaxError, "Invalid arguments \nexample: {%% %r OBJECT as VAR_NAME %%}" % tag_name
    
        obj, var_name = m.groups()
    elif len(args) == 1:
        obj = args[0]
        var_name = None
    else:
        raise template.TemplateSyntaxError, "Invalid arguments \nexample: {%% %r OBJECT as VAR_NAME %%}" % tag_name

    return TaglineNode(obj, var_name)
register.tag(tagline)


class HeaderImageNode(template.Node):
    def __init__(self, varname=None):
        self.varname = varname 

    def render(self, context):
        from cms.apps.media.models import Image
        try:
            image = Image.objects.filter(header=True).order_by('?')[0]
        except IndexError:
            return ''

        if not self.varname:
            return image.filename.url

        context[self.varname] = image
        return ''

def header_image(parser, token):
    try:
        tag_name, arg = token.contents.split(None, 1)
    except ValueError:
        raise template.TemplateSyntaxError, "%r tag requires arguments" % token.contents.split()[0]

    args = arg.split()
    if len(args) == 2:
        m = re.search(r'as (\w+)', arg)
        if not m:
            raise template.TemplateSyntaxError, "Invalid arguments \nexample: {%% %r as VAR_NAME %%}" % tag_name
    
        var_name = m.groups()[0]
    elif len(args) == 0:
        var_name = None
    else:
        raise template.TemplateSyntaxError, "Invalid arguments \nexample: {%% %r as VAR_NAME %%}" % tag_name

    return HeaderImageNode(var_name)
register.tag(header_image)
