from django import forms

from cms.apps.configuration.values import LongStringValue
from cms.apps.utils.forms.widgets import RichTextEditor
from cms.apps.utils.forms.widgets import MDTextarea


class RichTextValue(LongStringValue):
    """
    Config value that shows a rich text editor.
    """
    class field(forms.CharField):
        def __init__(self, *args, **kwargs):
            kwargs['required'] = False
            kwargs['widget'] = RichTextEditor(attrs={'class': 'mceEditorCustom'})
            forms.CharField.__init__(self, *args, **kwargs)

class MDTextValue(LongStringValue):
    class field(forms.CharField):
        def __init__(self, *args, **kwargs):
            markdown_set = kwargs.pop('markdownSet', 'markdownSimple')
            kwargs['required'] = False
            kwargs['widget'] = MDTextarea(attrs={'markdownSet':markdown_set, 'rows': '10'})
            forms.CharField.__init__(self, *args, **kwargs)

    def __init__(self, *args, **kwargs):
        return super(MDTextValue, self).__init__(*args, **kwargs)

    def make_field(self, **kwargs):
        if self.choices:
            if self.hidden:
                kwargs['widget'] = forms.MultipleHiddenInput()
            field = self.choice_field(**kwargs)
        else:
            if self.hidden:
                kwargs['widget'] = forms.HiddenInput()
            field = self.field(**kwargs)
            
        field.group = self.group
        field.default_text = self.default_text
        return field

