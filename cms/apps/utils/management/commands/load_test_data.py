import os
import random
from optparse import make_option
import datetime

from django.conf import settings
from django.core.management import BaseCommand, CommandError
from django.core.files import File as DjangoFile
from django.contrib.webdesign import lorem_ipsum as lorem
from django.core.urlresolvers import reverse

from cms.apps.media.models import Image
from cms.apps.content.models import *
from cms.apps.blog.models import Entry
from cms.apps.menus.models import MenuItem
from cms import config
from cms.apps.configuration import ConfigurationSettings

class Command(BaseCommand):
    """
    Load test data (images, pages, collections, blog, etc.)
    """
    option_list = BaseCommand.option_list + (
        make_option('--collections', action='store_true', dest='do_collections', default=False,
            help='Install collections'),
        make_option('--blog', action='store_true', dest='do_blog', default=False,
            help='Install blog entries'),
        make_option('--artists', action='store_true', dest='do_artists', default=False,
            help='Install artists'),
        make_option('--pages', action='store_true', dest='do_pages', default=False,
            help='Install pages'),
        make_option('--exhibitions', action='store_true', dest='do_exhibitions', default=False,
            help='Install exhibitions'),
    )
    help = "Loads testing content for developing new sites."

    requires_model_validation = True

    def handle(self, *args, **options):
        do_collections = options.get('do_collections', False)
        do_blog = options.get('do_blog', False)
        do_artists = options.get('do_artists', False)
        do_pages = options.get('do_pages', False)
        do_exhibitions = options.get('do_exhibitions', False)

        self.load_images()

        self.do_settings()

        if do_collections:
            self.do_collections()
        if do_artists:
            self.do_artists()
        if do_exhibitions:
            self.do_exhibitions()
        if do_blog:
            self.do_blog()
        if do_pages:
            self.do_pages()

    def load_images(self):
        # Install test images.
        dirname = os.path.join(settings.PROJECT_DIR, 'apps', 'media', 'test_images')         
        files = os.listdir(dirname)
        images = []
        for file in files:
            if file.endswith('jpg'):
                basename = os.path.basename(file)
                path = os.path.join(dirname, file)
                f = DjangoFile(open(path, 'r'))
                image = Image()
                image.caption = lorem.paragraph()
                image.filename.save(basename, f)
                images.append(image)

    def do_settings(self):
        mgr = ConfigurationSettings()
        mgr.get_config('SITE', 'TITLE').update('Site Title')


    def _get_random_images(self):
        images = Image.objects.all()
        rand_images = [x for x in images]
        random.shuffle(rand_images)
        rand_images = rand_images[:random.randrange(1,len(rand_images))]
        return rand_images

    def do_pages(self):
        titles = ['Contact', 'About', 'Page 1', 'Page 2']
        for title in titles:
            page = Page()
            page.title = title
            page.text = "\n\n".join(lorem.paragraphs(3))
            page.save()

    def do_collections(self):
        for i in range(0, 10):
            collection = Collection()
            collection.title = 'Collection %s' % (i + 1)
            collection.text = "\n\n".join(lorem.paragraphs(2))
            collection.save()

            # Append random images
            images = self._get_random_images()
            collection.images.add(*images)
        
        MenuItem.objects.create(item_title="Portfolio", url=reverse('content-collection-index'))
    
    def do_blog(self):
        pub_date = datetime.datetime.today()
        for i in range(0, 14):
            entry = Entry()
            entry.title = 'Blog Entry %s' % (i + 1)
            entry.body = "\n\n".join(lorem.paragraphs(4))
            entry.author_id = 1
            entry.pub_date = pub_date
            pub_date = pub_date - datetime.timedelta(days=20)
            entry.save()

        MenuItem.objects.create(item_title="Blog", url=reverse('blog-index'))
    
    def do_exhibitions(self):
        start_date = datetime.datetime.today() + datetime.timedelta(days=10)
        for i in range(0, 10):
            ex = Exhibition()
            ex.title = 'Exhibition %s' % (i + 1)
            ex.text = "\n\n".join(lorem.paragraphs(3))
            ex.start_date = start_date
            ex.end_date = start_date + datetime.timedelta(days=5)
            start_date = start_date - datetime.timedelta(days=10)
            ex.save()

            # Append random images
            images = self._get_random_images()
            ex.images.add(*images)

        MenuItem.objects.create(item_title="Exhibitions", url=reverse('content-exhibition-index'))
    
    def do_artists(self):
        for i in range(0, 10):
            artist = Artist()
            artist.name= 'Artist %s' % (i + 1)
            artist.bio = "\n\n".join(lorem.paragraphs(3))
            artist.save()

            # Append random images
            images = self._get_random_images()
            artist.images.add(*images)
