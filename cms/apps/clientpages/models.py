from django.db import models
from django.contrib.sites.models import Site

from cms.apps.utils.fields import AutoSlugField
from cms.apps.media.models import RelatedVideosField


class ClientPage(models.Model):
    title = models.CharField(max_length=255)
    slug = AutoSlugField(max_length=255, prepopulate_from='title', unique=True,
            help_text='Unique text identifier used in urls.')
    published = models.BooleanField(default=True, help_text='Whether to publish on the site.')

    videos = RelatedVideosField()

    class Meta:
        verbose_name = 'Client Page'
        verbose_name_plural = 'Client Pages'

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('clientpages-view', [self.slug])

    @property
    def url(self):
        site = Site.objects.get_current()
        return u'http://%s%s' % (site.domain, self.get_absolute_url())
