from django.contrib import admin

from cms.apps.clientpages.models import ClientPage


class ClientPageAdmin(admin.ModelAdmin):
    list_display = ('title', 'published', 'url')
    list_filter = ('published',)
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(ClientPage, ClientPageAdmin)
