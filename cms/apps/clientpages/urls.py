from django.conf.urls.defaults import *

urlpatterns = patterns('cms.apps.clientpages.views',
    url(r'^(?P<slug>[\w-]+)/$', 'view', name='clientpages-view'),
    url(r'^(?P<slug>[\w-]+)/(?P<video_id>\d+)/$', 'view_video', name='clientpages-view_video'),
)
