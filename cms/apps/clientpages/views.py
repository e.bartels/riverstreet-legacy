from django import http
from django.shortcuts import get_object_or_404, render_to_response
from django. template import RequestContext

from cms.apps.clientpages.models import ClientPage
from cms.apps.media.models import Video


def view(request, slug):
    page = get_object_or_404(ClientPage, slug=slug, published=True)
    videos = page.videos.published()

    return render_to_response('clientpages/view.html', {
        'page': page,
        'videos': videos,
    }, context_instance=RequestContext(request))


def view_video(request, slug, video_id):
    page = get_object_or_404(ClientPage, slug=slug, published=True)
    videos = page.videos.published()
    try:
        video = videos.get(pk=video_id)
    except Video.DoesNotExist:
        raise http.Http404

    return render_to_response('clientpages/view_video.html', {
        'page': page,
        'videos': videos,
        'video': video,
    }, context_instance=RequestContext(request))
