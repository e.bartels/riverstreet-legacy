from django.shortcuts import render_to_response, get_object_or_404
from django.http import Http404, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.conf import settings
from cms.apps.menus.models import MenuItem

def menuitem_view(request, slug):
    """
    View a menuitem category
    """
    menuitem = get_object_or_404(MenuItem, slug=slug)
    if menuitem.url:
        return HttpResponseRedirect(menuitem.url)
    elif menuitem.object and menuitem.object.get_absolute_url:
        return HttpResponseRedirect(menuitem.object.get_absolute_url())
    return render_to_response('menus/menuitem/view.html', {
            'menuitem': menuitem,
    }, context_instance=RequestContext(request))


def menu_css(request):
    """
    Dynamically generates CSS for menu colors based on stored values in the database models.
    """
    items = MenuItem.objects.all()
    response = render_to_response('menus/colors.css', {
        'items': items,
    }, context_instance=RequestContext(request))

    response['Content-Type'] = 'text/css'
    return response

def item_css(request, id):
    """
    Dynamically generates CSS for menu colors based on stored values in the database models.
    """
    item = get_object_or_404(MenuItem, id=id)
    response = render_to_response('menus/item.css', {
        'color': item.menu_color,
    }, context_instance=RequestContext(request))

    response['Content-Type'] = 'text/css'
    return response
