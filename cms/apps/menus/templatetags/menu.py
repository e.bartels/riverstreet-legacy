from django import template
from django.template.loader import render_to_string
from cms.apps.menus.models import MenuItem

register = template.Library()

class MainMenuNode(template.Node):
    def render(self, context):
        menuitems = MenuItem.tree.root_nodes()
        return render_to_string('includes/main_menu.html',{
            'menuitems' : menuitems
        })

def main_menu(parser, token):
    return MainMenuNode()
register.tag('main_menu', main_menu)


class GetMenuItemNode(template.Node):
    def __init__(self, request, varname):
        self.request = template.Variable(request)
        self.varname = varname

    def render(self, context):
        request = self.request.resolve(context)
        item = MenuItem.objects.get_parent_for_url(request.path)
        context[self.varname] = item
        return ''

def get_menuitem(parser, token):
    """
    Returns a MenuItem instance for the current request.path, or none
    if one is not found. 

    Usage::
       {% get_menuitem request as varname %}

    """
    args = token.split_contents()
    tag = args[0]
    if len(args) != 4 or args[2] != 'as':
        raise template.TemplateSyntaxError('Usage: {% %r request as varname %}' % tag)

    request = args[1]
    varname = args[3]
    
    return GetMenuItemNode(request, varname)
    
register.tag('get_menuitem', get_menuitem)
