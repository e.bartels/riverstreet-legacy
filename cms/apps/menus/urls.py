from django.conf.urls.defaults import *

urlpatterns = patterns('cms.apps.menus.views',
    url(r'^(?P<slug>[\w-]+)/$', 'menuitem_view', name='menus-menuitem-view'),
    url(r'^css/colors.css$', 'menu_css', name='menus-menu_css'),
    url(r'^(?P<id>\d+)/css/item.css$', 'item_css', name='menus-item_css'),
)
