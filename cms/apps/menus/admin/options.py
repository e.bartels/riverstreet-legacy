from django.contrib import admin
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect
from django.db.models.loading import get_model
from django.core.urlresolvers import reverse
from django.core.exceptions import PermissionDenied

from cms.apps.menus.models import MenuItem
from cms.apps.menus import registered_models, registered_urls
from cms.apps.utils.json import json_encode
from cms.apps.utils.forms.widgets import ColorInput


csrf_protect_m = method_decorator(csrf_protect)

class MenuItemAdmin(admin.ModelAdmin):
    model = MenuItem
    fields = ['item_title', 'url', 'subtitle', 'tagline', 'menu_color']

    def queryset(self, request):
        qs = self.model.tree.get_query_set() 
        ordering = self.ordering or () # otherwise we might try to *None, which is bad ;)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs

    def get_form(self, request, obj=None):
        if obj and obj.is_container():
            self.fields = ['item_title', ]
        else:
            self.fields = ['item_title', 'url', 'subtitle', 'tagline', 'menu_color']
        return super(MenuItemAdmin, self).get_form(request, obj)
    
    def get_urls(self):
        from django.conf.urls.defaults import patterns, url
        urls = super(MenuItemAdmin, self).get_urls()
        info = self.model._meta.app_label, self.model._meta.module_name
        
        myurls = patterns('',
            url(r'^move_item/$',
                self.admin_site.admin_view(self.move_menuitem_view),
                name='%s_%s_move' % info),
            url(r'^add_item/$',
                self.admin_site.admin_view(self.add_menuitem_view),
                name='%s_%s_add_item' % info),
            url(r'^delete_item/$',
                self.admin_site.admin_view(self.delete_menuitem_view),
                name='%s_%s_delete_item' % info),
            url(r'^add_object/$', 
                self.admin_site.admin_view(self.add_object_view),
                name='%s_%s_add_object' % info),
            url(r'^(?P<id>\d+)/edit_object/$', 
                self.admin_site.admin_view(self.edit_object),
                name="admin_menus_menuitem_edit_object"),
        )
        return myurls + urls

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'menu_color':
            kwargs.pop('request')
            kwargs['widget'] = ColorInput(attrs={'size':'10'})
            return db_field.formfield(**kwargs)
        return super(MenuItemAdmin, self).formfield_for_dbfield(db_field, **kwargs)

    @csrf_protect_m
    def changelist_view(self, request, extra_context=None):
        if not self.has_change_permission(request, None):
            raise PermissionDenied

        menuitems = MenuItem.tree.all()
        unused_objects = []
        for model in registered_models:
            ctype= ContentType.objects.get_for_model(model)
            ids = [i.object.pk for i in MenuItem.objects.filter(content_type=ctype)]
            objects = model.objects.exclude(id__in=ids)
            unused_objects.append((model._meta, objects))  

        used_app_urls = [i.url for i in MenuItem.objects.apps()]
        unused_apps = [app for app in registered_urls if app['url'] not in used_app_urls]
        
        # Add a new menu category
        if request.POST:
            if 'title' in request.POST:
                menuitem = MenuItem(item_title=request.POST['title'])
                if 'url' in request.POST:
                    menuitem.url = request.POST['url']
                MenuItem.tree.insert_node(menuitem, None, position='last-child', commit=True)
                request.user.message_set.create(message='Your menu category has been added.')
                return HttpResponseRedirect('./')
        
        return render_to_response('admin/menus/change_list.html', {
            'title': 'Configure Menu',
            'root_path': self.admin_site.root_path,
            'menuitems': menuitems,
            'unused_objects': unused_objects,
            'unused_apps': unused_apps,
        }, context_instance=RequestContext(request))
        
    @method_decorator(permission_required('menus.change_menuitem'))
    def move_menuitem_view(self, request):
        """
        Ajax view to reorder the placement of a MenuItem in the tree structure.
        """
        if request.POST:
            item_id = request.POST['item']
            item = get_object_or_404(MenuItem, pk=item_id)
            parent, rght, lft = None, None, None
            if 'parent' in request.POST:
                try:
                    parent_id = int(request.POST['parent'])
                    parent = get_object_or_404(MenuItem, pk=parent_id)
                except ValueError:
                    pass
            if 'rght' in request.POST:
                try:
                    rght_id = int(request.POST['rght'])
                    if rght_id != item_id:
                        rght = get_object_or_404(MenuItem, pk=rght_id)
                except ValueError:
                    pass
            if 'lft' in request.POST:
                try:
                    lft_id = int(request.POST['lft'])
                    if lft_id != item_id:
                        lft = get_object_or_404(MenuItem, pk=lft_id)
                except ValueError:
                    pass
                
            # Move node based on parent
            if rght:
                MenuItem.tree.move_node(item, rght, 'left')
            elif lft:
                MenuItem.tree.move_node(item, lft, 'right')
            else:
                MenuItem.tree.move_node(item, parent)
            return HttpResponse('ok')
        return HttpResponseBadRequest('no data provided')

    @method_decorator(permission_required('menus.add_menuitem'))
    def add_menuitem_view(self, request):
        if request.method == 'POST':
            if 'title' in request.POST and 'url' in request.POST:
                title = request.POST['title']
                url = request.POST['url']
                menuitem = MenuItem.objects.create(item_title=title, url=url)
                return HttpResponse(json_encode(menuitem))
            else:
                return HttpResponseBadRequest('wrong data')
        else:
            return HttpResponseBadRequest('no data provided')

    @method_decorator(permission_required('menus.delete_menuitem'))
    def delete_menuitem_view(self, request):
        if request.method == 'POST':
            if 'pk' in request.POST:
                pk = request.POST['pk']
                menuitem = get_object_or_404(MenuItem, pk=pk)
                menuitem.delete()
                return HttpResponse(json_encode(menuitem))
            else:
                return HttpResponseBadRequest('wrong data')
        else:
            return HttpResponseBadRequest('no data provided')

    @method_decorator(permission_required('menus.add_menuitem'))
    def add_object_view(self, request):
        if request.method == 'POST':
            if 'model' in request.POST and 'pk' in request.POST:
                app_name, model_name = request.POST['model'].split('.')
                pk = request.POST['pk']
                model = get_model(app_name, model_name)            
                instance = get_object_or_404(model, pk=pk)
                menuitem = MenuItem.objects.create_for_object(instance)
                return HttpResponse(json_encode(menuitem))
            else:
                return HttpResponseBadRequest('wrong data')
        else:
            return HttpResponseBadRequest('no data provided')

    def edit_object(self, request, id):
        menuitem = get_object_or_404(MenuItem, pk=id)
        if menuitem.object:
            url = reverse('admin:%s_%s_change' % (menuitem.object._meta.app_label, menuitem.object._meta.module_name), args=(menuitem.object.pk,))
        else:
            url = reverse('admin:menus_menuitem_change', args=(menuitem.pk,))
        return HttpResponseRedirect(url)

admin.site.register(MenuItem, MenuItemAdmin)
