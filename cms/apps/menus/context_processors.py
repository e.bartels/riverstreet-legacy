from django.core.urlresolvers import reverse 
from cms.apps.menus.models import MenuItem

def menuitems(request):
    context = {}
    path = request.META.get('PATH_INFO')
    if path:
        try:
            menuitem = MenuItem.objects.filter(url=path)[0]
        except IndexError:
            menuitem = None
        context = {
            'MENUITEM': menuitem 
        }
    return context
