from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.utils.functional import lazy
from django.db.models import signals

class AlreadyRegistered(Exception):
    """
    An attempt was made to register a model more than once.
    """
    pass

registered_models = []
def register_model(model):
    """
    Registers the model so instances can be used in the menu system.
    """
    from cms.apps.menus.models import MenuItem
    from cms.apps.menus.models import menuitem_object_post_save, menuitem_object_pre_save, menuitem_object_pre_delete
    if model in registered_models:
        raise AlreadyRegistered(
            'The model %s has already been registered for menu items.' % model.__name__)

    # Set up shortcut methods for object
    setattr(model, 'get_menuitem', lambda self: MenuItem.objects.get_menuitem_for_object(self))
    setattr(model, 'get_root_menuitem', lambda self: MenuItem.objects.get_root_menuitem_for_object(self))
    setattr(model, 'get_parent', lambda self: MenuItem.objects.get_parent_for_object(self))

    # Set up signals
    signals.post_save.connect(menuitem_object_post_save, sender=model)
    signals.pre_save.connect(menuitem_object_pre_save, sender=model)
    signals.pre_delete.connect(menuitem_object_pre_delete, sender=model)
    
    registered_models.append(model)


registered_urls = []
def register_url(app, url=None, name=None, **kwargs):
    """
    Registers a URL to be used in the menu navigation system.  You can pass 
    either a url directly, as in:
    
        register_url("Portfolio", "/collections/"), 
    
    Or the name of a view either directly or from the name in the urlconf, 
    as in:
        
        register_url("Portfolio", name="cms.apps.content.views.collections.index")

    Or: 

        register_url("Portfolio", name="content-collection-index")
    """
    if url is None and name is None:
        raise TypeError, "register_url() takes either 'url' or 'name' as arguments."

    if app in registered_urls:
        raise AlreadyRegistered(
            'The app %s has already been registered for menu items.' % app)
   
    lazy_reverse = lazy(reverse, str)
    if not url:
        url = lazy_reverse(name, **kwargs)
    registered_urls.append({
        'name': app,
        'url': url,
    })
