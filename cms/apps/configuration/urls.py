from django.conf.urls.defaults import *

urlpatterns = patterns('cms.apps.configuration.views',
    (r'^$', 'site_settings', {}, 'site_settings'),
    (r'^(?P<group>[^/]+)/$', 'group_settings'),
)
