import re
from django.template import Library, Node, TemplateSyntaxError
from django.utils.translation import ugettext as _
from django.conf import settings

from cms.apps.configuration import config_value
from cms.apps.configuration.models import find_setting, SettingNotSet

register = Library()

class SettingNode(Node):
    def __init__(self, group, key, context_var):
        self.group = group
        self.key = key
        self.context_var = context_var

    def render(self, context):
        try:
            setting = config_value(self.group, self.key)
        except SettingNotSet:
            if settings.TEMPLATE_DEBUG:
                raise SettingNotSet(self.key)
            else:
                return ''
        if self.context_var:
            context[self.context_var] = setting
            return ''
        return setting


def do_setting(parser, token):
    """
    Returns a config setting based on the group and key.

    Usage::

       {% setting group key %}
            OR
       {% setting group key as varname %}

    """
    args = token.split_contents()
    tag = args[0]
    # Check to see if we're setting to a context variable.
    if len(args) > 3 and args[-2] == 'as':
        context_var = args[-1]
        args = args[:-2]
    else:
        context_var = None
    
    try :
        group = args[1]
        key = args[2]
    except IndexError:
        raise TemplateSyntaxError('Syntax: {% %r group key %}' % tag)
    
    return SettingNode(group, key, context_var)
    
register.tag('setting', do_setting)
