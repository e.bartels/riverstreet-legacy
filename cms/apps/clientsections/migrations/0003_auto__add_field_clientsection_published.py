# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'ClientSection.published'
        db.add_column('clientsections_clientsection', 'published', self.gf('django.db.models.fields.BooleanField')(default=True), keep_default=False)


    def backwards(self, orm):
        
        # Deleting field 'ClientSection.published'
        db.delete_column('clientsections_clientsection', 'published')


    models = {
        'clientsections.category': {
            'Meta': {'ordering': "('sort', 'id')", 'unique_together': "(('section', 'slug'),)", 'object_name': 'Category'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'section': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'categories'", 'to': "orm['clientsections.ClientSection']"}),
            'slug': ('cms.apps.utils.fields.AutoSlugField', [], {'prepopulate_from': "'name'", 'max_length': '200', 'db_index': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'clientsections.clientsection': {
            'Meta': {'object_name': 'ClientSection'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'slug': ('cms.apps.utils.fields.AutoSlugField', [], {'prepopulate_from': "'name'", 'unique': 'True', 'max_length': '200', 'db_index': 'True'}),
            'subtitle': ('django.db.models.fields.CharField', [], {'default': "'top picks'", 'max_length': '15', 'blank': 'True'}),
            'tagline': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'clientsections.clientsplash': {
            'Meta': {'ordering': "('active', '-created')", 'object_name': 'ClientSplash'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'section': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'splash_pages'", 'to': "orm['clientsections.ClientSection']"})
        },
        'clientsections.collection': {
            'Meta': {'ordering': "('-date', 'id')", 'unique_together': "(('section', 'slug'),)", 'object_name': 'Collection'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'collections'", 'symmetrical': 'False', 'to': "orm['clientsections.Category']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'section': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'collections'", 'to': "orm['clientsections.ClientSection']"}),
            'slug': ('cms.apps.utils.fields.AutoSlugField', [], {'prepopulate_from': "'title'", 'max_length': '200', 'db_index': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['clientsections']
