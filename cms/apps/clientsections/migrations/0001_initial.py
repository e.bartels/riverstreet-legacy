# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'ClientSection'
        db.create_table('clientsections_clientsection', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('slug', self.gf('cms.apps.utils.fields.AutoSlugField')(prepopulate_from='name', unique=True, max_length=200, db_index=True)),
        ))
        db.send_create_signal('clientsections', ['ClientSection'])

        # Adding model 'Category'
        db.create_table('clientsections_category', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('section', self.gf('django.db.models.fields.related.ForeignKey')(related_name='categories', to=orm['clientsections.ClientSection'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('slug', self.gf('cms.apps.utils.fields.AutoSlugField')(prepopulate_from='name', max_length=200, db_index=True)),
            ('published', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('sort', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('clientsections', ['Category'])

        # Adding unique constraint on 'Category', fields ['section', 'slug']
        db.create_unique('clientsections_category', ['section_id', 'slug'])

        # Adding model 'Collection'
        db.create_table('clientsections_collection', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('section', self.gf('django.db.models.fields.related.ForeignKey')(related_name='collections', to=orm['clientsections.ClientSection'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('slug', self.gf('cms.apps.utils.fields.AutoSlugField')(prepopulate_from='title', max_length=200, db_index=True)),
            ('date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('published', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('sort', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('text', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal('clientsections', ['Collection'])

        # Adding unique constraint on 'Collection', fields ['section', 'slug']
        db.create_unique('clientsections_collection', ['section_id', 'slug'])

        # Adding M2M table for field categories on 'Collection'
        db.create_table('clientsections_collection_categories', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('collection', models.ForeignKey(orm['clientsections.collection'], null=False)),
            ('category', models.ForeignKey(orm['clientsections.category'], null=False))
        ))
        db.create_unique('clientsections_collection_categories', ['collection_id', 'category_id'])

        # Adding model 'ClientSplash'
        db.create_table('clientsections_clientsplash', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('section', self.gf('django.db.models.fields.related.ForeignKey')(related_name='splash_pages', to=orm['clientsections.ClientSection'])),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('clientsections', ['ClientSplash'])


    def backwards(self, orm):
        
        # Removing unique constraint on 'Collection', fields ['section', 'slug']
        db.delete_unique('clientsections_collection', ['section_id', 'slug'])

        # Removing unique constraint on 'Category', fields ['section', 'slug']
        db.delete_unique('clientsections_category', ['section_id', 'slug'])

        # Deleting model 'ClientSection'
        db.delete_table('clientsections_clientsection')

        # Deleting model 'Category'
        db.delete_table('clientsections_category')

        # Deleting model 'Collection'
        db.delete_table('clientsections_collection')

        # Removing M2M table for field categories on 'Collection'
        db.delete_table('clientsections_collection_categories')

        # Deleting model 'ClientSplash'
        db.delete_table('clientsections_clientsplash')


    models = {
        'clientsections.category': {
            'Meta': {'ordering': "('sort', 'id')", 'unique_together': "(('section', 'slug'),)", 'object_name': 'Category'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'section': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'categories'", 'to': "orm['clientsections.ClientSection']"}),
            'slug': ('cms.apps.utils.fields.AutoSlugField', [], {'prepopulate_from': "'name'", 'max_length': '200', 'db_index': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'clientsections.clientsection': {
            'Meta': {'object_name': 'ClientSection'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('cms.apps.utils.fields.AutoSlugField', [], {'prepopulate_from': "'name'", 'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        'clientsections.clientsplash': {
            'Meta': {'ordering': "('active', '-created')", 'object_name': 'ClientSplash'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'section': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'splash_pages'", 'to': "orm['clientsections.ClientSection']"})
        },
        'clientsections.collection': {
            'Meta': {'ordering': "('-date', 'id')", 'unique_together': "(('section', 'slug'),)", 'object_name': 'Collection'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'collections'", 'symmetrical': 'False', 'to': "orm['clientsections.Category']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'section': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'collections'", 'to': "orm['clientsections.ClientSection']"}),
            'slug': ('cms.apps.utils.fields.AutoSlugField', [], {'prepopulate_from': "'title'", 'max_length': '200', 'db_index': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['clientsections']
