from django.contrib import admin
from django import http
from django import forms
from django.core.urlresolvers import reverse, resolve
from django.template import Template, Context, RequestContext
from django.shortcuts import get_object_or_404, render_to_response

from cms.apps.clientsections.models import ClientSection, Category, Collection, ClientSplash
from cms.apps.utils.forms.widgets import MDTextarea
from cms.apps.utils.forms.widgets import ColorInput
from cms.apps.content.models import Collection as WorkCollection


class ClientSectionAdmin(admin.ModelAdmin):
    list_display = ('name', 'published', 'url')
    list_filter = ('published', )
    prepopulated_fields = {
        'slug': ('name',)
    }

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'menu_color':
            kwargs.pop('request')
            kwargs['widget'] = ColorInput(attrs={'size':'10'})
            return db_field.formfield(**kwargs)
        return super(ClientSectionAdmin, self).formfield_for_dbfield(db_field, **kwargs)

    def get_urls(self):
        from django.conf.urls.defaults import patterns, url
        # We handle custom views so that Collections & Categories can be
        # subsections of ClientSection admin instead of being their own top
        # level admin sections.
        custom_urls = patterns('',
            # Collection views
            url(r'^(\d+)/collection/$',
                self.admin_site.admin_view(self.collection_view_proxy),
                name='clientsections_collection_changelist',
            ),
            url(r'^(\d+)/collection/(.+)/$',
                self.admin_site.admin_view(self.collection_view_proxy)
            ),

            # Category views
            url(r'^(\d+)/category/$',
                self.admin_site.admin_view(self.category_view_proxy),
                name='clientsections_category_changelist',
            ),
            url(r'^(\d+)/category/(.+)/$',
                self.admin_site.admin_view(self.category_view_proxy)
            ),

            # ClientSplash views
            url(r'^(\d+)/clientsplash/$',
                self.admin_site.admin_view(self.clientsplash_view_proxy),
                name='clientsections_clientsplash_changelist',
            ),
            url(r'^(\d+)/clientsplash/(.+)/$',
                self.admin_site.admin_view(self.clientsplash_view_proxy)
            ),
        )
        return custom_urls + super(ClientSectionAdmin, self).get_urls()

    # Custom Views
    def collection_view_proxy(self, request, section_id, tail=''):
        """
        Proxy view for calling proper CollectionAdmin views.
        """
        section = get_object_or_404(ClientSection, id=section_id)
        root = reverse('admin:index')
        if tail and not tail.endswith('/'):
            tail = '%s/' % tail
        path = "%s%s/%s/%s" % (root,
                                Collection._meta.app_label,
                                Collection._meta.module_name,
                                tail)
        view, args, kwargs = resolve(path)
        request.section = section
        kwargs['extra_context'] = {
            'section': section,
        }
        request.GET = request.GET.copy()
        request.GET['section'] = section.pk
        return view(request, *args, **kwargs)

    def category_view_proxy(self, request, section_id, tail=''):
        """
        Proxy view for calling proper CategoryAdmin views.
        """
        section = get_object_or_404(ClientSection, id=section_id)
        root = reverse('admin:index')
        if tail and not tail.endswith('/'):
            tail = '%s/' % tail
        path = "%s%s/%s/%s" % (root,
                                Category._meta.app_label,
                                Category._meta.module_name,
                                tail)
        view, args, kwargs = resolve(path)
        request.section = section
        kwargs['extra_context'] = {
            'section': section,
        }
        request.GET = request.GET.copy()
        request.GET['section'] = section.pk
        return view(request, *args, **kwargs)

    def clientsplash_view_proxy(self, request, section_id, tail=''):
        """
        Proxy view for calling proper ClientSplashAdmin views.
        """
        section = get_object_or_404(ClientSection, id=section_id)
        root = reverse('admin:index')
        if tail and not tail.endswith('/'):
            tail = '%s/' % tail
        path = "%s%s/%s/%s" % (root,
                                ClientSplash._meta.app_label,
                                ClientSplash._meta.module_name,
                                tail)
        view, args, kwargs = resolve(path)
        request.section = section
        kwargs['extra_context'] = {
            'section': section,
        }
        request.GET = request.GET.copy()
        request.GET['section'] = section.pk
        return view(request, *args, **kwargs)


class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {
        'slug': ('name',)
    }

    def queryset(self, request):
        """
        Filters out the queryset by section if request.section exists.
        """
        qs = super(CategoryAdmin, self).queryset(request)
        section = getattr(request, 'section', None)
        if section:
            qs = qs.filter(section=section)
        return qs

    def get_urls(self):
        from django.conf.urls.defaults import patterns, url
        urls = super(CategoryAdmin, self).get_urls()
        info = self.model._meta.app_label, self.model._meta.module_name
        myurls = patterns('',
            url(r'^sort/$',
                self.admin_site.admin_view(self.sort_view),
                name='%s_%s_sort' % info),
        )
        return myurls + urls

    def sort_view(self, request):
        cats = request.POST.getlist('category')
        for sort, cat_id in enumerate(cats):
            category = Category.objects.get(id=cat_id)
            category.sort = sort
            category.save()
        return http.HttpResponse("ok")

    def changelist_view(self, request, **kwargs):
        section = getattr(request, 'section')
        if section:
            return http.HttpResponseRedirect(
                reverse('admin:clientsections_collection_changelist',
                    args=[section.pk]))


class ImportCollectionForm(forms.Form):
    """
    Copies a single "Our Work" collection into clientsecctions app.
    """
    existing_collection = forms.ModelChoiceField(
            label='"Our Work" Collections',
            help_text="Choose an existing collection to import",
            queryset=WorkCollection.objects.all())


class ImportCollectionsForm(forms.Form):
    """
    Copies multiple "Our Work" collections into clientsecctions app.
    """
    existing_collection = forms.ModelMultipleChoiceField(
            label='"Our Work" Collections',
            help_text=('Choose existing collections to import.<br/>'
                    'Hold down "Control", or "Command" on a Mac, to select more than one.'),
            queryset=WorkCollection.objects.all())


class CollectionAdmin(admin.ModelAdmin):
    exclude = ('sort',)
    list_filter = ('published', 'categories',)
    list_display = ('image_column', 'title', 'published',
            'date', 'category_column',)
    fieldsets = (
        ('New Collection', {
            'fields': ('section', 'categories', 'title',
                'slug', 'date', 'published', 'text',)
        }),
    )
    prepopulated_fields = {
        'slug': ('title',)
    }

    def queryset(self, request):
        """
        Filters out the queryset by section if request.section exists.
        """
        qs = super(CollectionAdmin, self).queryset(request)
        section = getattr(request, 'section', None)
        if section:
            qs = qs.filter(section=section)
        return qs

    def category_column(self, obj):
        return ", ".join(( c.name for c in obj.categories.all() ))
    category_column.short_description = 'Categories'

    def image_column(self, obj):
        t = Template('{% load thumbnail %}{% if img %}<img src="{% thumbnail img 60x60 crop %}" alt="{{img}}"/><br/>{% else %}{{obj.title}}{% endif %}')
        try:
            img = obj.images.all()[:1][0].filename
        except IndexError:
            try:
                img = obj.videos.all()[:1][0].image
            except IndexError:
                img = None
        return t.render(Context({
            'obj': obj,
            'img': img,
        }))
    image_column.allow_tags = True
    image_column.short_description = 'Image'

    def formfield_for_dbfield(self, db_field, **kwargs):
        RICH_FIELDS = ('text',)

        if db_field.name == 'title':
            kwargs.pop('request')
            kwargs['widget'] = forms.TextInput(attrs={'size': '60'})
            return db_field.formfield(**kwargs)
        elif db_field.name in RICH_FIELDS:
            kwargs.pop('request')
            kwargs['widget'] = MDTextarea(attrs={'rows': '10'})
            return db_field.formfield(**kwargs)
        elif db_field.name == 'categories':
            request = kwargs.get('request')
            if request:
                section = getattr(request, 'section', None)
                if section:
                    kwargs['queryset'] = section.categories.all()

        formfield = super(CollectionAdmin, self).formfield_for_dbfield(db_field, **kwargs)

        if db_field.name == 'section':
            formfield.empty_label = None

        return formfield

    def get_urls(self):
        from django.conf.urls.defaults import patterns, url
        # We handle custom views so that Collections & Categories can be
        # subsections of ClientSection admin instead of being their own top
        # level admin sections.
        custom_urls = patterns('',
            # Collection views
            url(r'import_collection/$',
                self.admin_site.admin_view(self.import_collection_view),
                name='clientsections_import_collection',
            ),
            url(r'import_collections/$',
                self.admin_site.admin_view(self.import_collections_view),
                name='clientsections_import_collections',
            ),
        )
        return custom_urls + super(CollectionAdmin, self).get_urls()

    def import_collection_view(self, request, extra_context=None):
        section = getattr(request, 'section')
        if request.method == 'POST':
            form = ImportCollectionForm(request.POST)
            if form.is_valid():
                collection = form.cleaned_data['existing_collection']
                new_collection = Collection.objects.copy_as_new(collection, section)
                return http.HttpResponseRedirect('../%s/' % new_collection.pk)
        else:
            form = ImportCollectionForm()

        opts = Collection._meta
        ctx = {
            'form': form,
            'opts': opts,
            'app_label': opts.app_label,
        }
        if extra_context:
            ctx.update(extra_context)

        return render_to_response('admin/clientsections/collection/import_collection.html', ctx,
                context_instance=RequestContext(request))

    def import_collections_view(self, request, extra_context):
        section = getattr(request, 'section')
        if request.method == 'POST':
            form = ImportCollectionsForm(request.POST)
            if form.is_valid():
                collections = form.cleaned_data['existing_collection']
                for col in collections:
                    new_collection = Collection.objects.copy_as_new(col, section)
                return http.HttpResponseRedirect('../')
        else:
            form = ImportCollectionsForm()

        opts = Collection._meta
        ctx = {
            'form': form,
            'opts': opts,
            'app_label': opts.app_label,
        }
        if extra_context:
            ctx.update(extra_context)

        return render_to_response('admin/clientsections/collection/import_collection.html', ctx,
                context_instance=RequestContext(request))

    def changelist_view(self, request, extra_context=None):
        # Get the categories in context.
        extra_context = extra_context or {}
        section = extra_context.get('section')
        categories = Category.objects.all()
        if section:
            categories = categories.filter(section=section)
        extra_context.update({
            'categories': categories
        })
        return super(CollectionAdmin, self).changelist_view(request, extra_context)


class ClientSplashAdmin(admin.ModelAdmin):
    list_display = ('created', 'active',)

    def queryset(self, request):
        """
        Filters out the queryset by section if request.section exists.
        """
        qs = super(ClientSplashAdmin, self).queryset(request)
        section = getattr(request, 'section', None)
        if section:
            qs = qs.filter(section=section)
        return qs


admin.site.register(ClientSection, ClientSectionAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Collection, CollectionAdmin)
admin.site.register(ClientSplash, ClientSplashAdmin)
