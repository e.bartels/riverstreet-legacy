from django.db import models
from django.contrib.sites.models import Site

from cms.apps.utils.fields import AutoSlugField
from cms.apps.media.models import RelatedImagesField, RelatedVideosField


class ClientSection(models.Model):
    name = models.CharField(max_length=200)
    slug = AutoSlugField(max_length=200, unique=True,
            prepopulate_from='name',
            help_text='Unique text identifier used in URLs.')
    published = models.BooleanField(default=True,
            help_text='Whether to publish on the site.')
    subtitle = models.CharField(max_length=15, blank=True,
            default='top picks',
            help_text="Text that appears below River Street logo.")
    tagline = models.CharField(max_length=100, blank=True,
            help_text="Text that appears beneath the main site menu.")
    menu_color = models.CharField('Highlight Color',
            max_length=10, blank=True, default="#049ED1",
            help_text="Choose a color for links & header text.")
    videos = RelatedVideosField()

    class Meta:
        verbose_name = 'Client Section'
        verbose_name_plural = 'Client Sections'

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('clientsections-splash', [self.slug])

    @property
    def url(self):
        site = Site.objects.get_current()
        return u'http://%s%s' % (site.domain, self.get_absolute_url())


class Category(models.Model):
    section = models.ForeignKey(ClientSection, related_name='categories')
    name = models.CharField(max_length=200)
    slug = AutoSlugField(max_length=200,
            prepopulate_from='name',
            help_text='Unique text identifier used in URLs.')
    published = models.BooleanField(default=True,
            help_text='Whether to publish on the site.')
    sort = models.IntegerField(default=0,
            help_text='Sort order, lower numbers will appear first on site.')

    videos = RelatedVideosField()

    class Meta:
        ordering = ('sort', 'id')
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'
        unique_together = ('section', 'slug',)

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('clientsections-collection-index', [self.section.slug, self.slug])


class CollectionManager(models.Manager):
    def copy_as_new(self, collection, section=None):
        """
        Takes another collection instance and makes a copy.  Note: this is
        really meant to make copies of content.Collection instances, but should
        work with other clientsection.Collection as well.
        """
        if not section:
            section = collection.section

        try:
            new_col = Collection.objects.filter(section=section,
                    slug__startswith=collection.slug)[0]
        except IndexError:
            new_col = Collection()

        new_col.section = section
        new_col.title = collection.title
        new_col.date = collection.date
        new_col.published = collection.published
        new_col.text = collection.text
        new_col.save()

        # Copy Media
        new_col.images = collection.images.all()
        new_col.videos = collection.videos.all()

        return new_col


class Collection(models.Model):
    """
    A Collection of images and/or files
    """
    section = models.ForeignKey(ClientSection, related_name='collections')
    title = models.CharField(max_length=200)
    slug = AutoSlugField(max_length=200,
            prepopulate_from='title',
            help_text='Unique text identifier used in URLs.')
    date = models.DateField(blank=True, null=True)
    published = models.BooleanField(default=True,
            help_text='Whether to publish on the site.')
    sort = models.IntegerField(default=0,
            help_text='Sort order, lower numbers will appear first on site.')
    text = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    categories = models.ManyToManyField('Category',
            related_name='collections')

    images = RelatedImagesField()
    videos = RelatedVideosField()

    objects = CollectionManager()

    class Meta:
        ordering = ('-date', 'id')
        verbose_name = 'Collection'
        verbose_name_plural = 'Collections'
        unique_together = ('section', 'slug',)

    def __unicode__(self):
        return u'%s' % self.title

    def has_media(self):
        if self.images.count() or self.files.count():
            return True
        return False

    @models.permalink
    def get_absolute_url(self):
        return ('clientsections-collection-view', [self.section.slug, self.slug])


class ClientSplash(models.Model):
    section = models.ForeignKey(ClientSection, related_name='splash_pages')
    created = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)
    videos = RelatedVideosField()

    class Meta:
        verbose_name = "Splash Page"
        verbose_name_plural = "Splash Pages"
        ordering = ('active', '-created',)

    def save(self, *args, **kwargs):
        if self.active:
            ClientSplash.objects.filter(section=self.section).update(active=False)
        super(ClientSplash, self).save(*args, **kwargs)
