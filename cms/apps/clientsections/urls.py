from django.conf.urls.defaults import *


urlpatterns = patterns('cms.apps.clientsections.views',
    url(r'^$', 'splash', name='clientsections-splash'),
    url(r'^cat/(?P<cat>[\w-]+)/$', 'index', name='clientsections-collection-index'),
    url(r'^(?P<slug>[\w-]+)/$', 'view', name='clientsections-collection-view'),
    url(r'^css/item.css$', 'item_css', name='clientsections-item_css'),
)
