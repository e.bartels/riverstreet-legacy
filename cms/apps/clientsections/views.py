from django import http
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext

from cms.apps.clientsections.models import (ClientSection, Collection,
                                            Category, ClientSplash)


def splash(request, section_slug):
    section = get_object_or_404(ClientSection,
                                slug=section_slug,
                                published=True)
    try:
        splash = section.splash_pages.get(active=True)
    except ClientSplash.DoesNotExist:
        splash = None

    return render_to_response('clientsections/splash.html', {
        'section': section,
        'splash': splash,
        'category': None,
    }, context_instance=RequestContext(request))


def index(request, section_slug, cat=None):
    """
    Index & search page for Collections
    """
    section = get_object_or_404(ClientSection,
                                slug=section_slug,
                                published=True)

    if cat:
        category = get_object_or_404(Category,
                                     section=section,
                                     slug=cat,
                                     published=True)
    else:
        try:
            category = section.categories.filter(published=True)[0]
        except IndexError:
            raise http.Http404

    # Store this category to retrieve when viewing a collection
    request.session['last_visited_category'] = category.slug

    ctx = {
        'section': section,
        'category': category,
    }

    # Splash page is shown if there are category videos
    if not category.videos.published().count():
        try:
            collection = category.collections.filter(published=True)[0]
            url = reverse('clientsections-collection-view',
                          args=(section.slug, collection.slug,))
            return http.HttpResponseRedirect(url)
        except IndexError:
            pass

    return render_to_response(
            'clientsections/category_splash.html',
            ctx,
            context_instance=RequestContext(request))


def view(request, section_slug, slug):
    section = get_object_or_404(ClientSection,
                                slug=section_slug,
                                published=True)

    collection = get_object_or_404(Collection,
                                   section=section,
                                   slug=slug,
                                   published=True)

    # Attempt to get last visted category
    last_visited_category = request.session.get('last_visited_category', '---')
    try:
        category = collection.categories.get(published=True,
                                             slug=last_visited_category)
    except Category.DoesNotExist:
        # Otherwise try to get the first category for the collection.
        try:
            category = collection.categories.filter(published=True)[0]
        except IndexError:
            category = None

    videos = collection.videos.published()
    images = collection.images.all()

    # Layout is hd if there are any vimeo videos
    layout = 'hd 'if any(v.has_vimeo_video for v in videos) else 'sd'

    return render_to_response('clientsections/collection/view.html', {
        'section': section,
        'collection': collection,
        'category': category,
        'videos': videos,
        'images': images,
        'layout': layout,
    }, context_instance=RequestContext(request))


def item_css(request, section_slug):
    """
    Dynamically generates CSS for colors in this section.
    """
    section = get_object_or_404(ClientSection,
                                slug=section_slug,
                                published=True)
    response = render_to_response('menus/item.css', {
        'color': section.menu_color,
    }, context_instance=RequestContext(request))
    response['Content-Type'] = 'text/css'
    return response
