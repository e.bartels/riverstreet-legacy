from django import template

from cms.apps.clientsections.models import ClientSection, Collection, Category

register = template.Library()


def collection_menu(section, category):
    """
    An inclusion tag to show a list of Collections.
    """
    categories = section.categories.filter(published=True)
    if category:
        collections = category.collections.filter(published=True)
    else:
        collections = Category.objects.none()
    return {
        'section': section,
        'categories': categories,
        'collections': collections,
        'category': category
    }
register.inclusion_tag('clientsections/includes/collection_menu.html')(collection_menu)
