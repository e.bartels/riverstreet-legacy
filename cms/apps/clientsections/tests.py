from django.test import TestCase

from cms.apps.clientsections.models import ClientSection, ClientSplash


class ClientSplashTest(TestCase):
    def setUp(self):
        for i in xrange(1, 9):
            section = ClientSection.objects.create(
                name="Test %s" % i,
                )
            for j in xrange(1,5):
                ClientSplash.objects.create(
                    section=section,
                   )

    def test_active_slashes(self):
        sections = ClientSection.objects.all()
        for section in sections:
            splashes = section.splash_pages.filter(active=True)
            self.assertEqual(splashes.count(), 1)

        section = sections[0]
        splashes = section.splash_pages.all()
        self.assertEqual(splashes.count(), 4)
        s = splashes[2]
        s.active = True
        s.save()

        self.assertEqual(s, splashes.filter(active=True)[0])

        for section in sections:
            splashes = section.splash_pages.filter(active=True)
            self.assertEqual(splashes.count(), 1)
