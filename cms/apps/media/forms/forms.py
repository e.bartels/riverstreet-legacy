from django import forms
from django.db.models.loading import get_model
from django.core.exceptions import PermissionDenied
from django.contrib.admin.widgets import ForeignKeyRawIdWidget

from fields import *
from widgets import *

from cms.apps.media.models import Image, File, Video
from cms.apps.media.models import ImageRelation, FileRelation, VideoRelation

__all__ = ('AddImageForm', 'AddFileForm', 'AddRelatedImageForm',
           'AddRelatedFileForm', 'AddRelatedVideoForm',)


class AddImageForm(forms.Form):
    id = forms.IntegerField(required=False, widget=forms.HiddenInput)
    imagefile = forms.ImageField(label='New Image')


class AddFileForm(forms.Form):
    id = forms.IntegerField(required=False, widget=forms.HiddenInput)
    file = forms.FileField(label='New File')


class AddRelatedImageForm(forms.Form):
    """
    Given the pk of a related model, this form will add a newly uploaded image
    and associate it with the related model.
    This assumes that the related model has a m2m relationship with Image.
    """
    id = forms.IntegerField(required=True, widget=forms.HiddenInput)
    model = forms.CharField(widget=forms.HiddenInput)
    imagefile = forms.ImageField(label='New Image', required=False)
    image = forms.IntegerField(label='Existing Image', required=False,
            widget=ForeignKeyRawIdWidget(
                rel=ImageRelation._meta.get_field('item').rel,
                attrs={'style': 'width:2em !important;'}))

    def clean_image(self):
        image_id = self.cleaned_data.get('image')
        try:
            return Image.objects.get(pk=image_id)
        except Image.DoesNotExist:
            return None

    def clean(self):
        if not self.cleaned_data.get('image') and not self.cleaned_data.get('imagefile'):
            raise forms.ValidationError('No image specified')
        return self.cleaned_data


    def save(self, user):
        # Get the related class
        (app_label, model_name) = self.cleaned_data['model'].split('.')
        related_class = get_model(app_label, model_name)

        # Check permissions
        change_perm = "%s.%s" % (app_label, related_class._meta.get_change_permission())
        if not user.has_perm(change_perm):
            raise PermissionDenied

        # Create a new image.
        image_file = self.cleaned_data['imagefile']
        if image_file:
            image = Image()
            image.filename.save(image_file.name, image_file, save=False)
            image.save()
        else:
            image = self.cleaned_data['image']

        # Attach image to the related object.
        related_obj = related_class.objects.get(pk=self.cleaned_data['id'])
        related_obj.images.add(image)
        return (related_obj, image)


class AddRelatedFileForm(forms.Form):
    """
    Given the pk of a related model, this form will add a newly uploaded file
    and associate it with the related model.
    This assumes that the related model has a m2m relationship with File.
    """
    id = forms.IntegerField(required=True, widget=forms.HiddenInput)
    model = forms.CharField(widget=forms.HiddenInput)
    filefile = forms.FileField(label='New File', required=False)
    file = forms.IntegerField(label='Existing File', required=False,
            widget=ForeignKeyRawIdWidget(
                rel=FileRelation._meta.get_field('item').rel,
                attrs={'style': 'width:2em !important;'}))

    def clean_file(self):
        file_id = self.cleaned_data.get('file')
        try:
            return File.objects.get(pk=file_id)
        except File.DoesNotExist:
            return None

    def clean(self):
        if not self.cleaned_data.get('file') and not self.cleaned_data.get('filefile'):
            raise forms.ValidationError('No file specified')
        return self.cleaned_data


    def save(self, user):
        # Get the related class
        (app_label, model_name) = self.cleaned_data['model'].split('.')
        related_class = get_model(app_label, model_name)

        # Check permissions
        change_perm = "%s.%s" % (app_label, related_class._meta.get_change_permission())
        if not user.has_perm(change_perm):
            raise PermissionDenied

        # Create a new file
        file_file = self.cleaned_data['filefile']
        if file_file:
            file = File()
            file.filename.save(file_file.name, file_file, save=False)
            file.save()
        else:
            file = self.cleaned_data['file']

        # Attach file to the related object
        related_obj = related_class.objects.get(pk=self.cleaned_data['id'])
        related_obj.files.add(file)
        return (related_obj, file)


class AddRelatedVideoForm(forms.Form):
    """
    Given the pk of a related model, this form will add a newly uploaded video
    and associate it with the related model.
    This assumes that the related model has a m2m relationship with Video.
    """
    id = forms.IntegerField(required=True, widget=forms.HiddenInput)
    model = forms.CharField(widget=forms.HiddenInput)
    videofile = forms.FileField(label='New Video', required=False)
    video = forms.IntegerField(label='Existing Video', required=False,
            widget=ForeignKeyRawIdWidget(
                rel=VideoRelation._meta.get_field('item').rel,
                attrs={'style': 'width:2em !important;'}))

    def clean_video(self):
        video_id = self.cleaned_data.get('video')
        try:
            return Video.objects.get(pk=video_id)
        except Video.DoesNotExist:
            return None

    def clean(self):
        if not self.cleaned_data.get('video') and not self.cleaned_data.get('videofile'):
            raise forms.ValidationError('No video specified')
        return self.cleaned_data

    def save(self, user):
        # Get the related class
        (app_label, model_name) = self.cleaned_data['model'].split('.')
        related_class = get_model(app_label, model_name)

        # Check permissions
        change_perm = "%s.%s" % (app_label, related_class._meta.get_change_permission())
        if not user.has_perm(change_perm):
            raise PermissionDenied

        # Create a new video
        video_file = self.cleaned_data['videofile']
        if video_file:
            video = Video()
            video.filename.save(video_file.name, video_file, save=False)
            video.save()
        else:
            video = self.cleaned_data['video']

        # Attach video to the related object
        related_obj = related_class.objects.get(pk=self.cleaned_data['id'])
        related_obj.videos.add(video)
        return (related_obj, video)
