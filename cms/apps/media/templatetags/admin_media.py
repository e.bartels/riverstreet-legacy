from django.template import Library
from django.utils.safestring import mark_safe
from django.contrib.contenttypes.models import ContentType

from cms.apps.media.forms import AddRelatedImageForm, AddRelatedFileForm, AddRelatedVideoForm
from cms.apps.media.models import ImageRelation, FileRelation

register = Library()

def image_sorter(context, model, title="Images"):
    """
    Adds a sortable list of images for models with a RelatedImagesField
    """
    form = AddRelatedImageForm(initial={
        'id': model.id,
        'model': "%s.%s" % (model._meta.app_label, model._meta.object_name),
    })
    
    context.update({
        'model' : model,
        'form' : form,
        'meta': model._meta,
        'title': title,
    })
    return context
register.inclusion_tag("admin/media/includes/image_sort.html", takes_context=True)(image_sorter)

def file_sorter(context, model, title="Files"):
    """
    Adds a sortable list of files for models with a RelatedFilesField
    """
    form = AddRelatedFileForm(initial={
        'id':model.id,
        'model': "%s.%s" % (model._meta.app_label, model._meta.object_name),
    })

    context.update({
        'model' : model,
        'form' : form,
        'meta': model._meta,
        'title': title,
    })
    return context
register.inclusion_tag("admin/media/includes/file_sort.html", takes_context=True)(file_sorter)

def video_sorter(context, model, title="Videos"):
    """
    Adds a sortable list of videos for models with a RelatedVideosField
    """
    form = AddRelatedVideoForm(initial={
        'id':model.id,
        'model': "%s.%s" % (model._meta.app_label, model._meta.object_name),
    })

    context.update({
        'model' : model,
        'form' : form,
        'meta': model._meta,
        'title': title,
    })
    return context
register.inclusion_tag("admin/media/includes/video_sort.html", takes_context=True)(video_sorter)

def image_list_filter(context):
    """
    Adds list filter for models related to an Image in the admin.
    """
    request = context['request']
    selected_model = None
    selected_id = None
    if 'relations__content_type__model' in request.GET:
        selected_model = request.GET['relations__content_type__model']
        if 'relations__object_id' in request.GET:
            selected_id = request.GET['relations__object_id']

    filters = []
    for model in ImageRelation.get_related_models():
        model_name = model._meta.module_name
        ctype = ContentType.objects.get_for_model(model)
        objs = model.objects.all()
        selected = None
        if selected_model == ctype.model:
            selected = selected_id
        filters.append({
            'name': model_name,
            'ctype': ctype,
            'objects': objs,
            'selected': selected,
        })
    
    return {
        'filters': filters
    }
register.inclusion_tag("admin/media/includes/image_list_filter.html",
        takes_context=True)(image_list_filter)
