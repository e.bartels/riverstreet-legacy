# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MediaItem'
        db.create_table('media_mediaitem', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('slug', self.gf('cms.apps.utils.fields.AutoSlugField')(prepopulate_from='title', unique=True, max_length=200)),
            ('published', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('caption', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('featured', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('tags', self.gf('tagging.fields.TagField')()),
        ))
        db.send_create_signal('media', ['MediaItem'])

        # Adding model 'File'
        db.create_table('media_file', (
            ('mediaitem_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['media.MediaItem'], unique=True, primary_key=True)),
            ('filename', self.gf('cms.apps.media.fields.CustomFileField')(max_length=100)),
        ))
        db.send_create_signal('media', ['File'])

        # Adding model 'Image'
        db.create_table('media_image', (
            ('mediaitem_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['media.MediaItem'], unique=True, primary_key=True)),
            ('filename', self.gf('cms.apps.media.fields.CustomImageField')(max_length=100)),
            ('width', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('height', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('header', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('work_page', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('media', ['Image'])

        # Adding model 'Video'
        db.create_table('media_video', (
            ('mediaitem_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['media.MediaItem'], unique=True, primary_key=True)),
            ('filename', self.gf('cms.apps.media.fields.CustomFileField')(max_length=100)),
            ('mp4', self.gf('cms.apps.media.fields.CustomFileField')(max_length=100, blank=True)),
            ('image', self.gf('cms.apps.media.fields.CustomImageField')(max_length=100, blank=True)),
            ('conversion_error_flag', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('media', ['Video'])

        # Adding model 'ImageRelation'
        db.create_table('media_imagerelation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('sort', self.gf('django.db.models.fields.PositiveIntegerField')(default=0, null=True, blank=True)),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(related_name='relations', to=orm['media.Image'])),
        ))
        db.send_create_signal('media', ['ImageRelation'])

        # Adding unique constraint on 'ImageRelation', fields ['item', 'object_id', 'content_type']
        db.create_unique('media_imagerelation', ['item_id', 'object_id', 'content_type_id'])

        # Adding model 'FileRelation'
        db.create_table('media_filerelation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('sort', self.gf('django.db.models.fields.PositiveIntegerField')(default=0, null=True, blank=True)),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(related_name='relations', to=orm['media.File'])),
        ))
        db.send_create_signal('media', ['FileRelation'])

        # Adding unique constraint on 'FileRelation', fields ['item', 'object_id', 'content_type']
        db.create_unique('media_filerelation', ['item_id', 'object_id', 'content_type_id'])

        # Adding model 'VideoRelation'
        db.create_table('media_videorelation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('sort', self.gf('django.db.models.fields.PositiveIntegerField')(default=0, null=True, blank=True)),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(related_name='relations', to=orm['media.Video'])),
        ))
        db.send_create_signal('media', ['VideoRelation'])

        # Adding unique constraint on 'VideoRelation', fields ['item', 'object_id', 'content_type']
        db.create_unique('media_videorelation', ['item_id', 'object_id', 'content_type_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'VideoRelation', fields ['item', 'object_id', 'content_type']
        db.delete_unique('media_videorelation', ['item_id', 'object_id', 'content_type_id'])

        # Removing unique constraint on 'FileRelation', fields ['item', 'object_id', 'content_type']
        db.delete_unique('media_filerelation', ['item_id', 'object_id', 'content_type_id'])

        # Removing unique constraint on 'ImageRelation', fields ['item', 'object_id', 'content_type']
        db.delete_unique('media_imagerelation', ['item_id', 'object_id', 'content_type_id'])

        # Deleting model 'MediaItem'
        db.delete_table('media_mediaitem')

        # Deleting model 'File'
        db.delete_table('media_file')

        # Deleting model 'Image'
        db.delete_table('media_image')

        # Deleting model 'Video'
        db.delete_table('media_video')

        # Deleting model 'ImageRelation'
        db.delete_table('media_imagerelation')

        # Deleting model 'FileRelation'
        db.delete_table('media_filerelation')

        # Deleting model 'VideoRelation'
        db.delete_table('media_videorelation')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'media.file': {
            'Meta': {'object_name': 'File', '_ormbases': ['media.MediaItem']},
            'filename': ('cms.apps.media.fields.CustomFileField', [], {'max_length': '100'}),
            'mediaitem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['media.MediaItem']", 'unique': 'True', 'primary_key': 'True'})
        },
        'media.filerelation': {
            'Meta': {'ordering': "('content_type', 'sort')", 'unique_together': "(('item', 'object_id', 'content_type'),)", 'object_name': 'FileRelation'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'relations'", 'to': "orm['media.File']"}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'sort': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'})
        },
        'media.image': {
            'Meta': {'object_name': 'Image', '_ormbases': ['media.MediaItem']},
            'filename': ('cms.apps.media.fields.CustomImageField', [], {'max_length': '100'}),
            'header': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'height': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'mediaitem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['media.MediaItem']", 'unique': 'True', 'primary_key': 'True'}),
            'width': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'work_page': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'media.imagerelation': {
            'Meta': {'ordering': "('content_type', 'sort')", 'unique_together': "(('item', 'object_id', 'content_type'),)", 'object_name': 'ImageRelation'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'relations'", 'to': "orm['media.Image']"}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'sort': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'})
        },
        'media.mediaitem': {
            'Meta': {'object_name': 'MediaItem'},
            'caption': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'featured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'slug': ('cms.apps.utils.fields.AutoSlugField', [], {'prepopulate_from': "'title'", 'unique': 'True', 'max_length': '200'}),
            'tags': ('tagging.fields.TagField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        },
        'media.video': {
            'Meta': {'object_name': 'Video', '_ormbases': ['media.MediaItem']},
            'conversion_error_flag': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'filename': ('cms.apps.media.fields.CustomFileField', [], {'max_length': '100'}),
            'image': ('cms.apps.media.fields.CustomImageField', [], {'max_length': '100', 'blank': 'True'}),
            'mediaitem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['media.MediaItem']", 'unique': 'True', 'primary_key': 'True'}),
            'mp4': ('cms.apps.media.fields.CustomFileField', [], {'max_length': '100', 'blank': 'True'})
        },
        'media.videorelation': {
            'Meta': {'ordering': "('content_type', 'sort')", 'unique_together': "(('item', 'object_id', 'content_type'),)", 'object_name': 'VideoRelation'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'relations'", 'to': "orm['media.Video']"}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'sort': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'})
        },
        'tagging.tag': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'})
        },
        'tagging.taggeditem': {
            'Meta': {'unique_together': "(('tag', 'content_type', 'object_id'),)", 'object_name': 'TaggedItem'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tag': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'items'", 'to': "orm['tagging.Tag']"})
        }
    }

    complete_apps = ['media']