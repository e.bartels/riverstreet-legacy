import os

from django.test import TestCase
from django.db import models
from django.core.files import File as DjangoFile
from django.conf import settings

from cms.apps.media.models import (
        Image, ImageRelation, File, FileRelation, Video, VideoRelation)
from cms.apps.media.models import (
        RelatedImagesField, RelatedFilesField, RelatedVideosField)


VIMEO_VIDEO_URL = 'https://vimeo.com/8129975'


# Test models
class RegularSet(models.Model):
    title = models.CharField(max_length=20)
    images = RelatedImagesField()
    files = RelatedFilesField()
    videos = RelatedVideosField()

class RegularSet2(models.Model):
    title = models.CharField(max_length=20)
    images = RelatedImagesField()
    files = RelatedFilesField()
    videos = RelatedVideosField()


class ChildSet(RegularSet):
    notes = models.TextField()


class AbstractSet(models.Model):
    title = models.CharField(max_length=20)
    images = RelatedImagesField()
    files = RelatedFilesField()
    videos = RelatedVideosField()

    class Meta:
        abstract = True


class AbsChildSet(AbstractSet):
    description = models.TextField(blank=True)


class AltNameModel(models.Model):
    image_attachments = RelatedImagesField()


# Test file locations
TEST_IMAGE_DIR = settings.TEST_IMAGE_DIR
TEST_FILE_DIR = settings.TEST_FILE_DIR
TEST_VIDEO_DIR = settings.TEST_VIDEO_DIR

IMAGES = []
FILES = []
VIDEOS = []


def load_files(file_path, file_list, filt=lambda x: x):
    paths = []
    for filename in sorted(os.listdir(file_path)):
        paths.append(os.path.join(file_path, filename))

    paths = filter(filt, paths)

    for path in paths:
        f = DjangoFile(open(path, 'r'))
        file_list.append(f)


load_files(TEST_IMAGE_DIR, IMAGES)
load_files(TEST_FILE_DIR, FILES)
load_files(TEST_VIDEO_DIR, VIDEOS)


# Tests
class ImageTest(TestCase):
    def test_image_creation(self):
        image = Image()
        image.filename.save('Test.jpg', IMAGES[0])

        if not os.path.exists(image.filename.path):
            self.fail("Image file was saved properly.")

        self.assertEqual(image.title, 'Test')
        self.assertEqual(image.type, 'image')
        self.assertEqual(image.mediaitem_ptr.type, 'image')

        image.delete()

    def test_delete(self):
        image = Image()
        image.filename.save('test', IMAGES[0])
        filepath = image.filename.path
        image.delete()

        if os.path.exists(filepath):
            self.fail("Associated file was not deleted after instance deletion.")

    def tearDown(self):
        Image.objects.all().delete()


class FileTest(TestCase):
    def test_file_creation(self):
        file = File()
        file.filename.save('test', FILES[0])

        if not os.path.exists(file.filename.path):
            self.fail("File file was saved properly.")

        self.assertEqual(file.title, 'Test')
        self.assertEqual(file.type, 'file')
        self.assertEqual(file.mediaitem_ptr.type, 'file')

        file.delete()

    def test_delete(self):
        file = File()
        file.filename.save('1', FILES[0])
        filepath = file.filename.path
        file.delete()

        if os.path.exists(filepath):
            self.fail("Associated file was not deleted after instance deletion.")

    def tearDown(self):
        File.objects.all().delete()


class VideoTest(TestCase):
    def test_video_creation(self):
        video = Video()
        video.filename.save('test', FILES[0])

        if not os.path.exists(video.filename.path):
            self.fail("Video video was saved properly.")

        self.assertEqual(video.title, 'Test')
        self.assertEqual(video.type, 'video')
        self.assertEqual(video.mediaitem_ptr.type, 'video')

        video.delete()

    def test_delete(self):
        video = Video()
        video.filename.save('1', FILES[0])
        videopath = video.filename.path
        video.delete()

        if os.path.exists(videopath):
            self.fail("Associated video was not deleted after "
                      "instance deletion.")

    def test_video_manager(self):
        video = Video()
        video.filename.save('test', FILES[0])
        self.assertEqual(Video.objects.count(), 1)
        self.assertEqual(Video.objects.encoded().count(), 0)

        video.mp4 = 'blah'
        video.save()
        self.assertEqual(Video.objects.encoded().count(), 1)
        self.assertEqual(Video.objects.published().count(), 1)

        video2 = Video(url=VIMEO_VIDEO_URL)
        video2.save()
        self.assertFalse(video2.mp4)
        self.assertEqual(Video.objects.encoded().count(), 1)
        self.assertEqual(Video.objects.published().count(), 2)

        video2.published = False
        video2.save()
        self.assertEqual(Video.objects.published().count(), 1)

    def tearDown(self):
        Video.objects.all().delete()


class ImageRelationTest(TestCase):
    def setUp(self):
        self.files = IMAGES
        for i, f in enumerate(self.files):
            image = Image()
            name, ext = os.path.splitext(f.name)
            image.filename.save('image_%s.%s' % (i, ext), f)

    def _do_relation_test(self, set_class):
        set = set_class.objects.create(title='TESTING')

        # Assign single image
        image = Image.objects.all()[0]
        set.images.add(image)
        self.assertEqual(image.relations.count(), 1, "Add one image failed.")

        # Add one image
        image2 = Image.objects.all()[1]
        set.images.add(image2)
        self.assertEqual(set.images.count(), 2, "Add a second image failed.")

        # Assign random images
        set.images = Image.objects.all().order_by('?')
        self.assertEqual(set.images.count(), len(self.files), "Adding random images failed.")

        # Test sort order change
        first_image = set.images.all()[0]
        set.images.set_position(first_image.pk, set.images.count())
        self.assertEqual(first_image.pk, set.images.all().reverse()[0].pk, "Reordering failed.")

        # Test removing an image
        set.images.remove(image)
        self.failIf(image in set.images.all(), "Image was not removed properly.")

        # Tests that related object deletion also deletes the ImageRelation
        set.delete()
        self.assertEqual(image.relations.count(), 0)
        self.assertEqual(ImageRelation.objects.count(), 0)

        # Images should still be there after though
        self.assertEqual(Image.objects.count(), len(self.files), "Image count is incorrect.")

    def test_image_relations(self):
        self._do_relation_test(RegularSet)

    def test_abstract_subclasses(self):
        self._do_relation_test(AbsChildSet)

    def test_subclass(self):
        self._do_relation_test(ChildSet)

    def test_media_registry(self):
        for klass in (RegularSet, ChildSet, AbsChildSet):
            self.failUnless(ImageRelation.relates_to(klass),
                "%s.%s was not found in the list of related models." % (klass._meta.app_label, klass._meta.object_name))
        self.failIf(ImageRelation.relates_to(AbstractSet))

    def test_alt_field_name(self):
        a = AltNameModel.objects.create()
        a.image_attachments = Image.objects.all()
        self.assertEqual(a.image_attachments.count(), len(self.files), "Failed with alternate field name")

    def test_object_deletion(self):
        set1 = RegularSet(title='test 1')
        set1.save()
        set2 = RegularSet2(title='test 1')
        set2.save()

        set1.images = Image.objects.all()
        set2.images = Image.objects.all() #set1.images.all()

        self.assertEqual(set1.images.count(), set2.images.count())
        self.assertEqual(ImageRelation.objects.count(), Image.objects.count()*2)

        # This test fails with django 1.1
        RegularSet.objects.all().delete()
        self.assertEqual(set2.images.count(), Image.objects.count())

        set1 = RegularSet(title='test 1')
        set1.save()

        set1.images = Image.objects.all()
        self.assertEqual(set1.images.count(), set2.images.count())
        set1.images.remove(set1.images.all()[1])
        self.assertEqual(set2.images.count(), Image.objects.count())
        self.assertEqual(set2.images.count(), set1.images.count()+1)

        set1.images.clear()
        self.assertEqual(set1.images.count(), 0)
        self.assertEqual(set2.images.count(), Image.objects.count())
        self.assertEqual(ImageRelation.objects.count(), Image.objects.count())

    def tearDown(self):
        Image.objects.all().delete()


class FileRelationTest(TestCase):
    def setUp(self):
        self.files = FILES
        for i, f in enumerate(self.files):
            file = File()
            name, ext = os.path.splitext(f.name)
            file.filename.save('file_%s.%s' % (i, ext), f)

    def _do_relation_test(self, set_class):
        set = set_class.objects.create(title='TESTING')

        # Assign single file
        file = File.objects.all()[0]
        set.files.add(file)
        self.assertEqual(file.relations.count(), 1, "Add one file failed.")

        # Add one file
        file2 = File.objects.all()[1]
        set.files.add(file2)
        self.assertEqual(set.files.count(), 2, "Add a second file failed.")

        # Assign random files
        set.files = File.objects.all().order_by('?')
        self.assertEqual(set.files.count(), len(self.files), "Add random files failed")

        # Test sort order change
        first_file = set.files.all()[0]
        set.files.set_position(first_file.pk, set.files.count())
        self.assertEqual(first_file.pk, set.files.all().reverse()[0].pk, "Reordering failed.")

        # Test removing a file
        set.files.remove(file)
        self.failIf(file in set.files.all(), "File was not removed properly.")

        # Tests that related object deletion also deletes the FileRelation
        set.delete()
        self.assertEqual(file.relations.count(), 0)
        self.assertEqual(FileRelation.objects.count(), 0)

        # Files should still be there after though
        self.assertEqual(File.objects.count(), len(self.files), "File count is incorrect.")

    def test_file_relations(self):
        self._do_relation_test(RegularSet)

    def test_abstract_subclasses(self):
        self._do_relation_test(AbsChildSet)

    def test_subclass(self):
        self._do_relation_test(ChildSet)

    def test_media_registry(self):
        for klass in (RegularSet, ChildSet, AbsChildSet):
            self.failUnless(FileRelation.relates_to(klass),
                "%s.%s was not found in the list of related models." % (klass._meta.app_label, klass._meta.object_name))
        self.failIf(FileRelation.relates_to(AbstractSet))

    def tearDown(self):
        File.objects.all().delete()


class VideoRelationTest(TestCase):
    def setUp(self):
        self.files = VIDEOS
        for i, f in enumerate(self.files):
            video = Video()
            name, ext = os.path.splitext(f.name)
            video.filename.save('video_%s.%s' % (i, ext), f)

    def _do_relation_test(self, set_class):
        set = set_class.objects.create(title='TESTING')

        # Assign single video
        video = Video.objects.all()[0]
        set.videos.add(video)
        self.assertEqual(video.relations.count(), 1, "Add one video failed.")

        # Add one video
        video2 = Video.objects.all()[1]
        set.videos.add(video2)
        self.assertEqual(set.videos.count(), 2, "Add a second video failed.")

        # Assign random videos
        set.videos = Video.objects.all().order_by('?')
        self.assertEqual(set.videos.count(), len(self.files), "Adding random videos failed.")

        # Test sort order change
        first_video = set.videos.all()[0]
        set.videos.set_position(first_video.pk, set.videos.count())
        self.assertEqual(first_video.pk, set.videos.all().reverse()[0].pk, "Reordering failed.")

        # Test video manager methods.
        self.assertEqual(set.videos.published().count(), 0)
        self.assertEqual(set.videos.encoded().count(), 0)
        self.assertEqual(set.videos.unencoded().count(), len(self.files))
        self.assertEqual(set.videos.errors().count(), 0)

        # Test removing an video
        set.videos.remove(video)
        self.failIf(video in set.videos.all(), "Video was not removed properly.")

        # Tests that related object deletion also deletes the VideoRelation
        set.delete()
        self.assertEqual(video.relations.count(), 0)
        self.assertEqual(VideoRelation.objects.count(), 0)

        # Videos should still be there after though
        self.assertEqual(Video.objects.count(), len(self.files), "Video count is incorrect.")


    def test_video_relations(self):
        self._do_relation_test(RegularSet)

    def test_abstract_subclasses(self):
        self._do_relation_test(AbsChildSet)

    def test_subclass(self):
        self._do_relation_test(ChildSet)

    def test_media_registry(self):
        for klass in (RegularSet, ChildSet, AbsChildSet):
            self.failUnless(VideoRelation.relates_to(klass),
                "%s.%s was not found in the list of related models." % (klass._meta.app_label, klass._meta.object_name))
        self.failIf(VideoRelation.relates_to(AbstractSet))

    def test_alt_field_name(self):
        a = AltNameModel.objects.create()
        a.video_attachments = Video.objects.all()
        self.assertEqual(a.video_attachments.count(), len(self.files), "Failed with alternate field name")

    def test_object_deletion(self):
        set1 = RegularSet(title='test 1')
        set1.save()
        set2 = RegularSet2(title='test 1')
        set2.save()

        set1.videos = Video.objects.all()
        set2.videos = Video.objects.all() #set1.videos.all()

        self.assertEqual(set1.videos.count(), set2.videos.count())
        self.assertEqual(VideoRelation.objects.count(), Video.objects.count()*2)

        # This test fails with django 1.1
        RegularSet.objects.all().delete()
        self.assertEqual(set2.videos.count(), Video.objects.count())

        set1.videos = Video.objects.all()
        self.assertEqual(set1.videos.count(), set2.videos.count())
        set1.videos.remove(set1.videos.all()[1])
        self.assertEqual(set2.videos.count(), Video.objects.count())
        self.assertEqual(set2.videos.count(), set1.videos.count()+1)

        set1.videos.clear()
        self.assertEqual(set1.videos.count(), 0)
        self.assertEqual(set2.videos.count(), Video.objects.count())
        self.assertEqual(VideoRelation.objects.count(), Video.objects.count())

    def tearDown(self):
        Video.objects.all().delete()
