from django.db import models
from django.db.models.fields.related import RelatedField, ManyToManyRel
from django.contrib.contenttypes.generic import GenericRelation


class RelatedMediaRel(ManyToManyRel):
    def __init__(self, to, related_name):
        self.to = to
        self.related_name = related_name
        self.limit_choices_to = {}
        self.symmetrical = True
        self.multiple = True
        self.through = None


class RelatedMediaField(RelatedField):
    """
    A Field that provides access to RelatedMediaManager, which allows any
    model class to manage a set of MediaItem instances (can change/add/remove).
    """
    def __init__(self, verbose_name='Media Items', through=None):
        from cms.apps.media.models import MediaRelation

        if self.__class__ == RelatedMediaField:
            raise ValueError('RelatedMediaField cannot be used directly.  You must subclass it.')

        self.use_gfk = through is None or issubclass(through, MediaRelation)
        self.through = through
        models.Field.creation_counter += 1
        self.creation_counter = models.Field.creation_counter
        self.rel = RelatedMediaRel(
                    to=self.through._meta.get_field("item").rel.to,
                    related_name='mediarel_%s' % self.creation_counter)
        self.verbose_name = verbose_name
        self.help_text = None
        self.blank = True
        self.editable = True
        self.unique = False
        self.creates_table = False
        self.db_column = None
        self.choices = None
        self.serialize = False
        self.null = True
        self.default = None

    def __get__(self, instance, model):
        if instance is not None and instance.pk is None:
            raise ValueError("%s objects need to have a primary key value "
                "before you can access media items." % model.__name__)

        # Builds the related media manager by inheriting from the related
        # model's default manager and from _RelatedMediaManager. This way
        # methods on the default manager are also available on the related
        # media manager.
        _RelatedManager = type(
                'Related%sManager' % self.rel.to._meta.module_name.title(),
                (type(self.rel.to._default_manager), _RelatedMediaManager),
                {})

        manager = _RelatedManager(
            through=self.through, model=model, instance=instance
        )
        return manager

    def __set__(self, instance, value):
        if instance is not None and instance.pk is None:
            raise ValueError("%s objects need to have a primary key value "
                "before you can access media items." % type(instance).__name__)
        manager = self.__get__(instance, instance.__class__)
        manager.set(*value)

    def contribute_to_class(self, cls, name):
        self.name = self.column = name
        self.model = cls
        cls._meta.add_field(self)
        setattr(cls, name, self)

        # Set up related_name
        if self.use_gfk and not cls._meta.abstract:
            related_items = GenericRelation(self.through,
                    related_name= cls._meta.app_label +'_%(class)s_set_' + str(self.creation_counter))
            related_items.contribute_to_class(cls, "related_%s" % name)

        # Add to media registry
        if not cls._meta.abstract:
            opts = self.through._meta
            if not hasattr(opts, '_media_registry'):
                opts._media_registry = []
            if cls not in opts._media_registry:
                opts._media_registry.append(cls)

        # Some Helper classes
        setattr(cls, 'has_related_media', True)
        media_module = self.through.media_model()._meta.module_name
        setattr(cls, 'has_related_%ss' % media_module, True)

    def formfield(self, **kwargs):
        return None

    def value_from_object(self, instance):
        if instance.pk:
            return self.through.objects.filter(**self.through.lookup_kwargs(instance))
        return self.through.objects.none()

    def related_query_name(self):
        return self.model._meta.module_name

    def m2m_reverse_name(self):
        return self.through._meta.get_field_by_name("item")[0].column

    def m2m_target_field_name(self):
        return self.model._meta.pk.name

    def m2m_reverse_target_field_name(self):
        return self.rel.to._meta.pk.name

    def m2m_column_name(self):
        if self.use_gfk:
            return self.through._meta.virtual_fields[0].fk_field
        return self.through._meta.get_field('content_object').column

    def db_type(self, connection=None):
        return None

    def get_internal_type(self):
        return "ManyToManyField"

    def m2m_db_table(self):
        return self.through._meta.db_table

    def bulk_related_objects(self, objs, using):
        if self.use_gfk:
            return self.through._base_manager.db_manager(using).filter(
                **self.through.bulk_lookup_kwargs(objs)
            )
        return []


class _RelatedMediaManager(models.Manager):
    def __init__(self, through, model, instance):
        self.through = through
        self.model = model
        self.instance = instance

    def get_query_set(self):
        return self.through.media_for(self.model, self.instance)

    def _lookup_kwargs(self):
        return self.through.lookup_kwargs(self.instance)

    def add(self, *items):
        for item in items:
            if not isinstance(item, self.through.media_model()):
                raise ValueError("Can only add items of type %s" % self.through)
            self.through.objects.get_or_create(item=item, **self._lookup_kwargs())

    def set(self, *items):
        self.clear()
        self.add(*items)

    def remove(self, *items):
        self.through.objects.filter(**self._lookup_kwargs()).filter(
            item__in=items).delete()

    def clear(self):
        self.through.objects.filter(**self._lookup_kwargs()).delete()

    def set_position(self, item, position):
        if isinstance(item, models.Model) and not isinstance(item, self.through.media_model()):
            raise ValueError("Can only set position for items of type %s"
                    % self.through.media_model)
        relation = self.through.objects.filter(**self._lookup_kwargs()).get(item=item)
        relation.sort = position
        relation.save()

    def set_order(self, item_id, order):
        self.set_position(item_id, order)
