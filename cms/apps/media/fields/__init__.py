import types

from django.db.models import ImageField, FileField, signals
from django.core.files.uploadedfile import UploadedFile


class CustomFileField(FileField):
    """
    Adds customizations to django's built-in FileField
    """

    def contribute_to_class(self, cls, name):
        """Hook up events so we can access the instance."""
        super(CustomFileField, self).contribute_to_class(cls, name)
        signals.post_delete.connect(self._post_delete, sender=cls)

    def _post_delete(self, instance=None, **kwargs):
        self._maybe_delete_file(instance)

    def _maybe_delete_file(self, instance):
        """
        Deletes the file associated with this field,
        but only if no other model references the same file.
        returns True if the file was deleted
        """
        file = getattr(instance, self.attname)
        if file:
            # If the file exists and no other object of this type references it,
            # delete it from the filesystem.
            if file.storage.exists(file.name):
                existing = instance.__class__._default_manager.filter(**{self.name: file.name}).exclude(pk=instance.pk)
                if not existing:
                    file.delete(save=False)
                    return True
        return False

    def save_form_data(self, instance, data):
        """
        Here we're handling two possibilities.  If the data is indexable, then
        we're dealing with a RemoveableFileFormField, which is a MultiValueField:
            data[0] : the normal file data
            data[1] : a delete flag to remove a file.
        Otherwise a regular FileField, and data is simply the uploaded file data.
        """
        # Separate out file data from delete flag if available
        if data:
            if type(data) == types.ListType:
                delete_flag = data[1]
                file_data = data = data[0]
            elif isinstance(data, UploadedFile):
                file_data = data
                delete_flag = False
            else:
                file_data = None
                delete_flag = False
        else:
            file_data = None
            delete_flag = False

        # Can't delete the file if field is not null/blank
        if not self.null and not self.blank:
            delete_flag = False

        if file_data: # Replace file
            self._maybe_delete_file(instance)
            super(CustomFileField, self).save_form_data(instance, file_data)
        elif delete_flag: # Delete file
            self._maybe_delete_file(instance)
            setattr(instance, self.name, None)
        else:
            super(CustomFileField, self).save_form_data(instance, data)

    def formfield(self, **kwargs):
        from cms.apps.media.forms.fields import RemovableFileFormField
        if self.blank or self.null:
            defaults = {'form_class': RemovableFileFormField}
        else:
            defaults = {}
        defaults.update(kwargs)
        return super(CustomFileField, self).formfield(**defaults)

    def get_internal_type(self):
        return 'FileField'


class CustomImageField(ImageField, CustomFileField):
    """
    Adds customizations to djangos built-in ImageField

    * Cleans thumbnails after a delete
    """
    thumbnail_glob = '*_[0-9]*x[0-9]*_q*'

    def contribute_to_class(self, cls, name):
        """Hook up events so we can access the instance."""
        super(CustomImageField, self).contribute_to_class(cls, name)
        signals.post_delete.connect(self._post_delete, sender=cls)

    def formfield(self, **kwargs):
        from cms.apps.media.forms.fields import RemovableImageFormField, CustomImageFormField
        if self.blank or self.null:
            defaults = {'form_class': RemovableImageFormField}
        else:
            defaults = {'form_class': CustomImageFormField}
        defaults.update(kwargs)
        return super(CustomImageField, self).formfield(**defaults)

    def get_internal_type(self):
        return 'FileField'

# South Introspection Rules
try:
    from south.modelsinspector import add_introspection_rules
    rules = [
        (
            (CustomFileField, CustomImageField),
            [], {},
        ),
    ]

    add_introspection_rules(rules, ["^.+\.apps\.media\.fields"])

except ImportError:
    pass
