import os
import re
import mimetypes
import urllib2

from django.db import models
from django.template.defaultfilters import title
from django.conf import settings
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.core.files.base import ContentFile
from django.core.exceptions import ValidationError
from django.utils import simplejson as json

from tagging.fields import TagField
from tagging.models import TaggedItem
from cms.apps.media.fields import CustomImageField, CustomFileField
from cms.apps.utils.fields import AutoSlugField, JSONField
from cms.apps.media.fields.related import RelatedMediaField


class MediaItemManager(models.Manager):
    def published(self):
        """
        Returns all public items.
        """
        return self.get_query_set().filter(published=True)


class MediaItem(models.Model):
    """
    A Generic Media item (images, files, videos, etc. should subclass)
    """
    title = models.CharField(max_length=200, blank=True)
    slug = AutoSlugField(max_length=200, unique=True, editable=False,
                    prepopulate_from='title',
                    help_text='Unique text identifier used in urls.')
    published = models.BooleanField(default=True,
                    help_text='Deselect to keep file private.')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    caption = models.TextField(blank=True)

    featured = models.BooleanField(default=False,
            help_text='Make this a featured item on the home page.')

    tags = TagField(help_text='Enter keywords separated by space or comma.')
    related_tags = generic.GenericRelation(TaggedItem)

    objects = MediaItemManager()

    def get_mimetype(self):
        if hasattr(self, 'filename'):
            type = mimetypes.guess_type(self.filename.name)[0]
            if type is None:
                return 'unkown'
            else:
                return type
        else:
            return 'unknown'
    mimetype = property(get_mimetype)

    def get_type(self):
        if hasattr(self, 'video'):
            return 'video'
        elif hasattr(self, 'image'):
            return 'image'
        elif hasattr(self, 'file'):
            return 'file'
        return 'unkown'
    type = property(get_type)

    def __unicode__(self):
        return u'Media Item: %s' % self.title


class File(MediaItem):
    """ A generic file model. """
    filename = CustomFileField(upload_to='files',
            help_text='Select a file to upload.')

    objects = MediaItemManager()

    def save(self, *args, **kwargs):
        # If title isn't set, create one based on filename
        if not self.title:
            self.title = title(os.path.splitext(
                os.path.basename(self.filename.name))[0].replace('_', ' '))
        super(File, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"File: %s (%s)" % (self.title,
                                   os.path.basename(self.filename.name))

    def get_absolute_url(self):
        return '%s%s' % (settings.MEDIA_URL, self.filename.name)


class Image(MediaItem):
    """ An image model. """
    filename = CustomImageField(upload_to='images',
                width_field='width', height_field='height',
                help_text='Select an image file to upload.')
    width = models.PositiveIntegerField(editable=False)
    height = models.PositiveIntegerField(editable=False)

    header = models.BooleanField(default=False,
            help_text='Cycle this image in the top-bar header of the site.')
    work_page = models.BooleanField('Our Work', default=False,
            help_text='Show this image in the "Our Work" section splash page.')

    objects = MediaItemManager()

    def save(self, *args, **kwargs):
        # If title isn't set, create one based on filename
        if not self.title:
            self.title = title(os.path.splitext(
                os.path.basename(self.filename.name))[0].replace('_', ' '))
        super(Image, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"Image: %s (%s)" % (self.title,
                                    os.path.basename(self.filename.name))

    def get_absolute_url(self):
        return '%s%s' % (settings.MEDIA_URL, self.filename.name)


class VideoManager(MediaItemManager):
    def published(self):
        """
        Returns all public videos that have been sucessfully encoded.
        """
        return (self.encoded() | self.vimeo()).filter(published=True)

    def vimeo(self):
        return self.all().exclude(url='')

    def encoded(self):
        return (self.all().exclude(mp4='')
                          .exclude(conversion_error_flag=True))

    def unencoded(self):
        """
        Returns all videos that haven't finished being encoded.
        """
        return (self.all().exclude(filename='')
                          .filter(mp4='', conversion_error_flag=False))

    def errors(self):
        """
        Returns all tracks that have had an error while converting to mp3
        format.
        """
        return self.all().filter(conversion_error_flag=True)


VIMEO_OEMBED_URL = 'https://vimeo.com/api/oembed.json'


class Video(MediaItem):
    # Vimeo
    url = models.URLField(max_length=255, blank=True,
        verify_exists=False,
        help_text=("Enter the URL for a vimeo video "
                   "(e.g. https://vimeo.com/VIDEO_ID/)."))
    thumbnail_url = models.URLField(max_length=255, blank=True,
                                    verify_exists=False, editable=False)
    data = JSONField(blank=True)

    # Legacy video file
    filename = CustomFileField(upload_to='videos',
            blank=True,
            help_text='Select a video file to upload.')
    mp4 = CustomFileField(upload_to='videos/mp4', blank=True,
            help_text=('Mp4 version of video (if left blank, this will '
                       'be generated automatically'))
    image = CustomImageField(upload_to='videos/images', blank=True,
            help_text=('If left blank, this will be generated '
                       'automatically from the video'))
    conversion_error_flag = models.BooleanField(default=False,
            help_text="An error occurred encoding (un-check to try again)",
            verbose_name="Errors")

    objects = VideoManager()

    def __unicode__(self):
        return u"Videos: %s" % self.title

    def clean(self, *args, **kwargs):
        if not self.url and not self.filename:
            raise ValidationError(
                    "Please provide either a 'url' or a 'filename'.")
        if self.url:
            try:
                self.data = self._get_info()
                if 'thumbnail_url' in self.data:
                    self.thumbnail_url = self.data['thumbnail_url']
                    if not self.image:
                        image_file = self._get_vimeo_image()
                        name = os.path.basename(self.thumbnail_url)
                        self.image.save(name=name, content=image_file, save=False)
                if not self.title and 'title' in self.data:
                    self.title = self.data['title']
                if not self.caption and 'description' in self.data:
                    self.caption = self.data['description']
            except urllib2.HTTPError:
                if not self.id:
                    raise
        else:
            self.data = ''
            if not self.title:
                self.title = title(os.path.splitext(
                    os.path.basename(self.filename.name))[0].replace('_', ' '))

        super(Video, self).clean(*args, **kwargs)

    def get_absolute_url(self):
        from django.conf import settings
        return '%s%s' % (settings.MEDIA_URL, self.filename.name)

    # Vimeo
    @staticmethod
    def vimeo_oembed_request(query):
        """
        Makes a request to vimeo.com oEmbed api.
        query: is the query string (e.g. url=https://vimeo.com/203499)
        returns the response object returned from sending the request.
        """
        url = "%s?%s" % (VIMEO_OEMBED_URL, query)
        headers = {'User-Agent': 'Django'}
        request = urllib2.Request(url, headers=headers)
        response = urllib2.urlopen(request)
        return response

    def _get_info(self, **kwargs):
        """
        Uses Vimeo's oEmbed api to fetch information about the video using the
        saved url.  Keyword arguments are used as parameters for the request to
        override defaults.
        """
        params = {
            'url': self.url,
            'portrait': 'false',
            'byline': 'false',
        }
        params.update(kwargs)
        query = "&".join(["%s=%s" % (k, v) for k, v in params.items()])
        response = self.vimeo_oembed_request(query)
        data = json.loads(response.read())
        return data

    @property
    def info(self):
        return self.data

    def _get_vimeo_image(self):
        image_url = self.data['thumbnail_url']
        headers = {'User-Agent': 'Django'}
        request = urllib2.Request(image_url, headers=headers)
        response = urllib2.urlopen(request)
        file_ = ContentFile(response.read())
        return file_

    def get_screenshot(self, size='thumb'):
        url = self.thumbnail_url
        thumbnails = {
            'thumb': re.sub(r'_\d+\.jpg', '_100.jpg', url),
            'small': re.sub(r'_\d+\.jpg', '_640.jpg', url),
            'medium500': re.sub(r'_\d+\.jpg', '_640.jpg', url),
            'medium640': re.sub(r'_\d+\.jpg', '_640.jpg', url),
            'large': url}
        return thumbnails[size]

    thumbnail = property(lambda self: self.get_screenshot('thumb'))
    screenshot = property(lambda self: self.get_screenshot('medium640'))
    large_screen = property(lambda self: self.get_screenshot('large'))

    @property
    def has_vimeo_video(self):
        return bool(self.data)

    # Legacy Video file
    def is_processing(self):
        if self.url:
            return False
        if self.pk and not self.mp4 and not self.conversion_error_flag:
            return True
        return False

    @property
    def aspect_ratio(self):
        if not self.has_vimeo_video:
            return 4 / 3.0
        return float(self.data['width']) / float(self.data['height'])

    @property
    def quality(self):
        if not self.has_vimeo_video:
            return 'default'
        aspect_ratio = self.aspect_ratio
        if aspect_ratio < (16 / 9.0):
            return 'sd'
        return 'hd'

    def get_embed(self, width=None, height=None):
        """
        Returns an embed code for the video
        """
        # HD videos (vimeo)
        if self.has_vimeo_video:
            if not width:
                width = 855
            if not height:
                orig_width = self.data['width']
                orig_height = self.data['height']
                height = int((width / float(orig_width)) * orig_height)
            embed = """<iframe src="%(src)s" width="%(width)s" height="%(height)s" class="vimeo"
            frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>""" % {
                'src': 'https://player.vimeo.com/video/%s?title=0&byline=0&portrait=0' % self.data['video_id'],
                'width': width,
                'height': height,
            }
            #embed = self.data['html']

        # Legacy SD videos
        else:
            if not width:
                width = 360
            if not height:
                height = 292
            if self.mp4 and self.image:
                embed = """<object id="flowplayer" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="%(width)s" height="%(height)s"><param name="movie" value="%(media_url)sflowplayer/flowplayer-3.1.2.swf" /><param name="allowFullScreen" value="true" /><param name="flashvars" value='config={"playlist":[{"url":"%(image)s","autoPlay":true},{"url":"%(mp4)s","autoPlay":false}]}' /><embed type="application/x-shockwave-flash" width="%(width)s" height="%(height)s" src="%(media_url)sflowplayer/flowplayer-3.1.2.swf" allowFullScreen="true" flashvars='config={"playlist":[{"url":"%(image)s","autoPlay":true},{"url":"%(mp4)s","autoPlay":false}]}'/></object>""" % {
                    'media_url': settings.MEDIA_URL,
                    'mp4': self.mp4.url,
                    'image': self.image.url,
                    'width': width,
                    'height': height,
                }
            else:
                embed = ''
        return embed
    embed = property(get_embed)


class MediaRelation(models.Model):
    """
    Abstract class to form generic m2m relations between a MediaItem
    and another model.  MediaItem types should each provide a corresponding
    subclass of MediaRelation so the media type can be easily attached to
    other model types.
    """
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField('object id', db_index=True)
    object = generic.GenericForeignKey('content_type', 'object_id')
    sort = models.PositiveIntegerField(null=True, blank=True, default=0,
                                       verbose_name="sort order")

    class Meta:
        abstract = True
        ordering = ('content_type', 'sort')
        unique_together = ('item', 'object_id', 'content_type')

    def save(self, *args, **kwargs):
        # new items should be added to the end of the list of
        # existing items as ordered by the `sort` field.
        if not self.pk and not self.sort:
            model = type(self)
            max_sort = model.objects.filter(
                    content_type=self.content_type,
                    object_id=self.object_id
                ).aggregate(max_sort=models.Max('sort'))['max_sort']
            if max_sort is not None:
                self.sort = max_sort + 1
        super(MediaRelation, self).save(*args, **kwargs)

    @classmethod
    def media_model(cls):
        return cls._meta.get_field_by_name("item")[0].rel.to

    @classmethod
    def lookup_kwargs(cls, instance):
        return {
            'object_id': instance.pk,
            'content_type': ContentType.objects.get_for_model(instance),
        }

    @classmethod
    def bulk_lookup_kwargs(cls, instances):
        return {
            "object_id__in": [instance.pk for instance in instances],
            "content_type": ContentType.objects.get_for_model(instances[0]),
        }

    @classmethod
    def media_relname(cls):
        return cls._meta.get_field_by_name('item')[0].rel.related_name

    @classmethod
    def media_for(cls, model, instance, **defaults):
        ctype = ContentType.objects.get_for_model(model)
        kwargs = {
            "%s__content_type" % cls.media_relname(): ctype
        }
        if instance is not None:
            kwargs["%s__object_id" % cls.media_relname()] = instance.pk
        kwargs.update(defaults)
        qs = cls.media_model().objects.filter(**kwargs).distinct()
        return qs.order_by("%s__sort" % cls.media_relname())

    @classmethod
    def get_related_models(cls):
        registered_models = getattr(cls._meta, '_media_registry', [])
        child_models = [x for y in registered_models
                            for x in y.__subclasses__()]
        return registered_models + child_models

    @classmethod
    def relates_to(cls, model):
        return model in cls.get_related_models()


class ImageRelation(MediaRelation):
    item = models.ForeignKey(Image, related_name='relations')


class RelatedImagesField(RelatedMediaField):
    def __init__(self, verbose_name="Images"):
        through = ImageRelation
        super(RelatedImagesField, self).__init__(verbose_name, through)


class FileRelation(MediaRelation):
    item = models.ForeignKey(File, related_name='relations')


class RelatedFilesField(RelatedMediaField):
    def __init__(self, verbose_name="Files"):
        through = FileRelation
        super(RelatedFilesField, self).__init__(verbose_name, through)


class VideoRelation(MediaRelation):
    item = models.ForeignKey(Video, related_name='relations')


class RelatedVideosField(RelatedMediaField):
    def __init__(self, verbose_name="Videos"):
        through = VideoRelation
        super(RelatedVideosField, self).__init__(verbose_name, through)


# South Introspection Rules
try:
    from south.modelsinspector import add_ignored_fields
    add_ignored_fields(["^.*apps\.media\.models"])
except ImportError:
    pass
