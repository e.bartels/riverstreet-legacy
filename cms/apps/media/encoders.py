"""
Handles processing of videos into flv flash video format.
"""
import os
import re
import math
import tempfile
import shutil
import logging
import subprocess
import itertools

from django.conf import settings
from django.utils.datastructures import SortedDict

FFMPEG_PATH = getattr(settings, 'FFMPEG_PATH', '/usr/bin/ffmpeg')
QTFASTSTART_PATH = getattr(settings, 'QTFASTSTART_PATH',
                           '/usr/bin/qt-faststart')


log = logging.getLogger(__name__)


class EncodeError(Exception):
    pass


class BaseEncoder(object):
    """
    Subclass for all encoder classes
    Subclasses should override `encode_options` and `encode` method.
    """
    encode_options = ()

    def __init__(self, options=None):
        self.encode_options = SortedDict(self.encode_options)
        if options:
            self.encode_options.update(options)

    def encode(self, source, target, options=None):
        """
        Encoder should accept a source video filename and a target filename.

        If there is an `options` paramter then those will be used for encoding,
        otherwise, self.encode_options should be used.
        """
        raise NotImplementedError(
                "Custom encoder sublcasses must implement this method.")


class FFMPEGInfo(object):
    """
    Gathers metadata info about a video by parsing ffmpeg's output
    """
    encoder = FFMPEG_PATH

    @classmethod
    def _get_video_info(cls, output):
        data = {}

        # Get video data
        m = re.search(r'Stream.+:\s+(Video):(.+)', output)
        if not m:
            raise EncodeError("Could not get video information: %s" % output)

        stream_data = [p.strip() for p in m.group(2).split(',')]
        stream_info = ','.join(stream_data)

        # Get format
        data['format'] = stream_data[0]

        # Get size
        size_m = re.search(r',(\d+x\d+)[^,]*,', stream_info)
        if size_m:
            data['size'] = size_m.group(1)
        else:
            data['size'] = None

        # Get frame rate
        fr_m = re.search(r'(\d+) fps,', stream_info)
        if fr_m:
            data['fps'] = int(fr_m.group(1))

        return data

    @classmethod
    def _get_audio_info(cls, output):
        data = {}

        # Get audio data
        m = re.search(r'Stream.+:\s+(Audio):(.+)', output)
        if not m:
            raise EncodeError("Could not read audio info: %s" % output)
        stream_data = [p.strip() for p in m.group(2).split(',')]
        stream_info = ','.join(stream_data)

        # Get format
        data['format'] = stream_data[0]

        # Get frame rate
        r_m = re.search(r'(\d+) Hz,', stream_info)
        if r_m:
            data['samplerate'] = int(r_m.group(1))

        return data

    @classmethod
    def get_info(cls, source, ffmpeg=None):
        if ffmpeg:
            encoder = ffmpeg
        else:
            encoder = cls.encoder

        if not os.path.exists(source):
            raise IOError("No such file or directory: %s" % source)
        if not os.path.isfile(source):
            raise EncodeError("Source file is a directory: %s" % source)

        command = subprocess.Popen((encoder, '-i', source),
                stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
        output, stderr = command.communicate()

        # Check that we can read the file
        m = re.search(r'Invalid data found', output)
        if m:
            message = "\n".join([
                "ffmpeg could not read this file: %s" % source,
                output,
            ])
            raise EncodeError(message)

        data = {
            'video': cls._get_video_info(output),
            'audio': cls._get_audio_info(output),
        }

        # Get duration
        m = re.search(r'Duration:\s(\d+:\d+:\d+)', output)
        if not m:
            message = "\n".join([
                "ffmpeg could not read the duration for: %s" % source,
                output,
            ])
            raise EncodeError(message)

        duration_parts = m.group(1).split(':')
        duration_parts.reverse()
        seconds = 0
        for exp, p in enumerate(duration_parts):
            d = int(p) * (60 ** exp)
            seconds += d

        data['duration'] = seconds
        return data


class FFMPEGEncoder(BaseEncoder):
    """
    An encoder that uses ffmpeg externally to do the encoding.

    The ffmpeg program must be installed in order to use this encoder.  If your
    ffmpeg executable is not in the sys path then you should pass the full path
    to the executable to init

    Example:
    encoder = FFMPEGEncoder(ffmpeg='/usr/local/bin/ffmpeg')
    """
    encoder = FFMPEG_PATH

    def __init__(self, ffmpeg=None, options=None):
        if ffmpeg:
            self.encoder = ffmpeg
        super(FFMPEGEncoder, self).__init__(options)
        #self.encode_options.insert(0, 'v', 0)

    def get_media_info(self, source):
        return FFMPEGInfo.get_info(source)

    def get_encode_options(self, source, target):
        options = self.encode_options
        media_info = self.get_media_info(source)

        # Determine width/height
        video_size = media_info.get('video').get('size')
        requested_size = options.get('s')
        if requested_size and video_size:
            target_size = self._determine_target_size(video_size,
                                                      requested_size)
            options['s'] = '%dx%d' % target_size

        return options

    def encode(self, source, target):
        if not os.path.exists(source):
            raise IOError("No such file or directory: %s" % source)
        if not os.path.isfile(source):
            raise EncodeError("Source file is a directory: %s" % source)

        if os.path.exists(target) and os.path.isdir(target):
            raise EncodeError("Target file is a directory: %s" % target)

        options = self.get_encode_options(source, target)

        # Run the ffmpeg command using subprocess
        command_args = self._get_command_args(source, target, options)
        log.debug(' '.join(command_args))

        command = subprocess.Popen(command_args,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                close_fds=True)
        stdout, stderr = command.communicate()

        if command.returncode != 0:
            message = "\n".join([
                "Encountered an error encoding file: '%s'",
                "command: %s",
                "%s",
            ])
            message = message % (source, ' '.join(command_args), stderr)
            log.error(message)
            raise EncodeError(message)

    def _determine_target_size(self, video_size, requested_size):
        """
        Figure the target video size based on actual size & requested size
        """
        width, height = [int(x) for x in video_size.split('x')]
        r_width, r_height = [int(x) for x in requested_size.split('x')]
        if width > r_width:
            height = (float(r_width) / width) * height
            width = r_width
        if height > r_height:
            width = (float(r_height) / height) * width
            height = r_height

        width = math.floor(width / 2) * 2
        height = math.floor(height / 2) * 2
        return (width, height)

    def _get_command_args(self, source, target, options):
        """
        Returns a command string to be used to run ffmpeg with the encoder's
        options
        """
        # Build options list
        args = [('-%s' % k, (str(v) if v is not None else ''))
                for k, v in options.items()]
        # Flatten list
        args = list(itertools.chain(*args))
        # Remove empty strings
        args = [arg for arg in args if arg is not '']
        #args = [str(arg) for arg in
        #         list(itertools.chain(*options.items()))if arg is not None]
        args = [self.encoder, '-i', source] + args + [target]
        return args


class H264Encoder(FFMPEGEncoder):
    """
    Encodes an h264 video w/ AAC audio.
    """
    encode_options = (
        ('y', None),            # overwrite target file
        ('f', 'mp4'),           # container format
        # new
        ('strict', 'experimental'),

        # Audio options

        # New
        #('acodec', 'aac'),      # use aac audio codec
        #('ar', 44100),          # audio sample rate
        #('ac', 2),              # audio channels
        #('aq', 100),            # audio quality (vbr)

        # Old
        ('acodec', 'libfaac'),  # use aac audio codec
        ('ab', '96k'),          # audio bitrate (cbr)
        ('ar', 22500),          # audio sample rate


        # Video options
        ('vcodec', 'libx264'),  # h264 video codec
        ('s', '320x240'),       # size

        # New
        #('b:v', '512k'),         # Choose bitrate, or use crf below
        #('threads', '0'),       # let ffmpeg decide
        #('preset', 'medium'),       # x264 preset (see x264 --fullhelp)
        #('level', '3.0'),           # h264 level

        # Old
        ('b', '512k'),         # Choose bitrate, or use crf below
        ('crf', 20),            # Constant rate factor
        ('coder', 1),
        ('flags', '+loop'),
        ('cmp', '+chroma'),
        ('partitions', '+parti8x8+parti4x4+partp8x8+partb8x8'),
        ('me_method', 'umh'),
        ('subq', '8'),
        ('me_range', '16'),
        ('g', '250'),
        ('keyint_min', '25'),
        ('sc_threshold', '40'),
        ('i_qfactor', '0.71'),
        ('b_strategy', '2'),
        ('qcomp', '0.6'),
        ('qmin', '10'),
        ('qmax', '51'),
        ('qdiff', '4'),
        ('bf', '4'),
        ('refs', '4'),
        ('directpred', '3'),
        ('trellis', '1'),
        ('flags2', '+bpyramid+wpred+mixed_refs+dct8x8+fastpskip'),
    )

    def get_encode_options(self, source, target):
        options = self.encode_options

        # For AAC audio @ 44100 lets just copy the stream.
        media_info = self.get_media_info(source)
        audio_format = media_info.get('audio').get('format')
        samplerate = media_info.get('audio').get('samplerate')
        if audio_format.startswith('aac') and samplerate == 44100:
            options['acodec'] = 'copy'
            for param in ('ab', 'ar', 'ac', 'aq'):
                if param in options:
                    del options[param]

        self.encode_options = options
        return super(H264Encoder, self).get_encode_options(source, target)


class FastStartEncoder(BaseEncoder):
    """
    Takes an h264 file and rearranges the moov atoms so fast start
    pseudo-streaming will work correctly.

    Makes a call to the qt-faststart program, which is distributed with ffmpeg.
    """
    encoder = QTFASTSTART_PATH

    def __init__(self, qt_faststart=None):
        if qt_faststart:
            self.encoder = qt_faststart

    def encode(self, source, target=None, options=None):
        if not os.path.exists(source):
            raise IOError("No such file or directory: %s" % source)
        if not os.path.isfile(source):
            raise EncodeError("Source file is a directory: %s" % source)

        if target and os.path.exists(target) and os.path.isdir(target):
            raise EncodeError("Target file is a directory: %s" % target)

        # Make a temp file if the source & target are the same
        if target is None or source == target:
            tmp, outfile = tempfile.mkstemp()
            os.close(tmp)
        else:
            outfile = target

        self.fast_start(source, outfile)

        if target is None or source == target:
            shutil.move(outfile, source)

    def fast_start(self, source, target):
        command = subprocess.Popen((self.encoder, source, target),
                stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        output, stderr = command.communicate()
        if command.returncode != 0:
            message = "\n".join([
                "Encountered an error encoding file: '%s'",
                "command: %s",
                "%s",
            ])
            message = message % (source, stderr)
            raise EncodeError(message)


class PNGEncoder(FFMPEGEncoder):
    """
    Creates a PNG image from video file.
    """
    encode_options = (
        ('y', None),
        ('vframes', '1'),
        ('ss', '00:00:05'),
        ('an', None),
        ('vcodec', 'png'),
        ('f', 'rawvideo'),
        ('s', '320x240'),
    )
