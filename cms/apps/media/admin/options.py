import urllib2

from django.contrib import admin
from django import forms
from django.template import Template, Context
from django.contrib.admin.widgets import AdminFileWidget

from cms.apps.media.models import Image, File, Video
from cms.apps.utils.forms.widgets import MDTextarea
from cms.apps.media.templatetags.media import get_mime_image
from cms.apps.media.forms.fields import (RemovableFileFormField,
                                         RemovableImageFormField)


class ImageAdmin(admin.ModelAdmin):
    model = Image
    list_display = ('image_column', 'title', 'created', 'modified',
            'published', 'featured', )
    list_filter = ('published', 'featured', 'header', 'work_page')
    search_fields = ('title', 'caption', 'tags')
    ordering = ('-modified',)
    fieldsets = [
        (None, {
            'fields': ('filename', 'title', 'featured', 'header', 'work_page',
                       'caption',)
        }),
        ('Tags', {
            'fields': ('tags',),
            'classes': ('collapse',)
        }),
    ]

    def image_column(self, obj):
        t = Template("""
                {% load thumbnail %}
                <img src="{% thumbnail obj.filename 100x100 %}"
                     alt="{{obj.filename}}"/>""")
        return t.render(Context({'obj': obj}))
    image_column.allow_tags = True
    image_column.short_description = 'Image'
    image_column.admin_order_field = 'filename'

    def formfield_for_dbfield(self, db_field, **kwargs):
        # Make content field an mceEditor instance
        if db_field.name == 'caption':
            kwargs.pop('request')
            kwargs['widget'] = MDTextarea(attrs={
                'markdownSet': 'markdownSimple',
                'rows': '10',
            })
            return db_field.formfield(**kwargs)

        if db_field.name == 'title' or db_field.name == 'tags':
            kwargs.pop('request')
            kwargs['widget'] = forms.widgets.TextInput(attrs={
                'size': '60',
            })
            return db_field.formfield(**kwargs)

        if db_field.name == 'filename':
            kwargs.pop('request')
            kwargs['widget'] = AdminFileWidget
            return db_field.formfield(**kwargs)

        return super(ImageAdmin, self).formfield_for_dbfield(db_field,
                                                             **kwargs)

    def lookup_allowed(self, key, value):
        if key in ('relations__content_type__model', 'relations__object_id'):
            return True
        return super(ImageAdmin, self).lookup_allowed(key, value)


class VideoAdminForm(forms.ModelForm):
    class Meta:
        model = Video

    def clean_url(self):
        url = self.cleaned_data.get('url', None)
        if url:
            try:
                Video.vimeo_oembed_request('url=%s' % url)
            except urllib2.HTTPError:
                if not self.instance.id:
                    raise forms.ValidationError((
                        'Could not retrieve data on this video. '
                        'It is possible that the video is not public, or '
                        'needs permissions to be set to allow embedding.'))
        return url


class VideoAdmin(admin.ModelAdmin):
    model = Video
    list_display = ('image_column', 'title', 'created', 'modified',
                    'published', 'featured', 'encoded_column', 'has_vimeo_col')
    list_filter = ('published', 'featured', 'conversion_error_flag')
    search_fields = ('title', 'caption', 'tags')
    ordering = ('-modified',)
    form = VideoAdminForm
    fieldsets = [
        ('Vimeo', {
            'fields': ('url',)
        }),
        ('Video File', {
            'fields': ('filename', 'mp4', 'image', 'conversion_error_flag')
        }),
        (None, {
            'fields': ('title', 'featured', 'caption',)
        }),
        #('Tags', {'fields': ('tags',), 'classes': ('collapse',)}),
    ]

    def image_column(self, obj):
        from sorl.thumbnail.main import DjangoThumbnail
        from sorl.thumbnail.base import ThumbnailException
        try:
            thumb = DjangoThumbnail(obj.image.name, (100, 100))
            thumb_url = thumb.absolute_url
            t = '<img src="%s" alt="%s"/>' % (thumb_url, obj.title)
        except ThumbnailException:
            thumb_url = get_mime_image(unicode(obj.filename), size=32)
            t = '<img src="%s" alt="%s"/>' % (thumb_url, obj.title)
        return t
    image_column.allow_tags = True
    image_column.short_description = 'Video'
    image_column.admin_order_field = 'filename'

    def conversion_error_column(self, obj):
        """
        Shows whether the track has had an error encoding a compressed file
        version.
        """
        return not obj.conversion_error_flag
    conversion_error_column.short_description = 'Encode Errors'
    conversion_error_column.boolean = True

    def encoded_column(self, obj):
        return bool(obj.mp4)
    encoded_column.short_description = 'Encoded'
    encoded_column.boolean = True
    encoded_column.admin_order_field = 'mp4'

    def has_vimeo_col(self, obj):
        if obj.url:
            return True
        return False
    has_vimeo_col.short_description = 'Has Vimeo?'
    has_vimeo_col.boolean = True
    has_vimeo_col.admin_order_field = 'url'

    def formfield_for_dbfield(self, db_field, **kwargs):
        # Make content field an mceEditor instance
        if db_field.name == 'caption':
            kwargs.pop('request')
            kwargs['widget'] = MDTextarea(attrs={
                'markdownSet': 'markdownSimple',
                'rows': '10',
            })
            return db_field.formfield(**kwargs)

        if db_field.name == 'title' or db_field.name == 'tags':
            kwargs.pop('request')
            kwargs['widget'] = forms.widgets.TextInput(attrs={'size': '60'})
            return db_field.formfield(**kwargs)

        if db_field.name == 'filename':
            kwargs.pop('request')
            kwargs['widget'] = AdminFileWidget
            return db_field.formfield(**kwargs)

        if db_field.name == 'mp4':
            return RemovableFileFormField(required=False,
                                          help_text=db_field.help_text)

        if db_field.name == 'image':
            return RemovableImageFormField(required=False,
                                           help_text=db_field.help_text)

        return super(VideoAdmin, self).formfield_for_dbfield(db_field,
                                                             **kwargs)


class FileAdmin(admin.ModelAdmin):
    model = File
    list_display = ('image_column', '__unicode__', 'title', 'created',
                    'modified', 'published',)
    list_filter = ('published', )
    search_fields = ('filename', 'title', 'caption')
    ordering = ('-modified',)
    fieldsets = [
        (None, {'fields': ('filename', 'title', 'caption',)}),
    ]

    def image_column(self, obj):
        from sorl.thumbnail.main import DjangoThumbnail
        from sorl.thumbnail.base import ThumbnailException
        try:
            thumb = DjangoThumbnail(obj.filename.name, (100, 100))
            thumb_url = thumb.absolute_url
        except ThumbnailException:
            thumb_url = get_mime_image(unicode(obj.filename), size=32)
        t = '<img src="%s" alt="%s"/>' % (thumb_url, obj.title)
        return t
    image_column.allow_tags = True
    image_column.short_description = 'Image'
    image_column.admin_order_field = 'filename'

    def formfield_for_dbfield(self, db_field, **kwargs):
        # Make content field an mceEditor instance
        if db_field.name == 'caption':
            kwargs.pop('request')
            kwargs['widget'] = MDTextarea(attrs={
                'markdownSet': 'markdownSimple',
                'rows': '10',
            })
            return db_field.formfield(**kwargs)
        if db_field.name == 'title' or db_field.name == 'tags':
            kwargs.pop('request')
            kwargs['widget'] = forms.widgets.TextInput(attrs={'size': '60'})
            return db_field.formfield(**kwargs)
        return super(FileAdmin, self).formfield_for_dbfield(db_field, **kwargs)


admin.site.register(Image, ImageAdmin)
admin.site.register(File, FileAdmin)
admin.site.register(Video, VideoAdmin)
