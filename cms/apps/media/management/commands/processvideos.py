import os
import sys
import commands
from optparse import make_option

from django.conf import settings
from django.core.management import BaseCommand

from cms.apps.media.models import Video
from cms.apps.media.encoders import (H264Encoder, FastStartEncoder,
        PNGEncoder, EncodeError)


class ProcessNotFoundException(Exception):
    pass


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--pidfile', dest='pidfile', default='',
            help='A pidfile path. (e.g. /tmp/processvideos.pid)  Only one instance of this command will run at a time.'),

        make_option('--force', action='store_true', dest='force', default=False,
            help='Attempt encoding for videos that have failed previously.'),

        make_option('--stop_on_error', action='store_true', dest='stop_on_error', default=False,
            help='If an error in encoding is encountered, give up and display the error message.')
    )
    help = "Transcodes videos & generates images from video instances."

    def process_videos(self, force=False, stop_on_error=False):
        # Add video screencap images.
        generate_image_for_videos(force=force)

        # Transcode videos
        transcode_new_videos(force=force, stop_on_error=stop_on_error)

    def handle(self, *args, **options):
        force = options.get('force')
        stop_on_error = options.get('stop_on_error')

        pidfile = options.get('pidfile')
        if pidfile:
            try:
                f = open(pidfile, 'r')
                pid = f.readline().strip()
                cmd = "ps -p %s --no-heading" % pid
                (status, output) = commands.getstatusoutput(cmd)
                if status != 0: # process not found
                    f.close()
                    raise ProcessNotFoundException
                else: # another process is running, so exit
                    sys.exit()
            except (ProcessNotFoundException, IOError):
                # no pid file or process not found
                pid = os.getpid()
                f = open(pidfile, 'w')
                f.write(str(pid))
                f.close()
                self.process_videos(force=force, stop_on_error=stop_on_error)
                os.remove(pidfile)
        else:
            self.process_videos(force=force, stop_on_error=stop_on_error)


def process_mp4(video, stop_on_error=False):
    """
    Encodes and saves a h264 mp4 version from the source video
    """
    source_field = video.filename
    mp4_field = video.mp4

    source_path = source_field.path
    (source_name, ext) = os.path.splitext(os.path.basename(source_path))
    mp4_path = os.path.join(settings.MEDIA_ROOT,
                            mp4_field.field.upload_to, "%s.mp4" % source_name)

    if not os.path.exists(os.path.dirname(mp4_path)):
        os.makedirs(os.path.dirname(mp4_path))

    try:
        encoder = H264Encoder()
        encoder.encode(source_path, mp4_path)
        fs_encoder = FastStartEncoder()
        fs_encoder.encode(mp4_path)
        video.mp4 = '%s/%s.mp4' % (mp4_field.field.upload_to, source_name)
        video.conversion_error_flag = False
    except EncodeError:
        if stop_on_error:
            raise
        video.conversion_error_flag = True

    video.save()


def process_image(video):
    """
    Creates an saves a still image frame from the source video.
    """
    source_field = video.filename
    image_field = video.image

    source_path = source_field.path
    (source_name, ext) = os.path.splitext(os.path.basename(source_path))
    image_path = os.path.join(
                    settings.MEDIA_ROOT,
                    image_field.field.upload_to, "%s.png" % source_name)

    if not os.path.exists(os.path.dirname(image_path)):
        os.makedirs(os.path.dirname(image_path))

    try:
        encoder = PNGEncoder()
        encoder.encode(source_path, image_path)
        video.image = '%s/%s.png' % (image_field.field.upload_to, source_name)
        video.conversion_error_flag = False
    except EncodeError:
        raise
        pass

    video.save()


def generate_image_for_videos(force=False):
    # Create still images from source video
    for video in Video.objects.filter(image=''):
        process_image(video)


def transcode_new_videos(force=False, stop_on_error=False):
    """
    Finds all videos that have not been encoded to mp3 and processes them.
    """

    # Encode h264 mp4 versions from source video
    videos = Video.objects.unencoded().order_by('created')
    if force:
        videos = videos | Video.objects.errors()
    for video in videos:
        process_mp4(video, stop_on_error=stop_on_error)
