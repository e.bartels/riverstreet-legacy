from django import template

from cms.apps.thumbdrive.models import ThumbDrive

register = template.Library()

def thumbdrive_tracker(request):
    """
    Inserts google analytics custom vars for tracking thumbdrive requests.
    """
    thumb_uuid = request.session.get('thumbdrive_uuid')
    if thumb_uuid:
        try:
            thumbdrive = ThumbDrive.objects.get(uuid=thumb_uuid)
        except ThumbDrive.DoesNotExist:
            return ''

        out = """
     // Tracking thumbdrives in analytics.
    _gaq.push(['_setCustomVar',
        1,             // This custom var is set to slot #1.  Required
        'Thumbdrive',   // The name of the custom variable.  Required
        'Yes',      // Sets the value of the variable. Required
        2             // Sets the scope to session-level.  Optional
    ]);
    _gaq.push(['_setCustomVar',
        2,
        'Thumbdrive Name',
        '%(name)s',
        2
    ]);
        """ % {
            'name': thumbdrive.name
        }
        return out

    return ''

register.simple_tag(thumbdrive_tracker)
