from django.db import models

from cms.apps.utils.fields import UUIDB64Field


class ThumbDrive(models.Model):
    """
    So Riverstreet can make thumbdrives with a redirect html file, which will
    show the URL of their choosing.
    """
    name = models.CharField(max_length=255,
            help_text="Descriptive name. This will be used in Analytics to track visits.")
    uuid = UUIDB64Field()
    url = models.URLField(verify_exists=False,
            help_text="The full URL for the page you would like to show. ( e.g. http://riverstreet.net/ )")
    filename = models.CharField(max_length=200, default='riverstreet.html', editable=False,
            help_text='The name given to the file for download.')
    created = models.DateTimeField(auto_now_add=True)
    notes = models.TextField(blank=True, help_text='For internal use.')

    class Meta:
        ordering = ('-created',)
