from django.contrib import admin
from django.core.urlresolvers import reverse
from django import http
from django.template.loader import render_to_string

from cms.apps.thumbdrive.models import ThumbDrive


class ThumbDriveAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'created', 'notes_col', 'download_col')
    search_fields = ('name', 'url', 'notes', 'filename',)

    def notes_col(self, obj):
        return obj.notes[:20]
    notes_col.short_description = 'Notes'

    def download_col(self, obj):
        href = reverse('admin:thumbdrive_thumbdrive_download', args=[obj.pk])
        return '<a href="%s">Download</a>' % href
    download_col.short_description = ''
    download_col.allow_tags = True

    def get_urls(self):
        from django.conf.urls.defaults import patterns, url
        urls = super(ThumbDriveAdmin, self).get_urls()
        info = self.model._meta.app_label, self.model._meta.module_name

        myurls = patterns('',
            url(r'^(?P<id>\d+)/download/$',
                self.admin_site.admin_view(self.download_view),
                name='%s_%s_download' % info),
        )
        return myurls + urls

    def download_view(self, request, id):
        try:
            instance = ThumbDrive.objects.get(pk=id)
        except ThumbDrive.DoesNotExist:
            raise http.Http404

        s = render_to_string('thumbdrive/redirect.html', {
            'thumbdrive': instance,
        })

        resp = http.HttpResponse(mimetype='text/html')
        resp['Content-Disposition'] = 'attachment; filename="%s"' % instance.filename
        resp.write(s)
        return resp


admin.site.register(ThumbDrive, ThumbDriveAdmin)
