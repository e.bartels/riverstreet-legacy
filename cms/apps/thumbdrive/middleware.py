from cms.apps.thumbdrive.models import ThumbDrive


class ThumbdriveMiddleware(object):
    """
    Tracks user requests that have come from a thumbdrive.

    Looks for a 'thumb' query var which should contain the uuid of a thumb
    drive if we find one, then we set a session var so we can access it latter
    in a templatetag.
    """
    def process_request(self, request):
        thumb_uuid = request.GET.get('thumb')
        request.session['thumbdrive_uuid'] = thumb_uuid
        return None
