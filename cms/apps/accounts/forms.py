from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm

from cms.apps.accounts.models import ClientFile, ClientFileNote

class ClientFileForm(forms.ModelForm):
    notes = forms.CharField(label='Notes', required=False, widget=forms.Textarea)

    class Meta:
        model = ClientFile
        fields = ['file', 'title', 'sort', 'notes', ]

    def save(self, belongs_to, uploaded_by, commit=True):
        file = super(ClientFileForm, self).save(commit=False)
        file.belongs_to = belongs_to
        file.uploaded_by = uploaded_by
        file.save()
        
        if self.cleaned_data['notes']:
            note = ClientFileNote(
                    file_id=file.id, 
                    note=self.cleaned_data['notes'],
                    user_id=uploaded_by.id
                )
            note.save()
        
        return file
            

class ClientChangeForm(forms.ModelForm):
    #username = forms.RegexField(label="Username", max_length=30, regex=r'^\w+$',
    #    help_text = "Required. 30 characters or fewer. Alphanumeric characters only (letters, digits and underscores).",
    #    error_message = "This value must contain only letters, numbers and underscores.")
    password1 = forms.CharField(label="Password", required=False, widget=forms.PasswordInput)
    password2 = forms.CharField(label="Password confirmation", required=False, widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['email', 'password1', 'password2',] 

    def clean_username(self):
        username = self.cleaned_data["username"]

        users = User.objects.filter(username=username).exclude(pk=self.instance.pk)
        if users:
            raise forms.ValidationError("A user with that username already exists.")

        return username

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1", "")
        password2 = self.cleaned_data["password2"]
        if password1 != password2:
            raise forms.ValidationError("The two password fields didn't match.")
        return password2

    def save(self, commit=True):
        user = super(ClientChangeForm, self).save(commit=False)
        
        password = self.cleaned_data["password1"]
        if password:
            user.set_password(password)
        if commit:
            user.save()
        return user
