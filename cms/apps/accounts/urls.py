from django.conf.urls.defaults import *

urlpatterns = patterns('', 
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'clients/login.html'}, name="login"),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login', name="logout"),

) + patterns('cms.apps.accounts.views',
    url(r'^(?P<id>\d+)/$', 'client_profile', name="clients-profile"),
    url(r'^(?P<id>\d+)/profile/$', 'client_change_profile', name="clients-change_profile"),
    url(r'^(?P<id>\d+)/upload/$', 'client_upload', name="clients-upload"),
    url(r'^(?P<id>\d+)/edit/(?P<fid>\d+)/$', 'client_edit_file', name="clients-edit_file"),
    url(r'^(?P<id>\d+)/delete/(?P<fid>\d+)/$', 'client_delete_file', name="clients-delete_file"),
    url(r'^(?P<id>\d+)/download/(?P<fid>\d+)/$', 'client_download_file', name="clients-download_file"),
    url(r'^$', 'client_index', name="clients-index"),
)

