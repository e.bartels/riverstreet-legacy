from django.contrib import admin
from django import forms
from django.utils.translation import ugettext as _
from django.utils.encoding import force_unicode
from django.http import HttpResponseRedirect
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.auth.models import User, Group

from cms.apps.accounts.models import UserProfile, ClientFile, ClientFileNote
from cms.apps.media.fields import CustomImageField
from cms.apps.media.forms.widgets import RemovableImageWidget


class UserProfileForm(forms.ModelForm):
    first_name = forms.CharField(max_length=30, required=False)
    last_name = forms.CharField(max_length=30, required=False)
    email = forms.EmailField('e-mail address', required=False)

    class Meta:
        model = UserProfile

    def __init__(self, *args, **kwargs):
        instance = kwargs.get('instance')
        if instance:
            user = instance.user
            data = {
                'initial': {
                    'first_name': user.first_name,
                    'last_name': user.last_name,
                    'email': user.email,
                    }
            }
            kwargs.update(data)
        return super(UserProfileForm, self).__init__(*args, **kwargs)

    def save(self, *args, **kwargs):
        profile = super(UserProfileForm, self).save(*args, **kwargs)
        user = profile.user
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']
        user.save()
        return profile
   

class UserProfileAdmin(admin.ModelAdmin):
    fields = ('first_name', 'last_name', 'email', 'image')
    form = UserProfileForm
    formfield_overrides = {
        CustomImageField: {'widget': RemovableImageWidget}
    }
    
    def has_change_permission(self, request, obj=None):
        """
        Override to allow a user changing their own profile.
        """
        if request.user.userprofile == obj:
            return True
        return super(UserProfileAdmin, self).has_change_permission(request, obj)

    def response_change(self, request, obj):
        """
        Determines the HttpResponse for the change_view stage.
        """
        opts = obj._meta
        pk_value = obj._get_pk_val()

        msg = _('The %(name)s "%(obj)s" was changed successfully.') % {'name': force_unicode(opts.verbose_name), 'obj': force_unicode(obj)}
        if request.POST.has_key("_saveasnew"):
            msg = _('The %(name)s "%(obj)s" was added successfully. You may edit it again below.') % {'name': force_unicode(opts.verbose_name), 'obj': obj}
            self.message_user(request, msg)
            return HttpResponseRedirect("../%s/" % pk_value)
        elif request.POST.has_key("_addanother"):
            self.message_user(request, msg + ' ' + (_("You may add another %s below.") % force_unicode(opts.verbose_name)))
            return HttpResponseRedirect("../add/")
        else:
            self.message_user(request, msg + ' ' + _("You may edit it again below."))
            if request.REQUEST.has_key('_popup'):
                return HttpResponseRedirect(request.path + "?_popup=1")
            else:
                return HttpResponseRedirect(request.path)


admin.site.register(UserProfile, UserProfileAdmin)

# Customize auth admin. 
admin.site.unregister(User)
admin.site.unregister(Group)

class ClientFileAdminForm(forms.ModelForm):
    notes = forms.CharField(label='Notes', required=False, widget=forms.Textarea)
    
    class Meta:
        model = ClientFile

    def save(self, commit=True):
        file = super(ClientFileAdminForm, self).save(commit)

        if self.cleaned_data['notes']:
            note = ClientFileNote(
                    file_id=file.id, 
                    note=self.cleaned_data['notes'],
                    user_id=file.uploaded_by.id
                )
            note.save()
        
        return file


class ClientFileInline(admin.StackedInline):
    model = ClientFile
    fk_name = 'belongs_to'
    extra=1
    form = ClientFileAdminForm

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        # Override uploaded_by field. If not a superuser, then only allow that user
        if db_field.name == 'uploaded_by':
            if request:
                if not request.user.is_superuser:
                    kwargs['queryset'] = User.objects.filter(pk=request.user.pk)
                kwargs['empty_label'] = None
                kwargs['initial'] = request.user.pk
        return super(ClientFileInline, self).formfield_for_foreignkey(db_field, request, **kwargs)



class CustomUserAdmin(UserAdmin):
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'group_list')
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_staff', 'is_active', 'is_superuser', 'user_permissions'), 'classes': ('collapse-closed',)}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined'), 'classes': ('collapse-closed',)}),
        (_('Groups'), {'fields': ('groups',)}),
    )
    inlines = (ClientFileInline,)

    def group_list(self, object): 
        return ", ".join([g.name for g in object.groups.all()])
    group_list.short_description = 'Groups'

    def changelist_view(self, request, extra_context=None):
        # Override to always show either staff or non-staff users
        if 'is_staff__exact' not in request.GET:
            return HttpResponseRedirect('?is_staff__exact=1')
        else:
            try:
                extra_context = {
                    'is_staff': int(request.GET['is_staff__exact'])
                }
            except ValueError:
                pass
        return super(CustomUserAdmin, self).changelist_view(request, extra_context)

admin.site.register(User, CustomUserAdmin)
admin.site.register(Group, GroupAdmin)
