from django import template
from django.contrib.auth.models import User, Group

register = template.Library()

class ClientsNode(template.Node):
    def __init__(self, varname):
        self.varname = varname
        try:
            group = Group.objects.get(name='Clients')
            self.clients = group.user_set.all()
        except Group.DoesNotExist:
            self.clients = None
    
    def render(self, context):
        context[self.varname] = self.clients
        return ''
 
def clients(parser, token):
    bits = token.contents.split()
    if len(bits) != 3:
        raise TemplateSyntaxError, "get_latest tag takes exactly four arguments"
    if bits[1] != 'as':
        raise TemplateSyntaxError, "third argument to get_latest tag must be 'as'"
    return ClientsNode(bits[2])
    
clients = register.tag(clients)

