import mimetypes
import os

from django.db import models
from django import template
from django.contrib.auth.models import User
from django.db.models import signals
from django.template.defaultfilters import title
from django.conf import settings

from cms.apps.media.fields import CustomImageField, CustomFileField
from cms.apps.utils.fields import AutoSlugField
from cms.apps import  menus
from cms.apps.media.encoders import PNGEncoder, EncodeError
from cms.apps.media.encoders import FFMPEG_PATH

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    image = CustomImageField(upload_to='images/users', blank=True,
            help_text="max dimensions: 60x70") 

    def __unicode__(self):
        return u'User Profile: %s' % self.user.username


# Make sure User model always has associated Member profile
def user_post_save(sender, instance, signal, *args, **kwargs):
    try:
        instance.userprofile
    except UserProfile.DoesNotExist:
        profile = UserProfile(
            user=instance,
        )
        profile.save()
signals.post_save.connect(user_post_save, sender=User)


class ClientFile(models.Model):
    title = models.CharField(max_length=200, blank=True)
    slug = AutoSlugField(max_length=200, unique=True, editable=False, prepopulate_from='title',
                    help_text='Unique text identifier used in urls.')
    file = CustomFileField(upload_to='files/clients/', help_text='Select a file to upload.')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    mimetype = models.CharField(max_length=100, editable=False)
    uploaded_by = models.ForeignKey(User, related_name='uploaded_files')
    belongs_to = models.ForeignKey(User, related_name='client_files')
    sort = models.IntegerField(null=True, blank=True, default=0, verbose_name="ordering", 
            help_text="Higher numbers will appear first.")

    def __unicode__(self):
        return u'Client File: %s (%s)' % (self.title, self.mimetype)

    def save(self, *args, **kwargs):
        # Guess mime type
        type = mimetypes.guess_type(self.file.name)[0]
        if type is None:
            type = 'unkown'
        self.mimetype = type
        
        # Auto-generate title from filename
        if not self.title:
            self.title = title(os.path.splitext(os.path.basename(self.file.name))[0].replace('_', ' '))
        super(ClientFile, self).save(*args, **kwargs)

    def type(self):
        if 'image/' in self.mimetype:
            return "image"
        elif 'video/' in self.mimetype:
            return "video"
        else:
            return None
    type = property(type)

    def has_preview(self):
        return self.type == "image" or self.type == "video"

    def video_thumb_url(self):
        if self.type != 'video':
            raise ValueError('video_preview must be called only on a video file type.')

        source = self.file.path
        target = "%s/_thumbs/videos/%s.png" % (os.path.dirname(source), self.pk)
        target_path = os.path.dirname(target)
        if not os.path.isfile(target) or (os.path.getmtime(source) > os.path.getmtime(target)):
            if not os.path.isdir(target_path):
                os.makedirs(target_path)
            try:
                encoder = PNGEncoder(ffmpeg=FFMPEG_PATH)
                encoder.encode(source, target)
            except EncodeError:
                if settings.TEMPLATE_DEBUG:
                    raise
                else:
                    return ''
        
        relpath = target.replace(settings.MEDIA_ROOT, '')
        return "%s%s" % (settings.MEDIA_URL, relpath)
    
    def preview_url(self):
        """
        """
        if self.type == 'image':
            t = template.Template('{% load thumbnail %}{% thumbnail file.file 320x320 detail %}')
            return t.render(template.Context({'file': self}))
        elif self.type == 'video':
            #return self.video_thumb_url()
            return self.file.url
        return ''
    preview_url = property(preview_url)

    def get_notes(self):
        from django.utils.dateformat import format
        from textwrap import dedent
        notes = []
        for note in self.notes.all():
            if note.user.is_superuser:
                cls = 'superuser'
            else:
                cls = 'client'
            t = template.Template(dedent("""
            <div class="{{cls}}">
            <pre>
            {{note.note}}
            <em>- {{note.date|date:"m/d/y P"}}</em>
            </pre>
            </div>
            """))
            notes.append (t.render(template.Context({
                'cls': cls,
                'note': note
            })))
        
        if notes:
            return "\n\n".join(notes)
        return ''


class ClientFileNote(models.Model):
    file = models.ForeignKey(ClientFile, related_name='notes')
    note = models.TextField(blank=False)
    date = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User)

    class Meta:
        ordering = ('-date',)

menus.register_url('Clients', '/clients/')
