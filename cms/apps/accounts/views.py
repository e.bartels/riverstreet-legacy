import os

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models import Q
from django.db.models import Max
from django.core.servers.basehttp import FileWrapper

from cms.apps.accounts.models import ClientFile
from cms.apps.accounts.forms import ClientFileForm, ClientChangeForm

@login_required
def client_index(request):
    if not request.user.is_superuser:
        user = request.user
        return HttpResponseRedirect('%s/' % user.id)
    
    user = request.user

    files = ClientFile.objects.all()
    total = files.count()

    # SEarch query
    search = request.GET.get('q', '')
    if search: 
        files = files.filter(Q(title__icontains=search) | Q(file__icontains=search))

    # Query ordering
    ordering = request.GET.get('o', 'belongs_to')
    dir = request.GET.get('ot', 'desc')
    if ordering and ordering in ClientFile._meta.get_all_field_names():
        if dir == 'desc':
            ot = '-'
        else:
            ot = ''
        files = files.order_by(ot + ordering, '-sort')

    last_updated = files.aggregate(Max('modified'))
    
    return render_to_response('clients/home.html', {
        'user': user,
        'files': files,
        'last_updated': last_updated['modified__max'],
        'ordering': ordering,
        'desc': dir == 'desc',
        'search': search,
        'total': total,
    }, context_instance=RequestContext(request))


@login_required
def client_profile(request, id):
    user = request.user
    owner = get_object_or_404(User, id=id)

    if not request.user.is_superuser and owner.id != request.user.id:
        return HttpResponseRedirect(settings.LOGIN_URL)

    files = owner.client_files.all()
    total = files.count()

    # Search query
    search = request.GET.get('q', '')
    if search:
        files = files.filter(Q(title__icontains=search) | Q(file__icontains=search))

    # Query ordering
    ordering = request.GET.get('o', 'sort')
    dir = request.GET.get('ot', 'desc')
    if ordering and ordering in ClientFile._meta.get_all_field_names():
        if dir == 'desc':
            ot = '-'
        else:
            ot = ''
        files = files.order_by(ot + ordering)
    else:
        ordering = ''

    last_updated = files.aggregate(Max('modified'))

    return render_to_response('clients/home.html', {
        'owner': owner,
        'user': user,
        'files': files,
        'last_updated': last_updated['modified__max'],
        'ordering': ordering,
        'desc': dir == 'desc',
        'search': search,
        'total': total,
    }, context_instance=RequestContext(request))


@login_required
def client_change_profile(request, id):
    user = request.user
    owner = get_object_or_404(User, id=id)

    if not request.user.is_superuser:
        return HttpResponseRedirect(settings.LOGIN_URL)

    if request.method == 'POST':
        form = ClientChangeForm(request.POST, instance=owner)
        if form.is_valid():
            owner = form.save()
            return HttpResponseRedirect('../')
    else:
        form = ClientChangeForm(instance=owner)
    
    return render_to_response('clients/change_profile.html', {
        'owner': owner,
        'user': user,
        'form': form,
    }, context_instance=RequestContext(request))

@login_required
def client_upload(request, id=None):
    user = request.user
    owner = get_object_or_404(User, id=id)

    if not request.user.is_superuser and owner.id != request.user.id:
        return HttpResponseRedirect(settings.LOGIN_URL)

    if request.method == "POST":
        form = ClientFileForm(request.POST, request.FILES)
        if form.is_valid():
            file = form.save(owner, user)
            return HttpResponseRedirect('../')
    else:
        form = ClientFileForm()

    return render_to_response('clients/upload.html', {
        'owner': owner,
        'user': user,
        'form': form
    }, context_instance=RequestContext(request))


@login_required
def client_edit_file(request, id, fid):
    user = request.user
    owner = get_object_or_404(User, id=id)

    if not request.user.is_superuser:
        return HttpResponseRedirect(settings.LOGIN_URL)

    file = get_object_or_404(ClientFile, id=fid)

    if request.method == 'POST':
        form = ClientFileForm(request.POST, request.FILES, instance=file)
        if form.is_valid():
            file = form.save(owner, user)
            file.save()
            return HttpResponseRedirect('../../')
    else:
        form = ClientFileForm(instance=file)
    
    return render_to_response('clients/edit_file.html', {
        'owner': owner,
        'user': user,
        'form': form
    }, context_instance=RequestContext(request))

@login_required
def client_delete_file(request, id, fid):
    user = request.user
    owner = get_object_or_404(User, id=id)

    if not request.user.is_superuser:
        return HttpResponseRedirect(settings.LOGIN_URL)

    file = get_object_or_404(ClientFile, id=fid)

    if request.POST:
        file.delete()
        return HttpResponseRedirect('../../')

    return render_to_response('clients/delete_file.html', {
        'owner': owner,
        'user': user,
        'file': file
    }, context_instance=RequestContext(request))


@login_required
def client_download_file(request, id, fid):
    user = request.user
    owner = get_object_or_404(User, id=id)

    if not request.user.is_superuser and owner.id != request.user.id:
        return HttpResponseRedirect(settings.LOGIN_URL)

    file = get_object_or_404(ClientFile, id=fid)

    response = HttpResponse(FileWrapper(file.file), mimetype=str(file.mimetype))
    response["Content-Disposition"]= "attachment; filename=%s" % os.path.basename(file.file.name)
    response['Content-Length'] = file.file.size

    return response
